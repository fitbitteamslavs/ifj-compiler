/* 
 * Subor datastructures.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 *  xmocar00 - Peter Močáry
 */

#ifndef IFJ20_DATASTRUCTURES_H
#define IFJ20_DATASTRUCTURES_H
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#define CONSTANT_ALLOC_INCREMENT 8
#define IDENTIF_STACK_INITIAL_CAPACITY 4

/* --- Context stack --- */
enum ContextType {
    CONTEXT_TYPE_NOT_SET = -1,
    CONTEXT_TYPE_FOR_HEADER,
    CONTEXT_TYPE_OTHER,
    CONTEXT_TYPE_FUNC
};

struct ContextInfo {
    unsigned id;
    enum ContextType type;
};

struct ContextStack {
    struct ContextInfo *contents;

    unsigned allocated_size;
    int top; /// Points to the current element at the top of the stack
};

enum ContextStackStatus {
    CONTEXT_STACK_EMPTY = -1,
    CONTEXT_STACK_OK = 0,
    CONTEXT_STACK_MALLOC_FAIL = 1,
    CONTEXT_STACK_REALLOC_FAIL = 2
};

int contextStackInit(struct ContextStack *stack);
void contextStackDestroy(struct ContextStack *stack);

int contextStackPush(struct ContextStack *stack, unsigned value, enum ContextType type);
void contextStackPop(struct ContextStack *stack);
int contextStackPeekTop(struct ContextStack *stack);

void contextStackClearContents(struct ContextStack *stack);
enum ContextType contextStackGetTopType(struct ContextStack *stack);

/* --- String --- */
enum StringStatus {  
    /* Order is important!
     * This enum must match scanner_errinfo.h ScannerErrorStatus enum in 0, 1 
     * and 2!
     */
    STR_OK = 0, 
    STR_MALLOC_FAIL = 1,
    STR_REALLOC_FAIL = 2,
};

typedef struct {
    char* str; 
    unsigned length;
    unsigned allocatedSize;
} string;


int strInit(string* s);
void strFree(string* s);

int strAddChar(string* s, char c);
int strAppendString(string* s, char* appendedString);

/* --- String vector --- */
struct StringVector {
    char **strings;
    unsigned stringsCount;
    unsigned allocatedSize;
};

enum StringVectorStatus {
    STRING_VECTOR_OK = 0,
    STRING_VECTOR_MALLOC_FAIL = 1,
    STRING_VECTOR_REALLOC_FAIL = 2
};

int stringVectorInit(struct StringVector *vect);
void stringVectorDestroy(struct StringVector *vect);

int stringVectorAddString(struct StringVector *vect, char *string);

/* --- Identifier stack --- */
struct IdentifierStack {
    char **identifierStack;
    unsigned top;   /// Points to first empty cell in the stack
    unsigned capacity;
};

enum IdentifierStackStatus {
    IDENTIF_STACK_OK,
    IDENTIF_STACK_MALLOC_FAILED,
    IDENTIF_STACK_CANNOT_GROW
};

enum IdentifierStackStatus identifierStackInit(struct IdentifierStack *stack);
void identifierStackDestroy(struct IdentifierStack *stack);

enum IdentifierStackStatus identifierStackPush(struct IdentifierStack *stack, char *identifierName);
char *identifierStackPop(struct IdentifierStack *stack);

bool identifierStackIsEmpty(struct IdentifierStack *stack);
unsigned identifierStackGetSize(struct IdentifierStack *stack);
#endif
