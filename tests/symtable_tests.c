#include <stdlib.h>
#include <check.h>
#include <stdarg.h>
#include "../symtable.h"

#define ck_assert_ptr_null(ptr)  do {ck_assert_int_eq(ptr == NULL, 1);} while (0);
#define ck_assert_ptr_nonnull(ptr) do {ck_assert_int_eq(ptr == NULL, 0);} while (0);

Symtable *symtable;

void setup(void) {
    symtable = symtableNew();
}

void teardown(void) {
    symtableFree(symtable);
}

Symbol *mk_test_symbol(int value) {
    Symbol *s = (Symbol *) malloc(sizeof(Symbol));
    if (s == NULL) return s;

    s->type = SYMBOL_TYPE_INT;
    s->attributes.intAttributes.value = value;

    return s;
}

START_TEST(insert_single_and_retrieve_no_context) { 
    const int symbol_value = 10;
    Symbol *s = mk_test_symbol(symbol_value);
    ck_assert_int_ne((long) s, 0);

    // Test symbol insertion.
    int status = symtableInsert(symtable, "symbol", s);
    ck_assert_int_eq(status, SYMTABLE_OK);
    
    // Test retrieval
    Symbol *retrieved_symbol = symtableLookup(symtable, "symbol");
    ck_assert_int_ne((long) retrieved_symbol, 0);
    ck_assert_int_eq(retrieved_symbol->type, SYMBOL_TYPE_INT);
    ck_assert_int_eq(retrieved_symbol->attributes.intAttributes.value, symbol_value);
}
END_TEST

START_TEST(insert_single_and_retrieve_with_context) { 
    const int symbol_value = 10;
    Symbol *s = mk_test_symbol(symbol_value);
    ck_assert_ptr_nonnull(s);
    
    int status;
    status = symtablePushContext(symtable);
    ck_assert_int_eq(status, SYMTABLE_OK);

    // Test symbol insertion.
    status = symtableInsert(symtable, "symbol", s);
    ck_assert_int_eq(status, SYMTABLE_OK);
    
    // Test retrieval
    Symbol *retrieved_symbol = symtableLookup(symtable, "symbol");
    ck_assert_int_ne((long) retrieved_symbol, 0);
    ck_assert_int_eq(retrieved_symbol->type, SYMBOL_TYPE_INT);
    ck_assert_int_eq(retrieved_symbol->attributes.intAttributes.value, symbol_value);
    
    // Test context pop
    status = symtablePopContext(symtable);
    ck_assert_int_eq(status, 0);

    // Is the symbol present?
    retrieved_symbol = symtableLookup(symtable, "symbol");
    ck_assert_ptr_null(retrieved_symbol);
}
END_TEST

START_TEST(insert_multiple_and_retrieve_with_context) { 
    const int symbol_values[] = {10, 20, 30};
    char * const symbol_names[] = {"symbol1", "symbol2", "symbol3"};

    int status;
    status = symtablePushContext(symtable);
    ck_assert_int_eq(status, SYMTABLE_OK);

    for (unsigned i = 0; i < 3; i++) {
        Symbol *s = mk_test_symbol(symbol_values[i]);
        ck_assert_ptr_nonnull(s);

        // Test symbol insertion.
        status = symtableInsert(symtable, symbol_names[i], s);
        ck_assert_int_eq(status, SYMTABLE_OK);
    }

    Symbol *retrieved_symbol;
    // Test retrieval
    for (unsigned i = 0; i < 3; i++) {
        Symbol *retrieved_symbol = symtableLookup(symtable, symbol_names[i]);
        ck_assert_ptr_nonnull(retrieved_symbol);
        ck_assert_int_eq(retrieved_symbol->type, SYMBOL_TYPE_INT);
        ck_assert_int_eq(retrieved_symbol->attributes.intAttributes.value, symbol_values[i]);
    }
    
    // Test context pop
    status = symtablePopContext(symtable);
    ck_assert_int_eq(status, 0);

    // Are the symbol present?
    for (unsigned i = 0; i < 3; i++) {
        retrieved_symbol = symtableLookup(symtable, symbol_names[i]);
        ck_assert_ptr_null(retrieved_symbol);
    }
}
END_TEST

Suite *symtable_suite(void) {
    Suite *suite = suite_create("SymtableSuite");
    TCase *test_case = tcase_create("Basic Tests");

    tcase_add_unchecked_fixture(test_case, setup, teardown);

    tcase_add_test(test_case, insert_single_and_retrieve_no_context);
    tcase_add_test(test_case, insert_single_and_retrieve_with_context);
    tcase_add_test(test_case, insert_multiple_and_retrieve_with_context);

    suite_add_tcase(suite, test_case);
    return suite;
}

int main(void) {
    Suite *suite = symtable_suite();
    SRunner *suite_runner = srunner_create(suite);

    srunner_run_all(suite_runner, CK_VERBOSE);
    srunner_free(suite_runner);
}
