#include <stdlib.h>
#include <check.h>
#include <stdarg.h>
#include "../scanner.h"
#include "../expression.h"
#include "../expression_debug.h"
#include "common_macros.h"


#define INITIALIZE_SCANNER_WITH_FILE(path)\
    FILE *old_stdin = stdin;                                \
    stdin = fopen(path, "r");     \
    scannerInit(scanner);                                   \
    peekToken(scanner);                                     \
    ck_assert_ptr_nonnull(stdin);                           \

#define DESTROY_SCANNER_ENV() \
    do { \
        scannerDeinit(scanner);     \
        fclose(stdin);              \
        stdin = old_stdin;          \
    } while(0);

Scanner *scanner;

void setup() {
    scanner = (Scanner *) malloc(sizeof(Scanner));
    if (scanner == NULL) {
        fprintf(stderr, "Could not initialize scanner.\n");
        exit(1);
    }
}

void teardown() {
    free(scanner);
}

void verifyBinaryExpr(
        Expr *e, 
        enum BinaryExprType expected_type,
        enum ExprType left_branch_type, 
        enum ExprType right_branch_type) {
    ck_assert_int_eq(e->type, EXPR_BINARY);
    ck_assert_int_eq(e->binaryExpr.type, expected_type);
    ck_assert_ptr_nonnull(e->binaryExpr.left);
    ck_assert_ptr_nonnull(e->binaryExpr.right);

    ck_assert_int_eq(e->binaryExpr.left->type, left_branch_type);
    ck_assert_int_eq(e->binaryExpr.right->type, right_branch_type);
}

START_TEST(test_simple_expr) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_simple.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);
    ck_assert_ptr_nonnull(stdin);


    Expr *e = parseExpressionIterative(scanner, false);
    ck_assert_ptr_nonnull(e);
    ck_assert_int_eq(e->type, EXPR_BINARY);
    ck_assert_int_eq(e->binaryExpr.type, BIN_EXPR_ADD);

    ck_assert_ptr_nonnull(e->binaryExpr.left);
    ck_assert_int_eq(e->binaryExpr.left->type, EXPR_LITERAL);
    ck_assert_int_eq(e->binaryExpr.left->literal.type, LIT_EXPR_INT);
    ck_assert_int_eq(e->binaryExpr.left->literal.intValue, 10);

    Expr *right_subtree = e->binaryExpr.right;
    ck_assert_ptr_nonnull(right_subtree);

    ck_assert_int_eq(e->type, EXPR_BINARY);
    ck_assert_ptr_nonnull(right_subtree->binaryExpr.left);
    ck_assert_int_eq(right_subtree->binaryExpr.left->type, EXPR_LITERAL);
    ck_assert_int_eq(right_subtree->binaryExpr.left->literal.type, LIT_EXPR_INT);
    ck_assert_int_eq(right_subtree->binaryExpr.left->literal.intValue, 20);

    ck_assert_ptr_nonnull(right_subtree->binaryExpr.right);
    ck_assert_int_eq(right_subtree->binaryExpr.right->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(right_subtree->binaryExpr.right->identifier.name, "identif");

    right_subtree = NULL;
    exprFree(e);
    
    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST

START_TEST(test_simple_reverse) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_simple_reverse.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);
    ck_assert_ptr_nonnull(stdin);


    Expr *e = parseExpressionIterative(scanner, false);
    ck_assert_ptr_nonnull(e);
    ck_assert_int_eq(e->type, EXPR_BINARY);
    ck_assert_int_eq(e->binaryExpr.type, BIN_EXPR_ADD);

    ck_assert_ptr_nonnull(e->binaryExpr.right);
    ck_assert_int_eq(e->binaryExpr.right->type, EXPR_IDENTIFIER);

    Expr *left_subtree = e->binaryExpr.left;
    ck_assert_ptr_nonnull(left_subtree);
    ck_assert_int_eq(left_subtree->type, EXPR_BINARY);
    ck_assert_int_eq(left_subtree->binaryExpr.type, BIN_EXPR_MUL);

    ck_assert_ptr_nonnull(left_subtree->binaryExpr.left);
    ck_assert_int_eq(left_subtree->binaryExpr.left->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(left_subtree->binaryExpr.left->identifier.name, "a");

    ck_assert_ptr_nonnull(left_subtree->binaryExpr.right);
    ck_assert_int_eq(left_subtree->binaryExpr.right->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(left_subtree->binaryExpr.right->identifier.name, "b");

    exprFree(e);
    
    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST

START_TEST(test_expr_with_funcall) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_with_funcall.ifj")
    
    Expr *e = parseExpressionIterative(scanner, false);
    ck_assert_ptr_nonnull(e);

    verifyBinaryExpr(e, BIN_EXPR_MUL, EXPR_FUNCALL, EXPR_IDENTIFIER);

    struct FuncCallExpr *fc;
    fc = &e->binaryExpr.left->funcCall;

    ck_assert_str_eq(fc->funcName, "print");
    ck_assert_int_eq(fc->argCount, 3);

    Expr *arg = fc->args[0];
    ck_assert_int_eq(arg->type, EXPR_LITERAL);
    ck_assert_int_eq(arg->literal.type, LIT_EXPR_INT);
    ck_assert_int_eq(arg->literal.intValue, 10);

    arg = fc->args[1];
    ck_assert_int_eq(arg->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(arg->identifier.name, "a");

    arg = fc->args[2];
    verifyBinaryExpr(arg, BIN_EXPR_ADD, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_int_eq(arg->binaryExpr.type, BIN_EXPR_ADD);
    ck_assert_str_eq(arg->binaryExpr.left->identifier.name, "b");
    ck_assert_str_eq(arg->binaryExpr.right->identifier.name, "b");
    
    exprFree(e);
    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_groupings_trivial) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_grouping_trivial.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);
    ck_assert_ptr_nonnull(stdin);
    
    struct ExprErrorDetails err_details = {0};
    Expr *e = parseExpressionIterative(scanner, false);

    ck_assert_ptr_nonnull(e);
    ck_assert_int_eq(e->type, EXPR_BINARY);
    ck_assert_int_eq(e->binaryExpr.type, BIN_EXPR_ADD);

    ck_assert_ptr_nonnull(e->binaryExpr.left);
    Expr *left_subtree = e->binaryExpr.left;
    ck_assert_int_eq(left_subtree->type, EXPR_BINARY);
    // identif * identif
    ck_assert_int_eq(left_subtree->binaryExpr.type, BIN_EXPR_MUL);
    ck_assert_ptr_nonnull(left_subtree->binaryExpr.left);
    ck_assert_int_eq(left_subtree->binaryExpr.left->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(left_subtree->binaryExpr.left->identifier.name, "identif");
    ck_assert_ptr_nonnull(left_subtree->binaryExpr.right);
    ck_assert_int_eq(left_subtree->binaryExpr.right->type, EXPR_IDENTIFIER);
    ck_assert_str_eq(left_subtree->binaryExpr.right->identifier.name, "identif");

    ck_assert_ptr_nonnull(e->binaryExpr.right);
    ck_assert_int_eq(e->binaryExpr.right->type, EXPR_LITERAL);
    ck_assert_int_eq(e->binaryExpr.right->literal.type, LIT_EXPR_INT);
    ck_assert_int_eq(e->binaryExpr.right->literal.intValue, 2);

    exprFree(e);

    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST


START_TEST(test_groupings_basic) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_grouping_basic.ifj");
    
    Expr *e = parseExpressionIterative(scanner, false);

    verifyBinaryExpr(e, BIN_EXPR_DIV, EXPR_BINARY, EXPR_IDENTIFIER);
    verifyBinaryExpr(e->binaryExpr.left, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_BINARY);

    /*ck_assert_str_eq(e->binaryExpr.left->binaryExpr.left->identifier.name, "a");*/

    Expr *grouping_expr = e->binaryExpr.left->binaryExpr.right;

    verifyBinaryExpr(grouping_expr, BIN_EXPR_ADD, EXPR_BINARY, EXPR_IDENTIFIER);

    verifyBinaryExpr(grouping_expr->binaryExpr.left, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_IDENTIFIER);

    exprFree(e);

    DESTROY_SCANNER_ENV();
}
END_TEST


START_TEST(test_groupings_nested) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_grouping_nested.ifj");
    
    Expr *e = parseExpressionIterative(scanner, false);
    
    verifyBinaryExpr(e, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_BINARY);
    verifyBinaryExpr(e->binaryExpr.right, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_BINARY);

    Expr *deepest_grouping_expr = e->binaryExpr.right->binaryExpr.right;
    verifyBinaryExpr(deepest_grouping_expr, BIN_EXPR_DIV, EXPR_BINARY, EXPR_IDENTIFIER);
    verifyBinaryExpr(deepest_grouping_expr->binaryExpr.left, BIN_EXPR_DIV, EXPR_IDENTIFIER, EXPR_IDENTIFIER);

    DESTROY_SCANNER_ENV();
}
END_TEST


START_TEST(test_advanced_mixed) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_mixed.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);
    ck_assert_ptr_nonnull(stdin);

    // (a + b * c + d) * (func(1)) <= (15 - func(b, a + c))
    Expr *e = parseExpressionIterative(scanner, false);
    // OP <= 
    // LEFT (a + b * c + d) * (func(1)) 
    // RIGHT (15 - func(b, a + c))
    verifyBinaryExpr(e, BIN_EXPR_LE, EXPR_BINARY, EXPR_BINARY);

    // LEFT (a + b * c + d) * (func(1)) 
    Expr *left = e->binaryExpr.left;
    verifyBinaryExpr(left, BIN_EXPR_MUL, EXPR_BINARY, EXPR_FUNCALL);
    verifyBinaryExpr(left->binaryExpr.left, BIN_EXPR_ADD, EXPR_BINARY, EXPR_IDENTIFIER);
    verifyBinaryExpr(left->binaryExpr.left->binaryExpr.left, BIN_EXPR_ADD, EXPR_IDENTIFIER, EXPR_BINARY);
    verifyBinaryExpr(left->binaryExpr
            .left->binaryExpr
            .left->binaryExpr
            .right, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_IDENTIFIER);

    // RIGHT (15 - func(b, a + c))
    Expr *right = e->binaryExpr.right;
    verifyBinaryExpr(right, BIN_EXPR_SUB, EXPR_LITERAL, EXPR_FUNCALL);
    
    struct FuncCallExpr *fc = &(right->binaryExpr.right->funcCall);
    ck_assert_int_eq(fc->argCount, 2);
    
    Expr *arg = fc->args[0];
    ck_assert_int_eq(arg->type, EXPR_IDENTIFIER);

    arg = fc->args[1];
    verifyBinaryExpr(arg, BIN_EXPR_ADD, EXPR_IDENTIFIER, EXPR_IDENTIFIER);

    exprFree(e);
    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST

START_TEST(test_expr_funcall_multiline) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_multiline_funcall.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);

    struct ExprErrorDetails err_details = {0};
    Expr *e = parseExpr(scanner, &err_details);
    ck_assert_ptr_nonnull(e);
    ck_assert_int_eq(e->type, EXPR_FUNCALL);
    ck_assert_str_eq(e->funcCall.funcName, "funcall");
    ck_assert_int_eq(e->funcCall.argCount, 3);

    ck_assert_int_eq(e->funcCall.args[0]->type, EXPR_IDENTIFIER);
    ck_assert_int_eq(e->funcCall.args[1]->type, EXPR_IDENTIFIER);
    ck_assert_int_eq(e->funcCall.args[2]->type, EXPR_LITERAL);
    ck_assert_int_eq(e->funcCall.args[2]->literal.type, LIT_EXPR_STR);
    
    exprFree(e);
    scannerDeinit(scanner);
    fclose(stdin);

    stdin = fopen("tests/inputs/expr_multiline_funcall_invalid.ifj", "r");
    scannerInit(scanner);
    
    e = parseExpr(scanner, &err_details);
    ck_assert_ptr_null(e);

    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST


START_TEST(test_expr_left_asoc) {
    FILE *old_stdin = stdin;
    stdin = fopen("tests/inputs/expr_left_asoc.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);

    Expr *e = parseExpressionIterative(scanner, false);

    Expr *it = e;
    verifyBinaryExpr(it, BIN_EXPR_MUL, EXPR_BINARY, EXPR_IDENTIFIER);
    ck_assert_str_eq(it->binaryExpr.right->identifier.name, "d");
    it = it->binaryExpr.left;

    verifyBinaryExpr(it, BIN_EXPR_DIV, EXPR_BINARY, EXPR_IDENTIFIER);
    ck_assert_str_eq(it->binaryExpr.right->identifier.name, "c");
    it = it->binaryExpr.left;

    verifyBinaryExpr(it, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_str_eq(it->binaryExpr.left->identifier.name, "a");
    ck_assert_str_eq(it->binaryExpr.right->identifier.name, "b");
    
    exprFree(e);
    scannerDeinit(scanner);

    stdin = fopen("tests/inputs/expr_left_asoc_adv.ifj", "r");
    scannerInit(scanner);
    peekToken(scanner);

    e = parseExpressionIterative(scanner, false);
    verifyBinaryExpr(e, BIN_EXPR_SUB, EXPR_BINARY, EXPR_BINARY);

    Expr *mul_subtree = e->binaryExpr.right;
    verifyBinaryExpr(mul_subtree, BIN_EXPR_DIV, EXPR_BINARY, EXPR_IDENTIFIER);
    ck_assert_str_eq(mul_subtree->binaryExpr.right->identifier.name, "a");

    verifyBinaryExpr(mul_subtree->binaryExpr.left, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_str_eq(mul_subtree->binaryExpr.left->binaryExpr.right->identifier.name, "b");
    ck_assert_str_eq(mul_subtree->binaryExpr.left->binaryExpr.left->identifier.name, "d");

    Expr *plus_subtree = e->binaryExpr.left;
    ck_assert_str_eq(plus_subtree->binaryExpr.right->identifier.name, "c");

    verifyBinaryExpr(plus_subtree->binaryExpr.left, BIN_EXPR_SUB, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_str_eq(plus_subtree->binaryExpr.left->binaryExpr.left->identifier.name, "a");
    ck_assert_str_eq(plus_subtree->binaryExpr.left->binaryExpr.right->identifier.name, "b");

    scannerDeinit(scanner);
    fclose(stdin);
    stdin = old_stdin;
}
END_TEST

START_TEST(test_expr_unbalanced_brackets) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_unbalanced_brackets.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);
    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_unfinished_funcall) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_unfinished_funcall.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);

    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_missing_operator) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_missing_operator.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);

    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_missing_operand) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_missing_operand.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);

    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_unexpected_comma) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_unexpected_comma.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);

    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_funcall_noparams) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_funcall_no_params.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);
    
    ck_assert_ptr_nonnull(e);
    ck_assert_int_eq(e->type, EXPR_FUNCALL);
    ck_assert_int_eq(e->funcCall.argCount, 0);

    exprFree(e);

    DESTROY_SCANNER_ENV()
}
END_TEST


START_TEST(test_expr_nested_funcall_missing_operator) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_nested_funcall_missing_operator.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);
    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_empty) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_empty.ifj");
    
    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);
    ck_assert_ptr_null(e);

    DESTROY_SCANNER_ENV()
}
END_TEST

START_TEST(test_expr_multiline_operator_at_end) {
    INITIALIZE_SCANNER_WITH_FILE("tests/inputs/expr_mutiline_operator_at_end.ifj");

    struct ExprErrorDetails details = {0};
    Expr *e = parseExpr(scanner, &details);
    ck_assert_ptr_nonnull(e);

    verifyBinaryExpr(e, BIN_EXPR_ADD, EXPR_BINARY, EXPR_BINARY);
    verifyBinaryExpr(e->binaryExpr.left, BIN_EXPR_ADD, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_str_eq(e->binaryExpr.left->binaryExpr.left->identifier.name, "a");
    ck_assert_str_eq(e->binaryExpr.left->binaryExpr.right->identifier.name, "b");


    verifyBinaryExpr(e->binaryExpr.right, BIN_EXPR_MUL, EXPR_IDENTIFIER, EXPR_IDENTIFIER);
    ck_assert_str_eq(e->binaryExpr.right->binaryExpr.left->identifier.name, "b");
    ck_assert_str_eq(e->binaryExpr.right->binaryExpr.right->identifier.name, "c");

    exprFree(e);
    
    DESTROY_SCANNER_ENV()
}
END_TEST

Suite * scanner_suite(void) {
    Suite *s;
    TCase *tc;

    s = suite_create("Expression parsing.");
    tc = tcase_create("Full expr parsing tests.");

    tcase_add_unchecked_fixture(tc, setup, teardown);

    tcase_add_test(tc, test_simple_expr);
    tcase_add_test(tc, test_simple_reverse);
    tcase_add_test(tc, test_expr_with_funcall);
    tcase_add_test(tc, test_expr_funcall_multiline);
    tcase_add_test(tc, test_groupings_trivial);
    tcase_add_test(tc, test_groupings_basic);
    tcase_add_test(tc, test_groupings_nested);
    tcase_add_test(tc, test_expr_left_asoc);
    tcase_add_test(tc, test_advanced_mixed);
    tcase_add_test(tc, test_expr_unbalanced_brackets);
    tcase_add_test(tc, test_expr_unfinished_funcall);
    tcase_add_test(tc, test_expr_unexpected_comma);
    tcase_add_test(tc, test_expr_missing_operand);
    tcase_add_test(tc, test_expr_missing_operator);
    tcase_add_test(tc, test_expr_funcall_noparams);
    tcase_add_test(tc, test_expr_nested_funcall_missing_operator);
    tcase_add_test(tc, test_expr_empty);
    tcase_add_test(tc, test_expr_multiline_operator_at_end);
    suite_add_tcase(s, tc);

    return s;
}

int main(void) {
    Suite *s;
    SRunner *sr;

    s = scanner_suite();
    sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_VERBOSE);
    srunner_free(sr);
}
