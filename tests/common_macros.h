#ifndef IFJ_TEST_COMMON_MACROS_H
#define IFJ_TEST_COMMON_MACROS_H

#define ck_assert_ptr_null(ptr)  do {ck_assert_int_eq(ptr == NULL, 1);} while (0);
#define ck_assert_ptr_nonnull(ptr) do {ck_assert_int_eq(ptr == NULL, 0);} while (0);

#endif
