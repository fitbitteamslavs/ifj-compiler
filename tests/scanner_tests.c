#include <stdlib.h>
#include <check.h>
#include <stdarg.h>
#include "../scanner.h"

Scanner scn;
FILE *oldStdin;

#define FLOAT_ARR_LEN 25
#define INT_ARR_LEN 10
#define ULLINT_ARR_LEN 5

#define testTokTyp(a)\
    ck_assert_int_eq(scn.current.type, a);\
    getToken(&scn);

#define testLCurly()\
    ck_assert_int_eq(scn.current.type, TOKEN_SEP_L_CURLY);\
    getToken(&scn);

#define testRCurly()\
    ck_assert_int_eq(scn.current.type, TOKEN_SEP_R_CURLY);\
    getToken(&scn);

#define testLBracket()\
    ck_assert_int_eq(scn.current.type, TOKEN_SEP_L_BRACKET);\
    getToken(&scn);

#define testRBracket()\
    ck_assert_int_eq(scn.current.type, TOKEN_SEP_R_BRACKET);\
    getToken(&scn);

#define testIdentif(a)\
    ck_assert_int_eq(scn.current.type, TOKEN_IDENTIF);\
    ck_assert_str_eq(scn.current.data.identifierName, a);\
    getToken(&scn);

#define testKW(kw)\
    ck_assert_int_eq(scn.current.type, TOKEN_KEYWORD);\
    ck_assert_int_eq(scn.current.data.keyword, kw);\
    getToken(&scn);

#define testInt(a)\
    ck_assert_int_eq(scn.current.type, TOKEN_LIT_INT);\
    ck_assert_uint_eq(scn.current.data.intValue, a);\
    getToken(&scn);   

#define testFloat(a)\
    ck_assert_int_eq(scn.current.type, TOKEN_LIT_FLOAT);\
    ck_assert_double_eq(scn.current.data.floatValue, a);\
    getToken(&scn);

#define testStr(a)\
    ck_assert_int_eq(scn.current.type, TOKEN_LIT_STR);\
    ck_assert_str_eq(scn.current.data.strValue, a);\
    getToken(&scn);

#define tEOL()\
    ck_assert_int_eq(scn.current.type, TOKEN_EOL);\
    getToken(&scn);

#define tEOF()\
    ck_assert_int_eq(scn.current.type, TOKEN_EOF);


void setupProg1(void) {
    FILE *fl = fopen("tests/inputs/prog1.ifj", "r");
    if(fl == NULL){
        fprintf(stderr, "Cannot open file\n");
    }
    oldStdin = stdin;
    stdin = fl;
    scannerInit(&scn);
}

void setupProg2(void) {
    FILE *fl = fopen("tests/inputs/prog2.ifj", "r");
    if(fl == NULL){
        fprintf(stderr, "Cannot open file\n");
    }
    oldStdin = stdin;
    stdin = fl;
    scannerInit(&scn);
}

void setupProg3(void) {
    FILE *fl = fopen("tests/inputs/prog3.ifj", "r");
    if(fl == NULL){
        fprintf(stderr, "Cannot open file\n");
    }
    oldStdin = stdin;
    stdin = fl;
    scannerInit(&scn);
}

void setupString(void) {
    FILE *fl = fopen("tests/inputs/scanner_string.ifj", "r");
    if(fl == NULL){
        fprintf(stderr, "Cannot open file\n");
    }
    oldStdin = stdin;
    stdin = fl;
    scannerInit(&scn);
}

void setupNumber(void) {
    FILE *fl = fopen("tests/inputs/scanner_number.ifj", "r");
    if(fl == NULL){
        fprintf(stderr, "Cannot open file\n");
    }
    oldStdin = stdin;
    stdin = fl;
    scannerInit(&scn);
}

void teardown(void) {
    stdin = oldStdin;
}

void testFunction(char *name, bool hasFunc, bool hasParams) {
    if(hasFunc) {
        testKW(KEYWORD_FUNC);
    }

    testIdentif(name);

    testLBracket();
    if(!hasParams) {
        testRBracket();
    }
}

START_TEST(test_prog1)
{

    // package main
    testKW(KEYWORD_PACKAGE);
    testIdentif("main");
    tEOL();
    tEOL();

    // func main() {
    testFunction("main", true, false);
    testLCurly();
    tEOL();

    // print("Zadejte cislo pro vypocet faktorialu: ")
    testFunction("print", false, true);
    testStr("Zadejte cislo pro vypocet faktorialu: ");
    testRBracket();
    tEOL();
    mark_point();

    // a := 0
    testIdentif("a");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();
    mark_point();

    // a, _ = inputi()
    testIdentif("a");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("_");
    testTokTyp(TOKEN_OP_ASSIGN);

    testFunction("inputi", false, false);
    tEOL();
    mark_point();

    //if a < 0 {
    testKW(KEYWORD_IF);
    testIdentif("a");
    testTokTyp(TOKEN_OP_REL_LESS);
    testInt(0);
    testLCurly();
    tEOL();
    mark_point();


    // print("Faktorial nejde spocitat!\n")
    testFunction("print", false, true);
    testStr("Faktorial nejde spocitat!\n");
    testRBracket();
    tEOL();
    mark_point();


    // } else {
    testRCurly();
    testKW(KEYWORD_ELSE);
    testLCurly();
    tEOL();

    // vysl := 1
    testIdentif("vysl");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(1);
    tEOL();

    // for ; a > 0; a = a - 1 {
    testKW(KEYWORD_FOR);
    testTokTyp(TOKEN_SEP_SEMICOLON);
    testIdentif("a");
    testTokTyp(TOKEN_OP_REL_GREATER);
    testInt(0);
    testTokTyp(TOKEN_SEP_SEMICOLON);
    testIdentif("a");
    testTokTyp(TOKEN_OP_ASSIGN);
    testIdentif("a");
    testTokTyp(TOKEN_OP_ARIT_MINUS);
    testInt(1);
    testLCurly();
    tEOL();

    // vysl = vysl * a
    testIdentif("vysl");
    testTokTyp(TOKEN_OP_ASSIGN);
    testIdentif("vysl");
    testTokTyp(TOKEN_OP_ARIT_TIMES);
    testIdentif("a");
    tEOL();

    // }
    testRCurly();
    tEOL();

    // print("Vysledek je ", vysl, "\n")
    testFunction("print", false, true);
    testStr("Vysledek je ");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("vysl");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testRBracket();
    tEOL();

    // }
    testRCurly();
    tEOL();
    
    //if a <= 10 {
    testKW(KEYWORD_IF);
    testIdentif("a");
    testTokTyp(TOKEN_OP_REL_LESSEQ);
    testInt(10);
    testLCurly();
    tEOL();

    //if a >= 0 {
    testKW(KEYWORD_IF);
    testIdentif("a");
    testTokTyp(TOKEN_OP_REL_GREATEREQ);
    testInt(0);
    testLCurly();
    tEOL();

    // a = a / 1
    testIdentif("a");
    testTokTyp(TOKEN_OP_ASSIGN);
    testIdentif("a");
    testTokTyp(TOKEN_OP_ARIT_DIV);
    testInt(1);
    tEOL();

    //}
    testRCurly();
    tEOL();

    //}
    testRCurly();
    tEOL();

    // }
    testRCurly();
    tEOL();
    tEOL();
    tEOF();
}
END_TEST

START_TEST(test_prog2)
{
    // package main
    testKW(KEYWORD_PACKAGE);
    testIdentif("main");
    tEOL();

    //EOL
    tEOL();

    //func factorial(n int) (int) {
    testFunction("factorial", true, true);
    testIdentif("n");
    testKW(KEYWORD_INT);
    testRBracket();
    testLBracket();
    testKW(KEYWORD_INT);
    testRBracket();
    testLCurly();
    tEOL();

    //dec_n := n - 1
    testIdentif("dec_n");
    testTokTyp(TOKEN_OP_DECLARE);
    testIdentif("n");
    testTokTyp(TOKEN_OP_ARIT_MINUS);
    testInt(1);
    tEOL();

    //if n < 2 {
    testKW(KEYWORD_IF);
    testIdentif("n");
    testTokTyp(TOKEN_OP_REL_LESS);
    testInt(2);
    testLCurly();
    tEOL();

    //return 1
    testKW(KEYWORD_RETURN);
    testInt(1);
    tEOL();
    
    //} else {
    testRCurly();
    testKW(KEYWORD_ELSE);
    testLCurly();
    tEOL();

    //tmp := 0
    testIdentif("tmp");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();
    
    //tmp = factorial(dec_n)
    testIdentif("tmp");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("factorial", false, true);
    testIdentif("dec_n");
    testRBracket();
    tEOL();
    
    //return n * tmp
    testKW(KEYWORD_RETURN);
    testIdentif("n");
    testTokTyp(TOKEN_OP_ARIT_TIMES);
    testIdentif("tmp");
    tEOL();
    
    //}
    testRCurly();
    tEOL();

    //}
    testRCurly();
    tEOL();
    
    //EOL
    tEOL();

    //func main() {
    testFunction("main", true, false);
    testLCurly();
    tEOL();

    //print("Zadejte cislo pro vypocet faktorialu: ")
    testFunction("print", false, true);
    testStr("Zadejte cislo pro vypocet faktorialu: ");
    testRBracket();
    tEOL();
    
    //a := 0
    testIdentif("a");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();
    
    //err := 0
    testIdentif("err");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();

    //a, err = inputi()
    testIdentif("a");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("err");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("inputi", false, false);
    tEOL();

    //if err == 0 {
    testKW(KEYWORD_IF);
    testIdentif("err");
    testTokTyp(TOKEN_OP_REL_EQ);
    testInt(0);
    testLCurly();
    tEOL();

    //if a < 0 {
    testKW(KEYWORD_IF);
    testIdentif("a");
    testTokTyp(TOKEN_OP_REL_LESS);
    testInt(0);
    testLCurly();
    tEOL();

    //print("Faktorial nejde spocitat!","\n")
    testFunction("print", false, true);
    testStr("Faktorial nejde spocitat!");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testRBracket();
    tEOL();

    //} else { 
    testRCurly();
    testKW(KEYWORD_ELSE);
    testLCurly();
    tEOL();

    //vysl := 0
    testIdentif("vysl");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();

    //vysl = factorial(a)
    testIdentif("vysl");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("factorial", false, true);
    testIdentif("a");
    testRBracket();
    tEOL();

    //print("Vysledek je: ", vysl,"\n")
    testFunction("print", false, true);
    testStr("Vysledek je: ");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("vysl");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testRBracket();
    tEOL();

    //}
    testRCurly();
    tEOL();

    //} else {
    testRCurly();
    testKW(KEYWORD_ELSE);
    testLCurly();
    tEOL();

    //print("Chyba pri nacitani celeho cisla!\n")
    testFunction("print", false, true);
    testStr("Chyba pri nacitani celeho cisla!\n");
    testRBracket();
    tEOL();

    //}
    testRCurly();
    tEOL();

    //}
    testRCurly();
    tEOL();

    //EOF
    tEOF();
}
END_TEST

START_TEST(test_prog3)
{

    //package main
    testKW(KEYWORD_PACKAGE);
    testIdentif("main");
    tEOL();

    //EOL
    tEOL();

    //func main() {
    testFunction("main", true, false);
    testLCurly();
    tEOL();

    //s1 := "Toto je nejaky text"
    testIdentif("s1");
    testTokTyp(TOKEN_OP_DECLARE);
    testStr("Toto je nejaky text");
    tEOL();

    //s2 := s1 + ",ktery jeste trochu obohatime"
    testIdentif("s2");
    testTokTyp(TOKEN_OP_DECLARE);
    testIdentif("s1");
    testTokTyp(TOKEN_OP_ARIT_PLUS);
    testStr(",ktery jeste trochu obohatime");
    tEOL();

    //print(s1,"\n", s2, "\n")
    testFunction("print", false, true);
    testIdentif("s1");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("s2");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testRBracket();
    tEOL();

    //s1len := 0
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();

    //s1len = len(s1)
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("len", false, true);
    testIdentif("s1");
    testRBracket();
    tEOL();

    //s1len = s1len - 4
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_ASSIGN);
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_ARIT_MINUS);
    testInt(4);
    tEOL();

    //s1, _ =substr(s2, s1len, 4)
    testIdentif("s1");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("_");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("substr", false, true);
    testIdentif("s2");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("s1len");
    testTokTyp(TOKEN_SEP_COMMA);
    testInt(4);
    testRBracket();
    tEOL();
    
    //s1len = s1len + 1
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_ASSIGN);
    testIdentif("s1len");
    testTokTyp(TOKEN_OP_ARIT_PLUS);
    testInt(1);
    tEOL();

    //print("4 znaky od", s1len,".znakuv\"", s2,"\":", s1,"\n")
    testFunction("print", false, true);
    testStr("4 znaky od");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("s1len");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr(". znaku v \"");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("s2");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\":");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("s1");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("\n");
    testRBracket();
    tEOL();

    //print("Zadejte serazenou posloupnost vsech malych pismen a-h,")
    testFunction("print", false, true);
    testStr("Zadejte serazenou posloupnost vsech malych pismen a-h,");
    testRBracket();
    tEOL();

    //print("pricemz se pismena nesmeji v posloupnosti opakovat:")
    testFunction("print", false, true);
    testStr("pricemz se pismena nesmeji v posloupnosti opakovat:");
    testRBracket();
    tEOL();

    //err := 0
    testIdentif("err");
    testTokTyp(TOKEN_OP_DECLARE);
    testInt(0);
    tEOL();

    //s1, err = inputs()
    testIdentif("s1");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("err");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("inputs",false,false);
    tEOL();

    //if err != 1 {
    testKW(KEYWORD_IF);
    testIdentif("err");
    testTokTyp(TOKEN_OP_REL_NOTEQ);
    testInt(1);
    testLCurly();
    tEOL();

    //for ;s1 != "abcdefgh"; {
    testKW(KEYWORD_FOR);
    testTokTyp(TOKEN_SEP_SEMICOLON);
    testIdentif("s1");
    testTokTyp(TOKEN_OP_REL_NOTEQ);
    testStr("abcdefgh");
    testTokTyp(TOKEN_SEP_SEMICOLON);
    testLCurly();
    tEOL();

    //print("\n","Spatne zadana posloupnost, zkuste znovu:")
    testFunction("print", false, true);
    testStr("\n");
    testTokTyp(TOKEN_SEP_COMMA);
    testStr("Spatne zadana posloupnost, zkuste znovu:");
    testRBracket();
    tEOL();

    //s1, _ = inputs()
    testIdentif("s1");
    testTokTyp(TOKEN_SEP_COMMA);
    testIdentif("_");
    testTokTyp(TOKEN_OP_ASSIGN);
    testFunction("inputs", false, false);
    tEOL();

    //}
    testRCurly();
    tEOL();

    //}else{
    testRCurly();
    testKW(KEYWORD_ELSE);
    testLCurly();
    tEOL();

    //}
    testRCurly();
    tEOL();

    //}
    testRCurly();
    tEOL();

    //EOF
    tEOF();
         


}
END_TEST

START_TEST(test_simple_strings)
{
    //empty string test
    testStr("");
    tEOL();

    //simple string test
    testStr("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
    tEOL();

    //simple string with number and a dummy comment
    testStr("Lorem 0123 ipsum dolor sit amet,// consectetur adipiscing elit.");
    tEOL();
    
    //string with dummy multi comment
    testStr("Lorem ipsum dolor sit amet,/* consectetur */ adipiscing elit.");
    tEOL();

    //string with all simple escapes
    testStr(" \"Lorem\tipsum\tdolor\nsit\tamet,\tconsectetur\tadip\\iscing\telit.");
    tEOL();
    
    //string with some valid HEX escapes
    testStr("Lorem\x0b ipsum\x2F dolor sit\x31 amet, consectetur\x32 adipiscing\x40 elit.");
    tEOL();

    //test if allocation of a large string doesnt break
    ck_assert_int_eq(scn.stateMachine.errorInfo.errorStatus, SCANNER_OK);
    getToken(&scn);
    tEOL();
    tEOF();
}
END_TEST

START_TEST(test_numbers)
{

    int integers[INT_ARR_LEN] = {
        0, 1, 6, 1337, 420,
        42069, 42, 30035, 123, 1000
    };

    double floats[FLOAT_ARR_LEN] = {
        0.42, 0.42e1, 0.42E0, 0.69e+2, 0.69e-2,
        0.42E+3, 0.42E-3, 0e12, 0e-2, 0e+2,
        0E-3, 0E+3, 420.69, 1337e1, 1337e-2,
        1337e+2, 1337E-3, 1337E+3, 69e6, 69E6,
        69e+2, 69e-2, 69E+3, 69E-3, 666.666
    };
        
    char *ullintegers[ULLINT_ARR_LEN] = {
        "66642069", "80085", "13376942", "999999999999999", "1111111111"
    };

    for (int i = 0; i < INT_ARR_LEN; ++i) {
        testInt(integers[i]);
        tEOL();
    }

    for (int i = 0; i < FLOAT_ARR_LEN; ++i) {
        testFloat(floats[i]);
        tEOL();
    }

    for (int i = 0; i < ULLINT_ARR_LEN; ++i) {

        ck_assert_int_eq(scn.current.type, TOKEN_LIT_INT);

        char current[16] = "123456789012345\0"; 
        sprintf(current, "%llu", scn.current.data.intValue);

        ck_assert_str_eq(current, ullintegers[i]);
        getToken(&scn);
        tEOL();
    }

    tEOF();

}
END_TEST

Suite * scanner_suite(void) {
    Suite *s;
    s = suite_create("Scanner");
    
    // test case creation for program1 form assignment
    TCase *prog1_tc;
    prog1_tc = tcase_create("Basic program1 test");
    tcase_add_unchecked_fixture(prog1_tc, setupProg1, teardown);

    tcase_add_test(prog1_tc, test_prog1);
    suite_add_tcase(s, prog1_tc);

    // test case creation for program2 from assignment
    TCase *prog2_tc;
    prog2_tc = tcase_create("Basic program2 test");
    tcase_add_unchecked_fixture(prog2_tc, setupProg2, teardown);

    tcase_add_test(prog2_tc, test_prog2);
    suite_add_tcase(s, prog2_tc);   

    // test case creation for program2 from assignment
    TCase *prog3_tc;
    prog3_tc = tcase_create("Basic program3 test");
    tcase_add_unchecked_fixture(prog3_tc, setupProg3, teardown);

    tcase_add_test(prog3_tc, test_prog3);
    suite_add_tcase(s, prog3_tc);

    // test case creation for string tests
    TCase *string_tc;
    string_tc = tcase_create("Basic string tests");
    tcase_add_unchecked_fixture(string_tc, setupString, teardown);

    tcase_add_test(string_tc, test_simple_strings);
    suite_add_tcase(s, string_tc);

    // test case creation for number tests
    TCase *number_tc;
    number_tc = tcase_create("Basic number tests");
    tcase_add_unchecked_fixture(number_tc, setupNumber, teardown);

    tcase_add_test(number_tc, test_numbers);
    suite_add_tcase(s, number_tc);

    return s;
}

int main(void) {
    Suite *s;
    SRunner *sr;

    s = scanner_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    srunner_free(sr);
    return 0;
}
