/* 
 * Subor buildins.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_BUILDINS_H 
#define IFJ20_BUILDINS_H   

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "scanner.h"
#include "symbol.h"
#include "symtable.h"

#define BUILDIN_PRINT_MAIN_LABEL        "print"
#define BUILDIN_INPUTS_MAIN_LABEL       "inputs"
#define BUILDIN_INPUTI_MAIN_LABEL       "inputi"
#define BUILDIN_INPUTF_MAIN_LABEL       "inputf"
#define BUILDIN_STRLEN_MAIN_LABEL       "len"
#define BUILDIN_SUBSTR_MAIN_LABEL       "substr"
#define BUILDIN_ORD_MAIN_LABEL          "ord"
#define BUILDIN_CHR_MAIN_LABEL          "chr"
#define BUILDIN_INT2FLOAT_MAIN_LABEL    "int2float"
#define BUILDIN_FLOAT2INT_MAIN_LABEL    "float2int"

#define _COMMENT(text) ""
#define _ENTER_FUNCTION(func_label) \
    "LABEL " func_label "\n" \
    "PUSHFRAME\n" 
#define _EXIT_FUNCTION() \
    "POPFRAME\n" \
    "RETURN\n"
#define _DECLARE_LOCAL(var_name) "DEFVAR LF@" var_name "\n"
#define _DECLARE_TEMP(var_name) "DEFVAR TF@" var_name "\n"
#define _LOCAL_VAR(var_name) "LF@" var_name
#define _TEMP_VAR(var_name) "TF@" var_name
#define _ADD(target, op1, op2) "ADD " target " " op1 " " op2 "\n"
#define _SUB(target, op1, op2) "SUB " target " " op1 " " op2 "\n"
#define _LT(target, op1, op2) "LT " target " " op1 " " op2 "\n"
#define _MOVE(target, source) "MOVE " target " " source "\n"
#define _JUMPIFEQ(target_label, var1, var2) "JUMPIFEQ " target_label " " var1 " " var2 "\n"
#define _JUMPIFNEQ(target_label, var1, var2) "JUMPIFNEQ " target_label " " var1 " " var2 "\n"
#define _STRLEN(target, source_string) "STRLEN " target " " source_string "\n"
#define _LABEL(name) "LABEL " name "\n"
#define _GETCHAR(destination, source, index) "GETCHAR " destination " " source " " index "\n"
#define _STRI2INT(destination, string, char_position) "STRI2INT " destination " " string " " char_position "\n"
#define _CONCAT(destination, op1, op2) "CONCAT " destination " " op1 " " op2 "\n"
#define _INT2CHR(destination, ord_value) "INT2CHAR " destination " " ord_value "\n"
#define _JUMP(dest) "JUMP " dest "\n"
#define _PUSHS(var) "PUSHS " var "\n"
#define _INT2FLOATS() "INT2FLOATS\n"
#define _FLOAT2INTS() "FLOAT2INTS\n"


enum BuildinFunction {
    BUILDINS_PRINT      = 0,
    BUILDINS_INT2FLOAT  = 1,
    BUILDINS_FLOAT2INT  = 2,
    BUILDINS_INPUTI     = 3,
    BUILDINS_INPUTS     = 4,
    BUILDINS_INPUTF     = 5,
    BUILDINS_LEN        = 6,
    BUILDINS_SUBSTR     = 7,
    BUILDINS_ORD        = 8,
    BUILDINS_CHR        = 9,
};


void emitCodeForBuildinFunction(enum BuildinFunction func);

void callBuildinPrint(unsigned argument_count);
void symtableInsertBuildIns(Symtable *sm);

#endif
