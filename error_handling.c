/* 
 * Subor error_handling.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xbalaz12 - Peter Balážec
 */
#include "error_handling.h"


char *generateUnexpectedTokenString(enum TokenType expected, enum TokenType actual) {
    // "Expected [token] but got [token]."
    char *msg = malloc(sizeof(char)*(MAX_TOKEN_NAME_LEN*2+24));
    sprintf(msg, "Expected '%s' but got '%s'.\n", getTokenName(expected), getTokenName(actual));
    return msg;
}

char *generateUnexpectedKWString(enum KeywordType expected, enum TokenType type, enum KeywordType actual) {
    char *msg;
    if(type == TOKEN_KEYWORD){
        msg = malloc(sizeof(char)*(MAX_TOKEN_NAME_LEN*2+34));
        sprintf(msg, "Expected keyword '%s' but got '%s'.\n", getKWName(expected), getKWName(actual));
    } else {
        msg = malloc(sizeof(char)*(MAX_KW_NAME_LEN+MAX_TOKEN_NAME_LEN+40));
        sprintf(msg, "Expected keyword '%s' but got token '%s'.\n", getKWName(expected), getTokenName(type));
    }
    return msg;
}

void syntaxError(const char *msg) {
    fprintf(stderr, "Syntax error: \n");
    // Different printing method based on whether tokens are read from history or stdin
    if(scn.fileFinished) {
        printCurrentStatement(scn);
    } else {
        errorInfoPrintErrorMessage(&scn.stateMachine.errorInfo);
    }
    fprintf(stderr, "%s\n", msg);
    exit(2);
}

void printCurrentStatement() {
    //current token
    struct TokenRecord *rec = &scn.hist.tokens[scn.hist.beginOfCurrentStatement];
    int col_position = 0;
    int arrow_position = 0;
    const char *empty = "";
    const char *to_print = NULL;
    // iterate from beginning of statement to the first EOL
    for(long long i = scn.hist.beginOfCurrentStatement; i < scn.hist.count && rec->tk.type != TOKEN_EOL && rec->tk.type != TOKEN_EOF; i++, rec++) {
        //When printing breaks abondon it and continue
        if(col_position > rec->srcColumn) {
            fprintf(stderr, "Something went terribly wrong while printing error message.\n");
            break;
        }

        // different printing methods for different tokens
        if(rec->tk.type == TOKEN_KEYWORD) {
            to_print = getKWName(rec->tk.data.keyword);
        } else if(rec->tk.type == TOKEN_IDENTIF) {
            to_print = rec->tk.data.identifierName;
        } else if(rec->tk.type == TOKEN_LIT_FLOAT) {
            to_print = empty;
            col_position += fprintf(stderr, "%.*g", rec->srcColumn - col_position, rec->tk.data.floatValue);
            col_position = rec->srcColumn-1;
        } else if(rec->tk.type == TOKEN_LIT_INT) {
            to_print = empty;
            col_position += fprintf(stderr, "%*lu", rec->srcColumn - col_position, rec->tk.data.intValue);
        } else if(rec->tk.type == TOKEN_LIT_STR) {
            to_print = empty;
            col_position += fprintf(stderr, "\"%s\"", rec->tk.data.strValue);
        } else {
            to_print = getTokenName(rec->tk.type);
        }

        //print spaces that were lost during reading, just approximate
        int str_len = strlen(to_print);
        while(col_position < rec->srcColumn-str_len) {
            putc(' ', stderr);
            col_position++;
        }

        //if currently processed token is the active token, then arrow should point here
        if(i+1 == scn.hist.currentPosition) {
            arrow_position = col_position;
        }

        col_position += fprintf(stderr, "%s", to_print);
    }
    putc('\n', stderr);

    //print arrow
    for(int i = 0; i < arrow_position; i++) {
        putc(' ', stderr);
    }
    putc('^', stderr);
    putc('\n', stderr);
    fprintf(stderr, "Row: %d Col: %d Arr: %d\n", scn.hist.currentRow, col_position, arrow_position);
}

void semanticError(int exitcode, const char *msg) {
    fprintf(stderr, "Semantic Error: \n");
    // Semantic error cannot occure during first pass
    printCurrentStatement();
    fprintf(stderr, "%s\n", msg);
    exit(exitcode);
}

void internalError(const char *msg) {
    fprintf(stderr, "Internal Error: \n");
    fprintf(stderr, "%s\n", msg);
    exit(99);
}
