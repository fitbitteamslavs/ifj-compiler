CC=gcc
CFLAGS=-g -O0 -Wall -Wextra -pedantic

TESTLIB := $$(pkg-config --cflags --libs check)

.PHONY = clean test

main: main.o syntax.o scanner.o scanner_errinfo.o expression.o symtable.o symbol.o expression_semantic.o error_handling.o semantic.o buildins.o datastructures.o

symtable_test: symtable.o symtable_test.o

valgrind: symtable_test
	valgrind --tool=memcheck --leak-check=full ./symtable_test

test-scanner-bin: tests/scanner_tests.o scanner.o datastructures.o scanner_errinfo.o
	@echo "Compiling scanner tests..."
	@$(CC) -o test $^ $(TESTLIB)

test-symtable-bin: tests/symtable_tests.o symtable.o
	@echo "Compiling symtable tests..."
	@$(CC) -o test-symtable $^ $(TESTLIB)


test-expr-bin: tests/expr_tests.o scanner.o scanner_errinfo.o datastructures.o expression.o expression_debug.o
	$(CC) -o $@ $^ $(TESTLIB)
	

test: test-scanner-bin test-symtable-bin
	@echo ""
	@./test
	@echo ""
	@./test-symtable

clean:
	rm -rf *.o
	rm -rf tests/*.o
	rm -rf test test-symtable
	rm -rf main
	rm -rf *gcno *gcda *gcov
	rm -rf scanner_coverage
	rm -rf .gdb_history


# Sometimes, it would be great if we recompiled after the header did change (for example we changed value in #define).
# Ofc, there is a way how to generate makefile dependency graph automatically
# but I'm tired.

run_test: run_test.o scanner.o scanner_errinfo.o datastructures.o expression.o expression_debug.o
	$(CC) -o run_test $^

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $^

expression_debug.o: expression_debug.c expression_debug.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

expression.o: expression.c expression.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

datastructures.o: datastructures.c datastructures.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

run_test.o: run_test.c
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

scanner.o: scanner.c scanner.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

symtable.o: symtable.c symtable.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

scanner_errinfo.o: scanner_errinfo.c scanner_errinfo.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $(filter %.c,$^)

test_scanner_coverage: datastructures.c scanner_errinfo.c scanner.c tests/scanner_tests.c
	@$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -o scanner_coverage $^ $(TESTLIB)
	@./scanner_coverage
	@echo ""
	@echo "Scanner tests source coverage"
	@gcov -bn scanner.c
	@echo ""
	@echo "For bit more info use gcov -bfn scanner.c"
	@echo "For line specific info use gcov -tkq scanner.c"
