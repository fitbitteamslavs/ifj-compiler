/* 
 * Subor expression_debug.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xhecko02 - Michal Hečko
 */

#include "expression_debug.h"

// Matches enum BinaryExprType
const char *BIN_EXPRESSION_NODE_NAMES[] = {
    "+",
    "-",
    "*",
    "/",
    "==",
    "!=",
    "<",
    "<=",
    ">",
    ">=",
};

void printLiteral(const Expr *e) {
    const struct LiteralExpr* lit = &e->literal;
    switch (lit->type) {
        case LIT_EXPR_INT: 
            printf("|-> Literal %u@int\n", lit->intValue);
            break;
        case LIT_EXPR_STR: 
            printf("|-> Literal %s@str\n", lit->strValue);
            break;
        case LIT_EXPR_FLOAT: 
            printf("|-> Literal %f@float\n", lit->floatValue);
            break;
    }
}

void printFuncCall(const Expr *e, const int depth) {
    printf("|-> funcall (funcName=%s), with args: \n", e->funcCall.funcName);
    for (unsigned i = 0; i < e->funcCall.argCount; i++) {
        prettyPrintExprTree(e->funcCall.args[i], depth + 1);
    }
}

void prettyPrintExprTree(const Expr *expr, int depth) {
    if (expr == NULL) {
        return;
    }
    
    printCharNTimes(' ', depth * 2); // Because we want bigger indent
    switch (expr->type) {
        case (EXPR_BINARY):
            printf("|-> %s\n", getBinaryExprTypeName(expr->binaryExpr.type));
            prettyPrintExprTree(expr->binaryExpr.left, depth + 1);
            prettyPrintExprTree(expr->binaryExpr.right, depth + 1);
            break;
        case (EXPR_LITERAL):
            printLiteral(expr);
            break;
        case (EXPR_FUNCALL):
            printFuncCall(expr, depth);
            break;
        case (EXPR_IDENTIFIER):
            printf("|-> itentifier=%s\n", expr->identifier.name);
            break;
        default:
            fprintf(stderr, "Error while printing expr tree, unknown node type: %u\n", expr->type);
            exit(1997);
    }
}

void printCharNTimes(const char c, const unsigned times) {
    for (unsigned i = 0; i < times; i++) {
        fputc(c, stdout);
    }
}

const char* getBinaryExprTypeName(enum BinaryExprType type) {
    return BIN_EXPRESSION_NODE_NAMES[type];
}
