/* 
 * Subor buildins.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 */

#include "buildins.h"

/*
 * ASM for function `print`
 * On TF expects only one argument - TF@argument_count - number of arguments to print
 * On Data Stack expects argCount * argument pushed from right to left
 */
const char *const BUILDIN_PRINT_ASM_BODY = 
"LABEL " BUILDIN_PRINT_MAIN_LABEL "\n"
"PUSHFRAME\n"

"DEFVAR LF@current_argument\n"

"LABEL print$printloop\n"
"JUMPIFEQ print$finalize LF@argument_count int@0\n"

"POPS LF@current_argument\n"
"WRITE LF@current_argument\n"

"SUB LF@argument_count LF@argument_count int@1\n"
"JUMP print$printloop\n"

"LABEL print$finalize\n"
"POPFRAME\n"
"RETURN\n";

const char *const BUILDIN_INPUTS_ASM_BODY =
"LABEL " BUILDIN_INPUTS_MAIN_LABEL "\n"
"PUSHFRAME\n"
"DEFVAR LF@input\n"
"READ LF@input string\n"
"JUMPIFEQ inputs$error LF@input nil@nil\n"
"PUSHS LF@input\n"
"PUSHS int@0\n"
"JUMP inputs$finalize\n"

"LABEL inputs$error\n"
"PUSHS string@\n"
"PUSHS int@1\n"
"JUMP inputs$finalize\n"

"LABEL inputs$finalize\n"
"POPFRAME\n"
"RETURN\n";

const char *const BUILDIN_INPUTI_ASM_BODY =
"LABEL " BUILDIN_INPUTI_MAIN_LABEL "\n"
"PUSHFRAME\n"
"DEFVAR LF@input\n"
"READ LF@input int\n"
"JUMPIFEQ inputi$error LF@input nil@nil\n"
"PUSHS LF@input\n"
"PUSHS int@0\n"
"JUMP inputi$finalize\n"

"LABEL inputi$error\n"
"PUSHS int@0\n"
"PUSHS int@1\n" "JUMP inputi$finalize\n"

"LABEL inputi$finalize\n"
"POPFRAME\n"
"RETURN\n";

const char *const BUILDIN_INPUTF_ASM_BODY =
"LABEL " BUILDIN_INPUTF_MAIN_LABEL "\n"
"PUSHFRAME\n"
"DEFVAR LF@input\n"
"READ LF@input float\n"
"JUMPIFEQ inputf$error LF@input nil@nil\n"
"PUSHS LF@input\n"
"PUSHS int@0\n"
"JUMP inputf$finalize\n"

"LABEL inputf$error\n"
"PUSHS float@0x0p+0\n"
"PUSHS int@1\n"
"JUMP inputf$finalize\n"

"LABEL inputf$finalize\n"
"POPFRAME\n"
"RETURN\n";

/**
 * Takes one argument - string, stored in TF@string
 * Returns length of TF@string on data stack
 */
const char *const BUILDIN_LEN_ASM_BODY =
"LABEL " BUILDIN_STRLEN_MAIN_LABEL "\n"
"PUSHFRAME\n"
"DEFVAR LF@string_length\n"
"STRLEN LF@string_length LF@s\n"
"PUSHS LF@string_length\n"
"POPFRAME\n"
"RETURN\n";

/**
 * Substr - takes 3 args:
 *   - TF@s - string to be taken substring from
 *   - TF@i - start of substring (0 means first character)
 *   - TF@n - numbers of character to be contained inside substring
 * Returns - (substring, int) on datastack (int is top)
 */
const char *const BUILDIN_SUBSTR_ASM_BODY = 
_ENTER_FUNCTION(BUILDIN_SUBSTR_MAIN_LABEL)

_DECLARE_LOCAL("string_length")
_STRLEN(_LOCAL_VAR("string_length"), _LOCAL_VAR("s"))

// Check whether index is within the bounds of string 
_DECLARE_LOCAL("cmp")
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), _LOCAL_VAR("string_length"))
_JUMPIFNEQ("substr$index_out_of_bounds", _LOCAL_VAR("cmp"), "bool@true") // Check !(i < strlen(s)) ==> i >= strlen(s)
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), "int@0") // Check if i < 0
_JUMPIFEQ("substr$index_out_of_bounds", _LOCAL_VAR("cmp"), "bool@true")

_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("n"), "int@0") // Check if n < 0
_JUMPIFEQ("substr$substr_size_negative", _LOCAL_VAR("cmp"), "bool@true")

// Check how many characters should the substr contain from i to end
_DECLARE_LOCAL("chars_remaining")
_SUB(_LOCAL_VAR("chars_remaining"), _LOCAL_VAR("string_length"), _LOCAL_VAR("i"))
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("n"), _LOCAL_VAR("chars_remaining"))
_JUMPIFEQ("substr$char_count_ok", _LOCAL_VAR("cmp"), "bool@true")

// In case the value in 'n' is bigger the chars_remaining, cap 'n' to chars remaining
_MOVE(_LOCAL_VAR("n"), _LOCAL_VAR("chars_remaining"))

_LABEL("substr$char_count_ok")
_ADD(_LOCAL_VAR("n"), _LOCAL_VAR("n"), _LOCAL_VAR("i"))

_DECLARE_LOCAL("tmp_char")
_DECLARE_LOCAL("result")
_MOVE(_LOCAL_VAR("result"), "string@") // Initialize to empty string in order for concat to work

_LABEL("substr$concat_loop")
_JUMPIFEQ("substr$concat_done", _LOCAL_VAR("i"), _LOCAL_VAR("n"))
_GETCHAR(_LOCAL_VAR("tmp_char"), _LOCAL_VAR("s"), _LOCAL_VAR("i"))
_CONCAT(_LOCAL_VAR("result"), _LOCAL_VAR("result"), _LOCAL_VAR("tmp_char"))

// Increment i
_ADD(_LOCAL_VAR("i"), _LOCAL_VAR("i"), "int@1")
_JUMP("substr$concat_loop")

_LABEL("substr$concat_done")
_PUSHS(_LOCAL_VAR("result"))
_PUSHS("int@0")
_JUMP("substr$finalize")

_LABEL("substr$substr_size_negative")
_LABEL("substr$index_out_of_bounds")
_PUSHS("string@")
_PUSHS("int@1")

_LABEL("substr$finalize")
_EXIT_FUNCTION()
;

/**
 * The body of buildin 'ord' function.
 * Args:
 *  - TF@s string containing the char to be taked ord of
 *  - TF@i int  index of the char
 * Returns:
 *  - (int, int) ordinal value, error  -- stored on data stack
 */
const char *const BUILDIN_ORD_ASM_BODY = 
_ENTER_FUNCTION(BUILDIN_ORD_MAIN_LABEL)

_DECLARE_LOCAL("string_length")
_STRLEN(_LOCAL_VAR("string_length"), _LOCAL_VAR("s"))
_DECLARE_LOCAL("cmp")
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), _LOCAL_VAR ("string_length"))
_JUMPIFNEQ("ord$index_out_of_bounds", _LOCAL_VAR("cmp"), "bool@true")
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), "int@0")
_JUMPIFEQ("ord$index_out_of_bounds", _LOCAL_VAR("cmp"), "bool@true")

// MAYBE: Store the ordinal value in string_lenght, so we don't have to declare new var
_DECLARE_LOCAL("ord_value")
_STRI2INT(_LOCAL_VAR("ord_value"), _LOCAL_VAR("s"), _LOCAL_VAR("i"))
_PUSHS(_LOCAL_VAR("ord_value"))
_PUSHS("int@0")
_JUMP("ord$finalize")

_LABEL("ord$index_out_of_bounds")
_PUSHS("int@0")
_PUSHS("int@1")

_LABEL("ord$finalize")
_EXIT_FUNCTION();


const char *const BUILDIN_CHR_ASM_BODY = 
_ENTER_FUNCTION(BUILDIN_CHR_MAIN_LABEL)
_DECLARE_LOCAL("cmp")
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), "int@256")
_JUMPIFNEQ("chr$ord_out_of_range", _LOCAL_VAR("cmp"), "bool@true")
_LT(_LOCAL_VAR("cmp"), _LOCAL_VAR("i"), "int@0")
_JUMPIFEQ("chr$ord_out_of_range", _LOCAL_VAR("cmp"), "bool@true")
_DECLARE_LOCAL("char")
_INT2CHR(_LOCAL_VAR("char"), _LOCAL_VAR("i"))
_PUSHS(_LOCAL_VAR("char"))
_PUSHS("int@0")
_JUMP("chr$finalize")
_LABEL("chr$ord_out_of_range")
_PUSHS("string@")
_PUSHS("int@1")
_LABEL("chr$finalize")
_EXIT_FUNCTION();

/**
 * Body for buildin function int2float(int) (float)
 * Params:
 *  - TF@i int - integer to be converted into float
 * Returns:
 *  - float value of TF@i on Data stack
 */
const char *const BUILDIN_INT2FLOAT_ASM_BODY = 
_ENTER_FUNCTION(BUILDIN_INT2FLOAT_MAIN_LABEL)
 // This will convert the value LF@i pushed onto stack to float and store it on stack
_PUSHS(_LOCAL_VAR("i"))
_INT2FLOATS()      
_EXIT_FUNCTION();

/**
 * Body for buildin function float2int(float) (int)
 * Params:
 *  - TF@f float - float to be converted into int
 * Returns:
 *  - int value of TF@f on Data stack
 */
const char *const BUILDIN_FLOAT2INT_ASM_BODY =
_ENTER_FUNCTION(BUILDIN_FLOAT2INT_MAIN_LABEL)
// This will convert the value LF@f pushed onto stackto int and store it on stack
_PUSHS(_LOCAL_VAR("f"))
_FLOAT2INTS()       
_EXIT_FUNCTION();

char * build_in_fns[] = {
    "inputs", "inputi", "inputf",
    "print",
    "int2float", "float2int",
    "len", "substr", "ord", "chr"
};

#define addRet(arg) addReturnTypeToFunction(sm, arg);
#define addArg(argN, argT) addArgumentToFunction(sm, argN, argT);
#define advance() setFunctionLabel(sm, build_in_fns[i]);symtableInsert(st, build_in_fns[i], sm);i++;sm = createFunctionSymbol(); 

void symtableInsertBuildIns(Symtable *st) {
    Symbol *sm = createFunctionSymbol();
    int i = 0;
    //inputs
    addRet(VAL_TYPE_STR);
    addRet(VAL_TYPE_INT);
    advance();
    //inputi
    addRet(VAL_TYPE_INT);
    addRet(VAL_TYPE_INT);
    advance();
    //inputf
    addRet(VAL_TYPE_FLOAT);
    addRet(VAL_TYPE_INT);
    advance();
    //print
    advance();
    //int2float
    addRet(VAL_TYPE_FLOAT);
    addArg("i", VAL_TYPE_INT);
    advance();
    //float2int
    addRet(VAL_TYPE_INT);
    addArg("f", VAL_TYPE_FLOAT);
    advance();
    //len
    addArg("s", VAL_TYPE_STR);
    addRet(VAL_TYPE_INT);
    advance();
    //substr
    addArg("s", VAL_TYPE_STR);
    addArg("i", VAL_TYPE_INT);
    addArg("n", VAL_TYPE_INT);
    addRet(VAL_TYPE_STR);
    addRet(VAL_TYPE_INT);
    advance();
    //ord
    addArg("s", VAL_TYPE_STR);
    addArg("i", VAL_TYPE_INT);
    addRet(VAL_TYPE_INT);
    addRet(VAL_TYPE_INT);
    advance();
    //chr
    addArg("i", VAL_TYPE_INT);
    addRet(VAL_TYPE_STR);
    addRet(VAL_TYPE_INT);
    advance();
}


// Debugging purposes
const char *const BuildinFunctionNames[] = {
    "PRINT",
    "INT2FLOAT",
    "FLOAT2INT",
    "INPUTI",
    "INPUTS",
    "INPUTF",
    "LEN",
    "SUBSTR",
    "ORD",
    "CHR"
};


/**
 * When called emits the whole body including initial label, for 
 * buildin function print.
 *
 * The function has following arguments:
 *  - on TF:
 *      - TF@argument_count  -- int - number of arguments to be printed
 *  - on Data stack:
 *      - arugments to print - the leftmost argument to print when called must be
 *        stored on top of the stack
 *        Example: print(t1, t2, t3)
 *                 stack-top: t1, then t2, then t3 
 *  Function does not return anything.
 */
void emitBuildinPrintBody() {
    fprintf(stdout, BUILDIN_PRINT_ASM_BODY); 
}

/**
 * Presumes that arguments to be printed are already stored on data stack.
 */
void callBuildinPrint(unsigned argument_count) {
    fprintf(stdout, "CREATEFRAME\n"); 
    fprintf(stdout, "DEFVAR TF@argument_count\n"); 
    fprintf(stdout, "MOVE TF@argument_count int@%u\n", argument_count); 
    fprintf(stdout, "CALL %s\n", BUILDIN_PRINT_MAIN_LABEL); 
}


void emitCodeForBuildinFunction(enum BuildinFunction func) {
    const char* const BUILDIN_FUNCTION_BODIES[] = {
        BUILDIN_PRINT_ASM_BODY,
        BUILDIN_INT2FLOAT_ASM_BODY,
        BUILDIN_FLOAT2INT_ASM_BODY,
        BUILDIN_INPUTI_ASM_BODY,
        BUILDIN_INPUTS_ASM_BODY,
        BUILDIN_INPUTF_ASM_BODY,
        BUILDIN_LEN_ASM_BODY,
        BUILDIN_SUBSTR_ASM_BODY,
        BUILDIN_ORD_ASM_BODY,
        BUILDIN_CHR_ASM_BODY,
    };
    fprintf(stdout, "%s", BUILDIN_FUNCTION_BODIES[func]); 
}
