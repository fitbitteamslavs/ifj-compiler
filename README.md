# Compiler project for IFJ

## State machine for scanner
[State machine diagram](https://docs.google.com/drawings/d/1rmmuM2e3eiKhyu2wJCk70vQqO5iMrheQW9_A8CLrN7A/edit?usp=sharing)


## Code style
| code part | style                            |
|-----------|----------------------------------|
| constants | CAPITAL                          |
| variables | this\_is\_variable\_name         |
| functions | thisIsFunctionThatDoesSomething()|
| new types | TypeName                         |

New types should be defined by `typedef` only for often used elements.
Indent is `4 spaces`.
End of line is `LF`.
Use .editorconfig for these.

## Testing
For testing without different threads run the test binary with variable `CK_FORK=no`.
