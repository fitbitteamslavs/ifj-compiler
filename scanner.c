/* 
 * Subor scanner.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#include "scanner.h"

const char *keywords[] = {
        "if", "else", "for", 
        "return", "float64", "int",
        "string", "func", "package"};


// Because we want to print syntax error messages
// Matches enum TokenType - changing order will break translation
const char * const token_names[] = { 
    "identifier",
    "EOL",
    ":=",
    "=",
    "+",
    "-",
    "*",
    "/",
    "==",
    "!=",
    "<",
    "<=",
    ">",
    ">=",
    "String literal",
    "Int literal",
    "Float literal",
    "(",
    ")",
    "{",
    "}",
    ",",
    ";",
    "Keyword",
    "EOF",
};

/* Main Scanner functions */
void scannerInit(Scanner *scn) {
    scn->isNextValid = false;
    scn->fileFinished = false;
    scn->stateMachine.hasExtraChar = false;
    errorInfoInit(&scn->stateMachine.errorInfo);

    scn->hist.currentPosition = -1;
    scn->hist.beginOfCurrentStatement = 0;
    scn->hist.count = 0;
    scn->hist.currentRow = 0;
    struct TokenRecord *tmp = malloc(sizeof(struct TokenRecord));
    if(tmp == NULL) {
        fprintf(stderr, "Malloc of history failed.\n");
        exit(99);
    }
    scn->hist.tokens = tmp;
    scn->hist.size = 1;

    getToken(scn);
}

void scannerDeinit(Scanner *scn) {
    errorInfoDestroy(&scn->stateMachine.errorInfo);
    
    if (scn->isNextValid) {
        if (scn->next.type == TOKEN_IDENTIF) {
            free(scn->next.data.strValue);
        }
    }
}

void scannerSeek(Scanner *scn, long long index) {
    if(index < scn->hist.count && index >= 0) {
        scn->hist.currentPosition = index;
        scn->current = scn->hist.tokens[index].tk;
        scn->stateMachine.errorInfo.srcPositionColumn = scn->hist.tokens[index].srcColumn;
        scn->stateMachine.errorInfo.srcPositionLine = scn->hist.tokens[index].srcLine;

    }
}

long long scannerGetPosition(Scanner *sc) {
    return sc->hist.currentPosition;
}

void scannerNewStatement(Scanner *scn) {
    scn->hist.beginOfCurrentStatement = scn->hist.currentPosition;
    scn->hist.currentRow += 1;
}

void getToken(Scanner *scn){
    if(scn->fileFinished) {
        if(scn->hist.currentPosition < scn->hist.count-1) {
            scn->hist.currentPosition++;
            scn->current = scn->hist.tokens[scn->hist.currentPosition].tk;
        }
    } else {
        if(scn->isNextValid) {
            scn->current = scn->next;
            scn->isNextValid = false;
        } else {
            stateMachineDriver(&scn->stateMachine, &scn->current); 
            checkError(scn); // Check if an error occurred in Scanner
        }
        
        if(scn->hist.count >= scn->hist.size) {
            struct TokenRecord *tmp = realloc(scn->hist.tokens, scn->hist.size*2*sizeof(struct TokenRecord));
            if(tmp == NULL) {
                fprintf(stderr, "Realloc of history failed.\n");
                exit(99);
            }

            scn->hist.tokens = tmp;

            scn->hist.size *= 2;
        }
        scn->hist.tokens[scn->hist.count].tk = scn->current;
        scn->hist.tokens[scn->hist.count].srcLine = scn->stateMachine.errorInfo.srcPositionLine-(scn->stateMachine.hasExtraChar ? 1 : 0);
        scn->hist.tokens[scn->hist.count].srcColumn = scn->stateMachine.errorInfo.srcPositionColumn-(scn->stateMachine.hasExtraChar ? 1 : 0);
        scn->hist.count++;

        if(scn->current.type == TOKEN_EOF) {
            scn->hist.currentPosition = scn->hist.count-1;
            scn->fileFinished = true;
        }
    }
}

void peekToken(Scanner *scn) {
    if(scn->fileFinished) {
        if(scn->hist.currentPosition < scn->hist.count - 2) {
            scn->next = scn->hist.tokens[scn->hist.currentPosition+1].tk;
            scn->stateMachine.errorInfo.srcPositionColumn = scn->hist.tokens[scn->hist.currentPosition+1].srcColumn;
            scn->stateMachine.errorInfo.srcPositionLine = scn->hist.tokens[scn->hist.currentPosition+1].srcLine;
        } else {
            scn->next = scn->hist.tokens[scn->hist.count-1].tk;
            scn->stateMachine.errorInfo.srcPositionColumn = scn->hist.tokens[scn->hist.count-1].srcColumn;
            scn->stateMachine.errorInfo.srcPositionLine = scn->hist.tokens[scn->hist.count-1].srcLine;
        }
    } else {
        if(!scn->isNextValid){
            int originalCol = scn->stateMachine.errorInfo.srcPositionColumn;
            int originalLine = scn->stateMachine.errorInfo.srcPositionLine;
            stateMachineDriver(&scn->stateMachine, &scn->next);
            checkError(scn);
            scn->newCol = scn->stateMachine.errorInfo.srcPositionColumn;
            scn->newLine = scn->stateMachine.errorInfo.srcPositionLine;
            scn->stateMachine.errorInfo.srcPositionColumn = originalCol;
            scn->stateMachine.errorInfo.srcPositionLine = originalLine;
            scn->isNextValid = true;
        }
    }
}

/* Scanner auxiliary functions */
const char * getTokenName(enum TokenType tk_type) {
    return token_names[tk_type];
}

const char * getKWName(enum KeywordType keyword) {
    return keywords[keyword];
}

void checkError(Scanner *scn) {
    enum ScannerErrorStatus status = scn->stateMachine.errorInfo.errorStatus;
    if(status != SCANNER_OK){
        if(status >= SCANNER_UNEXPECTED_SYMBOL) {
            fprintf(stderr, "Lexical error: \n");
            errorInfoPrintErrorMessage(&(scn->stateMachine.errorInfo));
            // Exit code for unsuccessful parsing
            exit(1);
        } else {
            fprintf(stderr, "Memory allocation error.\n");
            exit(99);
        }
    } else {
        return;
    }
}

/* Main StateMachine functions */
void stateMachineInitializeBeforeRun(StateMachine *sm) {
    sm->handle = SMInitialState; 
    sm->hasFinishedReading = false;
    sm->isTokenValid = false;
}

void stateMachineDriver(StateMachine *sm, Token *tk) {
    stateMachineInitializeBeforeRun(sm);

    int input_char;
    while (!sm->hasFinishedReading) {
        if (sm->hasExtraChar) {
            input_char = sm->extraChar;
            sm->hasExtraChar = false;
        } else {
            input_char = stateMachineReadStdinChar(sm);
        }

        if(input_char == EOF){
            sm->outputToken.type = TOKEN_EOF;
            sm->isTokenValid = true;
            sm->hasFinishedReading = true;
        } else {
            (sm->handle)(sm, input_char); // Process next symbol
        }
    }
    
    // Check whether the machine did produce valid token, or it encoutered an error
    if (sm->isTokenValid) {
        *tk = sm->outputToken;
    } 
}

void stateMachineStoreExtraSymbol(StateMachine *sm, char sym) {
    if (sm->hasExtraChar) {
        fprintf(stderr, "FSM is trying to store more than one extra char\n");
        exit(99);
    }
    sm->extraChar = sym;
    sm->hasExtraChar = true;
}

int stateMachineReadStdinChar(StateMachine *sm) {
    int c = getc(stdin);
    if(c != EOF)
        errorInfoUpdateInputAdvanced(&sm->errorInfo, c);
    return c;
}

/* StateMachine auxiliary functions */
bool readUntil(StateMachine *sm, char s) {
    int c;
    while((c = stateMachineReadStdinChar(sm)) && c != s && c != EOF){}
    return c != EOF;
}

bool isKeyword(char *identifier, enum KeywordType *keyword) {
    for(int i = 0; i < KEYWORD_COUNT; i++) {
        if(strcmp(identifier, keywords[i]) == 0){
            *keyword = i;
            return true;
        }
    }
    return false;
}

/* StateMachine states */
void SMInitialState(StateMachine *sm, char sym) {
    if (isspace(sym)) {
        if(sym == '\n') {
            sm->outputToken.type = TOKEN_EOL;
            sm->isTokenValid = true;
            sm->hasFinishedReading = true;
        } else {
            sm->handle = SMInitialState;
        }
        return;
    }

    if(isalpha(sym) || sym == '_'){
        sm->handle = SMIdentifier;
        stateMachineStoreExtraSymbol(sm, sym);
    } else if (isdigit(sym)){
        if (sym == '0') {
            sm->handle = SMZero;
        } else {
            sm->handle = SMInteger;
        }
        stateMachineStoreExtraSymbol(sm,sym);
    } else {
        switch (sym) {
            case '>':
                sm->handle = SMReadOpRelGreater; // The function itself will handle token building
                break;
            case '<':
                sm->handle = SMReadOpRelLess; // The function itself will handle token building
                break;
            case '/':
                sm->handle = SMReadOpDiv;
                break;
            case '=':
                sm->handle = SMReadOpAssignOrEq;
                break;
            case ':':
                sm->handle = SMReadOpDefine;
                break;
            case '!':
                sm->handle = SMReadNotEq;
                break;
            case '"':
                sm->errorInfo.errorStatus = strInit(&sm->symbolAccumulator);
                verifyMalloc();
                sm->handle = SMString;
                break;
            case '+':
                sm->outputToken.type = TOKEN_OP_ARIT_PLUS;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case '-':
                sm->outputToken.type = TOKEN_OP_ARIT_MINUS;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case '*':
                sm->outputToken.type = TOKEN_OP_ARIT_TIMES;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case '(':
                sm->outputToken.type = TOKEN_SEP_L_BRACKET;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case ')':
                sm->outputToken.type = TOKEN_SEP_R_BRACKET;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case '{':
                sm->outputToken.type = TOKEN_SEP_L_CURLY;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case '}':
                sm->outputToken.type = TOKEN_SEP_R_CURLY;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case ',':
                sm->outputToken.type = TOKEN_SEP_COMMA;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            case ';':
                sm->outputToken.type = TOKEN_SEP_SEMICOLON;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
                break;
            default:
                errorInfoStoreErrorDetails(&(sm->errorInfo), "No such token exists.\n");
                sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
                sm->isTokenValid = false;
                sm->hasFinishedReading = true;
                break;
        }
    }
}

void SMReadOpDiv(StateMachine *sm, char sym) {
    switch(sym){
        case '/':
            if(readUntil(sm, '\n'))
                sm->handle = SMInitialState;
            else {
                sm->outputToken.type = TOKEN_EOF;
                sm->isTokenValid = true;
                sm->hasFinishedReading = true;
            }
            break;
        case '*':
            sm->handle = SMMultiComment;
            break;
        default:
            sm->outputToken.type = TOKEN_OP_ARIT_DIV;
            sm->isTokenValid = true;
            sm->hasFinishedReading = true;
            stateMachineStoreExtraSymbol(sm, sym);
    }
}

void SMMultiComment(StateMachine *sm, char sym) {
    if(readUntil(sm, '*') || sym == '*') {
        sm->handle = SMMultiCommentEnd;
    } else {
        errorInfoStoreErrorDetails(&sm->errorInfo, "Multi line comment is not ended.");
        sm->outputToken.type = TOKEN_EOF;
        sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
        sm->isTokenValid = false;
        sm->hasFinishedReading = true;
    }
}

void SMMultiCommentEnd(StateMachine *sm, char sym) {
    switch(sym) {
        case '/':
            sm->handle = SMInitialState;
            break;
        case '*':
            sm->handle = SMMultiCommentEnd;
            break;
        default:
            sm->handle = SMMultiComment;
    }
}

void SMIdentifier(StateMachine *sm, char sym) {
    char *name = malloc(sizeof(char)*2);
    if (name == NULL) {
        errorInfoStoreErrorDetails(&(sm->errorInfo), "Cannot allocate enough space for identifier.\n");
        sm->errorInfo.errorStatus = SCANNER_MALLOC_FAILED;
        sm->isTokenValid = false;
        sm->hasFinishedReading = true;
        return;
    }

    int index = 0, size = 2, increment = 2;
    int c = sym;
    while(isalnum(c) || c == '_') {
        if(index >= size - 1) {
            if(increment < MAX_REALLOC_INCREMENT) {
                increment *= 2;
            }
            size += increment;
            
            char *newName = realloc(name, sizeof(char)*(size));
            if(newName == NULL) {
                free(name);
                errorInfoStoreErrorDetails(&(sm->errorInfo), "Cannot allocate enough space for identifier.\n");
                sm->errorInfo.errorStatus = SCANNER_REALLOC_FAILED; 
                sm->isTokenValid = false;
                sm->hasFinishedReading = true;
                return;
            } else {
                name = newName;
            }

        }
        name[index++] = c;
        c = stateMachineReadStdinChar(sm);
    }
    name[index] = '\0';

    // If c is EOF, then it would be easier to read it again on next call to readChar
    if(c != EOF)
        stateMachineStoreExtraSymbol(sm, c);

    enum KeywordType keyword;
    if(isKeyword(name, &keyword)) {
        sm->outputToken.type = TOKEN_KEYWORD;
        sm->outputToken.data.keyword = keyword;
        free(name);
    } else {
        sm->outputToken.type = TOKEN_IDENTIF;
        // TODO Insert to symbol table
        sm->outputToken.data.identifierName = name;
    }
    sm->isTokenValid = true;
    sm->hasFinishedReading = true;
}

void SMReadOpRelGreater(StateMachine *sm, char sym) {
    switch (sym) {
        case '=':
            sm->outputToken.type = TOKEN_OP_REL_GREATEREQ;
            break;
        default:
            // If anything else comes, Rel operator was already matched 
            sm->outputToken.type = TOKEN_OP_REL_GREATER;
            sm->isTokenValid = true;
            // But we have a symbol that was not processed
            stateMachineStoreExtraSymbol(sm, sym);
    } 

    // Since we already saw '>' we emit token either way
    sm->isTokenValid = true;
    sm->hasFinishedReading = true; 
}

void SMReadOpRelLess(StateMachine *sm, char sym) {
    switch (sym) {
        case '=':
            sm->outputToken.type = TOKEN_OP_REL_LESSEQ;
            break;
        default:
            // If anything else comes, Rel operator was already matched 
            sm->outputToken.type = TOKEN_OP_REL_LESS;
            // But we have a symbol that was not rocessed
            stateMachineStoreExtraSymbol(sm, sym);
    } 
    
    // Since we already saw '<' we emit token either way
    sm->isTokenValid = true;
    sm->hasFinishedReading = true; 
}

void SMReadOpAssignOrEq(StateMachine *sm, char sym) {
    // We have already seen '='
    switch (sym) {
        case '=':
            // In case of second '=', we got relational operator ==
            sm->outputToken.type = TOKEN_OP_REL_EQ; 
            break;
        default:
            // We were only able to read '=' (in SMInitialState)
            sm->outputToken.type = TOKEN_OP_ASSIGN; 
            stateMachineStoreExtraSymbol(sm, sym); // We got symbol that we cannot consume
    }
    // If we encountered =<??> or ==, we have been able to produce token
    sm->hasFinishedReading = true;
    sm->isTokenValid = true;
}

void SMReadNotEq(StateMachine *sm, char sym) {
    // We have already seen !
    switch (sym) {
        case '=': 
            sm->outputToken.type = TOKEN_OP_REL_NOTEQ;
            sm->isTokenValid = true;
            break;
        default:
            // We didn't recognize token !<??>
            sm->isTokenValid = false;
            errorInfoStoreErrorDetails(&(sm->errorInfo), "Unexpected symbol while parsing '!='.\n");
            sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
    }
    sm->hasFinishedReading = true;
}

void SMReadOpDefine(StateMachine *sm, char sym) {
    // We have already seen :
    switch (sym) {
        case '=': 
            sm->outputToken.type = TOKEN_OP_DECLARE;
            sm->isTokenValid = true;
            break;
        default:
            sm->isTokenValid = false;
            errorInfoStoreErrorDetails(&(sm->errorInfo), "Unexpected symbol while parsing ':='\n");
            sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
    }
    sm->hasFinishedReading = true;
}

void SMZero(StateMachine *sm, char sym) {
    sm->errorInfo.errorStatus = strInit(&sm->symbolAccumulator);
    verifyMalloc(); 

    sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, sym);
    verifyRealloc();
    int c = sym; //in case we read EOF
    c = stateMachineReadStdinChar(sm);
    
    if (c == '.'){
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
        verifyRealloc();
        sm->handle = SMExpectDecimalPart;
    } else if (c == 'e' || c == 'E') {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
        verifyRealloc();
        sm->handle = SMExpectExponent;
    } else {

        if (isdigit(c)) { //zero at the begining of a number isnt valid
            strFree(&sm->symbolAccumulator);
            sm->isTokenValid = false;
            sm->hasFinishedReading = true;
            errorInfoStoreErrorDetails(&(sm->errorInfo), "Zero at the beginning of an integer is not allowed.\n");
            sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
            return;
        }

        if (c != EOF){
            stateMachineStoreExtraSymbol(sm, c);
        }

        strFree(&sm->symbolAccumulator);

        sm->outputToken.type = TOKEN_LIT_INT;
        sm->outputToken.data.intValue = 0; 
        sm->isTokenValid = true;
        sm->hasFinishedReading = true;
    }

}

void SMInteger(StateMachine *sm, char sym) {
    sm->errorInfo.errorStatus = strInit(&sm->symbolAccumulator);
    verifyMalloc();

    int c = sym; //in case we read EOF

    char *endptr = sm->symbolAccumulator.str;
    unsigned long long int converted_number;

    while (isdigit(c)) {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
        verifyRealloc();
        c = stateMachineReadStdinChar(sm);
    }
    
    switch (c) {
        case '.':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c); 
            verifyRealloc();
            sm->handle = SMExpectDecimalPart;
            break;
        case 'e':
        case 'E':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c); 
            verifyRealloc();
            sm->handle = SMExpectExponent;
            break;
        default:
            converted_number = strtoull(sm->symbolAccumulator.str, &endptr, 10);
            if (errno == ERANGE) {
                strFree(&sm->symbolAccumulator);
                sm->isTokenValid = false;
                sm->hasFinishedReading = true;
                errorInfoStoreErrorDetails(&(sm->errorInfo), "Integer overflow.\n");
                sm->errorInfo.errorStatus = SCANNER_OVERFLOW;
                break;
            }

            if (c != EOF){
                stateMachineStoreExtraSymbol(sm, c);
            }

            strFree(&sm->symbolAccumulator);

            sm->outputToken.type = TOKEN_LIT_INT;
            sm->outputToken.data.intValue = (uint64_t) converted_number;
            sm->isTokenValid = true;
            sm->hasFinishedReading = true;
            break;
    }
}

void SMExpectDecimalPart(StateMachine *sm, char sym) {
    if (isdigit(sym)) {
        //it is not necessary to append this numer in this state
        stateMachineStoreExtraSymbol(sm, sym);
        sm->handle = SMReal;
    } else {
        strFree(&sm->symbolAccumulator);
        sm->isTokenValid = false;
        sm->hasFinishedReading = true;
        errorInfoStoreErrorDetails(&(sm->errorInfo), "Expected digit after decimal part.\n");
        sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
    }
}

void SMExpectExponent(StateMachine *sm, char sym) {
    if (isdigit(sym) || sym == '+' || sym == '-') {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, sym); 
        verifyRealloc();
        sm->handle = SMRealExponent; 
    } else {
        strFree(&sm->symbolAccumulator);
        sm->isTokenValid = false;
        sm->hasFinishedReading = true;
        errorInfoStoreErrorDetails(&(sm->errorInfo), "Expected exponent part (ie. +, -, [0-9]).\n");
        sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
    }
}

void SMReal(StateMachine *sm, char sym) {
    int c = sym;
    double converted_number;
    char *endptr = sm->symbolAccumulator.str;

    while (isdigit(c)) {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
        verifyRealloc();
        c = stateMachineReadStdinChar(sm);
    }

    switch (c) {
        case 'e':
        case 'E':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
            verifyRealloc();
            sm->handle = SMExpectExponent;
            break;
        default:
            converted_number = strtod(sm->symbolAccumulator.str, &endptr);
            if (errno == ERANGE) {
                strFree(&sm->symbolAccumulator);
                sm->isTokenValid = false;
                sm->hasFinishedReading = true;
                errorInfoStoreErrorDetails(&(sm->errorInfo), "Floating point overflow.\n");
                sm->errorInfo.errorStatus = SCANNER_OVERFLOW;
                break;
            }

            if (c != EOF){
                stateMachineStoreExtraSymbol(sm, c);
            }

            strFree(&sm->symbolAccumulator);

            sm->outputToken.type = TOKEN_LIT_FLOAT;
            sm->outputToken.data.floatValue = converted_number;
            sm->isTokenValid = true;
            sm->hasFinishedReading = true;
            break;
    }
}

void SMRealExponent(StateMachine *sm, char sym) {
    int c = sym; 

    while (isdigit(c)) {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c); 
        verifyRealloc();
        c = stateMachineReadStdinChar(sm);
    }

    double converted_number;
    char *endptr = sm->symbolAccumulator.str;

    converted_number = strtod(sm->symbolAccumulator.str, &endptr);
    if (errno == ERANGE){
        strFree(&sm->symbolAccumulator);
        sm->isTokenValid = false;
        sm->hasFinishedReading = true;
        errorInfoStoreErrorDetails(&(sm->errorInfo), "Floating point overflow.\n");
        sm->errorInfo.errorStatus = SCANNER_OVERFLOW;
        return;
    }

    if (c != EOF) {
        stateMachineStoreExtraSymbol(sm, c);
    }

    strFree(&sm->symbolAccumulator);

    sm->outputToken.type = TOKEN_LIT_FLOAT;
    sm->outputToken.data.floatValue = converted_number;
    sm->isTokenValid = true;
    sm->hasFinishedReading = true;

}

void SMString(StateMachine *sm, char sym){
    int c = sym;

    while (c >= ' ' && c != '\\' && c != '"') {
        sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c);
        verifyRealloc();
        c = stateMachineReadStdinChar(sm);
    }

    switch (c) {
        case '"':
            sm->handle = SMStringEnd; 
            break;
        case '\\':
            sm->handle = SMEscape;
            break;
        default:
            strFree(&sm->symbolAccumulator);
            sm->isTokenValid = false;
            sm->hasFinishedReading = true;
            errorInfoStoreErrorDetails(&(sm->errorInfo), "Unexpected symbol while parsing string literal.\n");
            sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
            break;
    }
}

void SMEscape(StateMachine *sm, char sym){
    char hexstr[3] = "xx\0";
    int c;

    switch (sym) {
        case '"':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, sym); 
            verifyRealloc();
            sm->handle = SMString;
            break;
        case 'n':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, '\n'); 
            verifyRealloc();
            sm->handle = SMString;
            break;
        case '\\':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, '\\'); 
            verifyRealloc();
            sm->handle = SMString;
            break;
        case 't':
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, '\t'); 
            verifyRealloc();
            sm->handle = SMString;
            break;
        case 'x':

            for (int i = 0; i < 2; i++) { 
                c = stateMachineReadStdinChar(sm);
                if ( isdigit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') ) {
                    hexstr[i] = c;
                } else {
                    strFree(&sm->symbolAccumulator);
                    sm->isTokenValid = false;
                    sm->hasFinishedReading = true;
                    errorInfoStoreErrorDetails(&(sm->errorInfo), "Expected hex symbol.\n");
                    sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
                    return; 
                }
            }

            c = (int)strtol(hexstr, (char **)NULL, 16);
            sm->errorInfo.errorStatus = strAddChar(&sm->symbolAccumulator, c); 
            verifyRealloc();
            sm->handle = SMString;
            break;
        default:
            strFree(&sm->symbolAccumulator);
            sm->isTokenValid = false;
            sm->hasFinishedReading = true;
            errorInfoStoreErrorDetails(&(sm->errorInfo), "Unexpected escape sequence.\n");
            sm->errorInfo.errorStatus = SCANNER_UNEXPECTED_SYMBOL;
    }
}

void SMStringEnd(StateMachine *sm, char sym){
    sm->outputToken.type = TOKEN_LIT_STR;
    sm->outputToken.data.strValue = sm->symbolAccumulator.str;
    stateMachineStoreExtraSymbol(sm, sym);
    sm->isTokenValid = true;
    sm->hasFinishedReading = true;
}
