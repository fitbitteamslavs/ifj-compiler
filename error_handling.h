/* 
 * Subor error_handling.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xbalaz12 - Peter Balážec
 */
#ifndef IFJ20_SCANNER_ERROR_HANDLER_H
#define IFJ20_SCANNER_ERROR_HANDLER_H

#include "scanner_errinfo.h"
#include "scanner.h"

/// Scanner instance from syntax.c
extern Scanner scn;


/**
 * Prints given error message together with position in file and exits program with exit code 2
 */
void syntaxError(const char *msg);

/**
 * This function prints semantic error and exits with given code.
 * @param msg Is addtional message to given semantic error.
 */
void semanticError(int exitcode, const char *msg);

/**
 * When internal error occures, this function should be called.
 * It exits program with exit code 99 and prints given message.
 * @param msg String with details to the error that occured.
 */
void internalError(const char *msg);


/* Helper functions */

/**
 * This function prints current statement and the ASCII arrow that points to the error.
 * It uses Scanner scn instance to get all the information.
 * Sometimes the arrow points wrong.
 */
void printCurrentStatement();
/**
 * Generates string for syntaxError() giving what was expected and actual token.
 * @return string with error message
 */
char *generateUnexpectedTokenString(enum TokenType expected, enum TokenType actual);
/**
 * Generates string for syntaxError() giving what keyword was expected, but what was actually read.
 * @return string with error message
 */
char *generateUnexpectedKWString(enum KeywordType expected, enum TokenType type, enum KeywordType actual);


#endif
