/* 
 * Subor expression.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xhecko02 - Michal Hečko
 *  xlacko09 - Dávid Lacko
 */

#ifndef IFJ20_EXPRESSION_H 
#define IFJ20_EXPRESSION_H  

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include "scanner.h"

#define FUNCALL_ARGS_INITIAL_CAPACITY 2
#define EXPR_ERRROR_INVALID_TOKEN_MSG_FORMAT "Error while parsing expression, expected token '%s' but found '%s'\n"
#define EXPR_ERROR_UNEXPECTED_TOKEN "Error while parsing expression, unexpected token %s\n"
#define EXPRESSION_STACK_INITIAL_CAPACITY 16
#define MIN_OPERATOR_PRIORITY -1000

// Verbose, but clear when doin error handling
enum FuncCallStatus {
    FUNCALL_OK,
    FUNCALL_MALLOC_FAILED,
};

enum ExprErrorType {
    EXPR_ERROR_MALLOC,
    EXPR_ERROR_SYNTAX
};

struct ExprErrorDetails {
    enum ExprErrorType errorType;   // Why did the expr failed to build?
    char *errorDetails;             // Heap allocated string containing message.
    bool ownsErrorMessage;
};

enum ExprType {
    EXPR_LITERAL,
    EXPR_FUNCALL,
    EXPR_BINARY,
    EXPR_IDENTIFIER,
};

enum BinaryExprType {
    BIN_EXPR_ADD,
    BIN_EXPR_SUB,
    BIN_EXPR_MUL,
    BIN_EXPR_DIV,

    BIN_EXPR_EQ, // ==
    BIN_EXPR_NE, // !=
    BIN_EXPR_LT, // <
    BIN_EXPR_LE, // <=
    BIN_EXPR_GT, // >
    BIN_EXPR_GE, // >=
};

typedef struct Expression Expr;  // We need this in order to avoid forward declaration

struct BinaryExpression {
    Expr *left;
    Expr *right;
    enum BinaryExprType type;
};

enum LiteralExprType {
    LIT_EXPR_FLOAT,
    LIT_EXPR_INT,
    LIT_EXPR_STR
};

struct LiteralExpr {
    enum LiteralExprType type;    
    union {
        unsigned intValue;
        double floatValue;
        char *strValue;
    };
};

struct IndentifExpr {
    char *name;
};

struct FuncCallExpr {
    char *funcName;
    unsigned argCount;
    Expr** args;

    unsigned argCapacity;
};

struct Expression {
    union {
        struct BinaryExpression binaryExpr;
        struct LiteralExpr literal;
        struct FuncCallExpr funcCall;
        struct IndentifExpr identifier;
    };
    enum ExprType type;
};

/**
 * Helper function. Wraps given literal into Expression structure, so it
 * can be further used directly as an expression.
 *
 * @param value The (null-terminated) value of the literal to be wrapped in Expression.
 */
Expr *makeStrLiteralExpr(char *value); 

/**
 * Helper function. Wraps given literal into Expression structure, so it
 * can be further used directly as an expression.
 *
 * @param value The value of the literal to be wrapped in Expression.
 */
Expr *makeFloatLiteralExpr(double value); 

/**
 * Helper function. Wraps given literal into Expression structure, so it
 * can be further used directly as an expression.
 *
 * @param value The value of the literal to be wrapped in Expression.
 */
Expr *makeIntLiteralExpr(unsigned long long value); 

/**
 * Helper function. Wraps given identifier into Expression structure, so it
 * can be further used directly as an expression.
 *
 * @param value The value of the literal to be wrapped in Expression.
 */
Expr *makeIdentifExpr(char *name); 

/**
 * Joins the two expressions (left and right) into one, representing some
 * form of binary operation between them. (For exaple +)
 * 
 * @param type  Type of the operation between the two expressions.
 * @param left  The operand which is on the left side of operator.
 * @param right The operand which is on the right side of operator.
 *
 * @returns A expression of type EXPR_BINARY which represent the operation or
 *          when memory acquisition fails, returns NULL.
 */
Expr *makeBinaryExpr(enum BinaryExprType type, Expr *left, Expr *right);

/**
 * Initializes the given `func` so it can be used.
 *
 * @param func Structure to be initialized.
 * @returns - FUNCALL_OK if there was no problem.
            - FUNCALL_MALLOC_FAILED should the memory acquisition fail.
 */
enum FuncCallStatus funcCallExprInit(struct FuncCallExpr *func);

/**
 * Adds the given argument arg to the funcall.
 *
 * @param func Struture containing information about function call.
 * @param arg  Expression to be added as the next argument to the function call.
 * @returns - FUNCALL_OK if there was no problem.
 *          - FUNCALL_MALLOC_FAILED should the memory acquisition fail.
 */
enum FuncCallStatus funcCallExprAddArgument(struct FuncCallExpr *func, Expr *arg);

/**
 * Sets the name of the function, which call is being contructed.
 *
 * @param func          Struture containing information about function call.
 * @param func_name     Name of the function that is being called.
 */
void funcCallExprSetFuncName(struct FuncCallExpr *func, char *func_name);

/**
 * Releases the memory resources owned by the function call structure.
 */
void funcCallExprFree(struct FuncCallExpr *func);

/**
 * Releases the memory which is owned by given @p expr.
 * Note that there is no restriction on the type of Expresion given,
 * so even if the expr should represent some form of complicated operation,
 * all memory will be released correctly.
 *
 * @param expr Expression whose memory shall be released.
 */
void exprFree(Expr *expr);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that the token is not valid.
 * @param expr_error Structure to be filled with information about an error.
 * @param expected The type of a token which was expect.
 * @param actual The type of a token which was found.
 */
void setErrorMessageForInvalidToken(
        struct ExprErrorDetails *expr_error,
        enum TokenType expected, 
        enum TokenType actual);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that expr parsing process is missing an expression.
 * @param expr_error Structure to be filled with information about an error.
 */
void setErrorMessageForMissingExpr(struct ExprErrorDetails *err_details);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that memory needed to successfully parse an expression
 * could not be allocated for some reason.
 * @param expr_error Structure to be filled with information about an error.
 */
void setErrorMallocFailed(struct ExprErrorDetails *err_details);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that during the expression parsing process there 
 * there were more brackets of type @p unbalaned_bracket_type that their counterparts.
 *
 * @param expr_error Structure to be filled with information about an error.
 * @param unbalaned_bracket_type The type of bracket there was more of that it should be.
 */
void setErrorForUnbalancedBrackets(
        struct ExprErrorDetails *details, 
        enum TokenType unbalaned_bracket_type);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that during the expression parsing process there 
 * there were was a token which couldn't be contained in a valid expression.
 *
 * @param expr_error Structure to be filled with information about an error.
 * @param token Token which was unexpected.
 */
void setErrorForUnexpectedToken(
        struct ExprErrorDetails *details, 
        enum TokenType token);

/**
 * Helper function. Sets up the given @p expr_error in such a way that it contain
 * all information needed to inform that during the expression parsing process there 
 * were two expressions that were missing an operator between them.
 *
 * @param expr_error Structure to be filled with information about an error.
 */
void setErrorMessageForMissingOperator(struct ExprErrorDetails *details);


/*
 * ------ EXPRESSION STACK ------
 */

/**
 * Enum containg all types of items which can be stored in ExpressionStack.
 */
enum ExpressionStackItemType {
    EXPR_STACK_ITEM_TYPE_TERMINAL,
    EXPR_STACK_ITEM_TYPE_EXPRESSION,
};

struct ExpressionStackItem {
    enum ExpressionStackItemType type;  
    union {
        Expr *expr;
        enum TokenType terminal; 
    };
};

typedef struct ExpressionStackItem ExpressionStackItem;


struct ExpressionStack {
    ExpressionStackItem *contents; // Array of contents 
    unsigned firstFreeSlot;
    unsigned capacity;
};

typedef struct ExpressionStack ExpressionStack;

/**
 * Enum containing all status kinds which may be result of an operation with
 * expression stack.
 */
enum ExpressionStackStatus {
    EXPR_STACK_STATUS_OK,
    EXPR_STACK_STATUS_MALLOC_FAILED,
    EXPR_STACK_STATUS_REALLOC_FAILED,
};

/**
 * Initialized the structure so it can be safely used.
 * @param stack Structure which sould be initialized.
 *
 * @returns 
 *  - EXPR_STACK_STATUS_OK              If there was no error or
 *  - EXPR_STACK_STATUS_MALLOC_FAILED   If the memory allocation failed.
 */
enum ExpressionStackStatus expressionStackInit(struct ExpressionStack *stack);

/**
 * Releases the resources held by @p stack.
 *
 * @param stack Structure which will have its resources freed.
 */
void expressionStackDestroy(struct ExpressionStack *stack);

/**
 * Predicate. Returns true should the stack contain no items.
 *
 * @param stack Stack about which the information should be computed.
 * @returns true should the stack be empty
 *          false should the contain items.
 */
bool expressionStackEmpty(struct ExpressionStack *stack);

/**
 * Attempts to resize internal structures so more items can be fit inside the stack.
 *
 * @param stack Stack which needs more memory.
 * @returns 
 *  - EXPR_STACK_STATUS_OK               If there was no error or
 *  - EXPR_STACK_STATUS_REALLOC_FAILED   If the memory allocation failed.
 */
enum ExpressionStackStatus expressionStackGrow(ExpressionStack *stack);


/**
 * Stores the given expression on stack.
 *
 * @param stack     The stack onto which the expression should be stored.
 * @param expr      The expression to be stored.
 * @returns 
 *  - EXPR_STACK_STATUS_OK               If there was no error or
 *  - EXPR_STACK_STATUS_REALLOC_FAILED   If the memory allocation failed.
 */
enum ExpressionStackStatus expressionStackPushExpression(ExpressionStack *stack, Expr *expr);

/**
 * Stores the given terminal on stack.
 *
 * @param stack             The stack onto which the expression should be stored.
 * @param terminal_type     The type of terminal to be stored on the stack.
 * @returns 
 *  - EXPR_STACK_STATUS_OK               If there was no error or
 *  - EXPR_STACK_STATUS_REALLOC_FAILED   If the memory allocation failed.
 */
enum ExpressionStackStatus expressionStackPushTerminal(ExpressionStack *stack, enum TokenType terminal_type);

/**
 * Stores the information on top of the stack into @p dest and decreases stack top.
 *
 * @param stack Stack from which the topmost element should be removed.
 * @param dest  Destination where the topmost element's infomration should be stored.
 */
void expressionStackPop(struct ExpressionStack *stack, ExpressionStackItem *dest);

/**
 * Returns the type of the terminal which is closest to stack's top.
 *
 * @param stack Stack whose contents are in question.
 * @returns A valid token type of therminal which is closest to top if there were any terminals on stack,
 *          or -1 should there be no terminals (or the stack was empty)
 */
enum TokenType expressionStackGetTopTerminal(ExpressionStack *stack);

/**
 * Stores the information that is stored on stack top in @p dest.
 *
 * @param stack Stack whose contents are in question.
 * @param dest  Destination where the information of topmost item should be stored.
 */
void expressionStackPeekTop(ExpressionStack *stack, ExpressionStackItem *dest);

/**
 * Provides information about the number of items stored on stack.
 *
 * @param stack Stack whose contents are in question.
 * @returns The number of items stored on stack.
 */
unsigned expressionStackSize(ExpressionStack *stack);

/**
 * Helper predicate, which returns true if the topmost item on stack is a binary operator.
 * 
 * @param stack Stack whose contents are in question.
 * @returns true should the topmost item be a binary operand, false otherwise.
 */
bool expressionStackIsTopBinaryOperator(ExpressionStack *stack);

/**
 * Helper predicate, which returns true if the topmost item on stack is an expression.
 * 
 * @param stack Stack whose contents are in question.
 * @returns true should the topmost item be an expression, false otherwise.
 */
bool expressionStackIsTopExpression(ExpressionStack *stack);

/**
 * Helper function. Allows to examine the contents of stack in a safe fashion.
 *
 * @param stack Stack whose contents are in question.
 * @param type The type of terminal whose presence in stack we examine.
 * @returns     True should the terminal with given type be present somewhere on the stack, 
 *              false otherwise.
 */
bool doesExpressionStackContainTerminalWithType(ExpressionStack *stack, enum TokenType type);

/**
 * Performs reduction of the stack.
 * 
 * @param stack Stack on which the reduction should be performed.
 */
void reduceTopmostExpressionOnStack(ExpressionStack *stack);

/**
 * Performs reduction of the stack, until the terminal closest to top becomes left bracket.
 * 
 * @param stack Stack on which the reduction should be performed.
 */
void reduceExpressionUntilLeftBracketFound(ExpressionStack *stack);

/**
 * Helper predicate function, which returns true if the current input on scanner could be direcly
 * reduced to trivial expression (EXPR_LITERAL, EXPR_IDENTIFIER, EXPR_FUNCALL) aka atom.
 *
 * @param scn Scanner.
 * @returns true if the current token can be reduced to trivial expression, false otherwise.
 */
bool isAtom(Scanner *scn);

/**
 * Helper predicate function, which returns true if the given token type represent binary operator.
 *
 * @param type The type of the token in question.
 * @returns true if the current token is in fact a binary operator, false otherwise.
 */
bool isTokenBinaryOperator(enum TokenType type);

/**
 * Returns a type of a binary expression which would result when applying a token of given type
 * (which represents a binary operator). Use with `isTokenBinaryOperator`.
 *
 * @param type Type of token which encodes binary operation of some form.
 * @returns Type of binary expression which would result as application of binary operator with given type.
 */
enum BinaryExprType getBinaryExprTypeFromTokenType(enum TokenType type);

/**
 * Returns the integer priority for given binary operator of given type.
 *
 * @param type Type of binary operator.
 * @returns Integer priority of binary operator with type @p type.
 */
int getTerminalPriority(enum TokenType type);

/**
 * Helper function. Reads and drops all tokens from scanner while they match given tk_type.
 * 
 * @param scanner Scanner.
 * @param tk_type The type of a token which should be skipped.
 */
void skipAllTokensWithType(Scanner *scanner, enum TokenType tk_type);

/**
 * Converts the token(s) which are currently seen by scanner into a trivial expression.
 * Trivial expression means an Expression which encodes:
 *  - string/int/float literal
 *  - function call
 *  - identifier
 *
 *  @param scanner Scanner.
 *
 *  @returns A pointer to a valid heap allocated expression constructed from scanner inputs or
 *           NULL should the input be mallformed (cannor parse a trivial expression) or the memory allocation
 *           failed.
 *           When such error occurres the global variable _err_details will be filled out with the error details.
 */
Expr *parseAtom(Scanner *scanner);


/**
 * Parses function call into an Expression.
 *
 * @param scn Scanner from which the expression tokens will be read.
 *
 * @returns A valid expression encapsulating a function call, or NULL should the parsing process fail.
 *          If there was such an error, the global variable _err_details will contain information about error 
 *          details.
 */
Expr *parseFuncallIterative(Scanner *scn);


/**
 * An implementation of shift-reduce presedence praser. Parses the expression on input.
 * There might be many reasons why this procedure might fail, for example the expression could not be parsed
 * until a valid expression input. In that case the global variable _err_details will be filled out with the details
 * of the error.
 *
 * @returns A valid expression which encapsulates the input or NULL if the input doesn't contain a valid expression.
 *          If NULL is returned, global variable _err_details will contain the information about the error.
 */
Expr *parseExpressionIterative(Scanner *scn, bool inside_funcall);

/**
 * Parses an expression from the input.
 *
 * @param scanner Source of tokens for the expression.
 * @param error_details Structure which will be filled out with the details of the error, should any occur.
 *
 * @returns A valid expression encapsulation the expression on input, or NULL should there be an error.
 *          In case of error the @p error_details will contain information about the error.
 */
Expr *parseExpr(Scanner *scn, struct ExprErrorDetails *error_details);
#endif

