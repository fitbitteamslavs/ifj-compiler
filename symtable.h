/* 
 * Subor symtable.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_SYMTABLE_H
#define IFJ20_SYMTABLE_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include "symbol.h"

#define SYMTABLE_HASH_SEED 7717ul;
#define SYMTABLE_SIZE 128
#define SYMTABLE_STACK_SENTINEL_NAME "<!SENTINEL>"
#define SYMTABLE_FREE_SYMBOL_AFTER_POP 1

enum SymtableOperationStatus {
	SYMTABLE_OK,
	SYMTABLE_MALLOC_FAILED,
    SYMTABLE_ALREADY_DEFINED
};

typedef struct SymtableBucket {
	struct Symbol *symbol;
	SymbolName symbolName;
	struct SymtableBucket *next;
	int prevSymbolBucketId; // Keep note of the push history
    int level;
} SymtableBucket;

typedef struct Symtable {
	SymtableBucket *table[SYMTABLE_SIZE];
	
	int symbolStackTopBucketId; 
    int stackLevel;
} Symtable;

SymtableBucket *symtableBucketNew(SymbolName name, Symbol *symbol, SymtableBucket *tail);
void symtableBucketFreeSingle(SymtableBucket *bucket); // #Why is there this?
void symtableBucketFreeWholeBucket(SymtableBucket  *head);

Symtable *symtableNew();
void symtableFree(Symtable *symtable);
int symtableInsert(Symtable *symtable, SymbolName symbol_name, Symbol *symbol);
Symbol *symtableLookup(Symtable *symtable, SymbolName name);
int symtablePushContext(Symtable *symtable);
int symtablePopContext(Symtable *symtable);
bool symtableIsInCurrentContext(Symtable *symtable, SymbolName name);

/**
 * Hash function used in symbol table.
 * This is an implementation of the known djb2 algorithm. 
 *
 * @param key Key to be hashed. Not checked for NULLity for speed reasons. 
 * @returns Computed hash value for provided key.
 */
static inline unsigned symtableHash(SymbolName key) {
	unsigned hash = SYMTABLE_HASH_SEED;

	char current_char = *key;
	while (current_char) {
		hash = (hash * 33) + current_char;	
		current_char = *(key++); // Advance to the next char
	}

	return hash;
}
#endif
