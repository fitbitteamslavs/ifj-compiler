/* 
 * Subor expression_debug.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_SCANNER_ERRINFO_H 
#define IFJ20_SCANNER_ERRINFO_H  
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define ERR_INFO_INITIAL_CAPACITY 128

enum ErrorInfoOperationStatus {
    ERR_INFO_OK,
    ERR_INFO_MALLOC_FAILED,
    ERR_INFO_BUFFER_RESIZE_FAILED,
    ERR_INFO_STORE_DETAILS_FAILED,
};

enum ScannerErrorStatus { 
    /* Order is important
     * This enum must match str.h StringStatus enum in 0, 1 and 2!
     */
    SCANNER_OK = 0,
    SCANNER_MALLOC_FAILED = 1,
    SCANNER_REALLOC_FAILED = 2,
    SCANNER_UNEXPECTED_SYMBOL,
    SCANNER_OVERFLOW,
};

// Error tracing
struct ErrorInfo {
    unsigned srcPositionLine;
    unsigned srcPositionColumn;

    unsigned currentLineLength;
    char *currentLineContents;
    unsigned lineContentsCapacity;

    char *errorDetails; // Error details provided by statemachine for the user
    enum ScannerErrorStatus errorStatus; // Error info about what went wrong in SM for the compiler
};

int errorInfoInit(struct ErrorInfo *err_info);
void errorInfoDestroy(struct ErrorInfo *err_info);
int errorInfoStoreLineChar(struct ErrorInfo *err_info, char c);
int errorInfoUpdateInputAdvanced(struct ErrorInfo *err_info, char c);
void errorInfoUpdateLineAdvanced(struct ErrorInfo *err_info);
void errorInfoPrintErrorMessage(struct ErrorInfo *err_info);

int  errorInfoStoreErrorDetails(struct ErrorInfo *err_info, char *msg_text);
void errorInfoClearErrorDetails(struct ErrorInfo *err_info);
#endif
