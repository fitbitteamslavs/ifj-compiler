/* 
 * Subor semantic.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xmocar00 - Peter Močáry
 */

#include "semantic.h"

const char* INSTRUCTION_SET[INST_COUNT] = {
    "MOVE", "CREATEFRAME", "PUSHFRAME", "POPFRAME", "DEFVAR", "CALL", "RETURN",

    "PUSHS", "POPS", "CLEARS",

    "ADD", "ADDS", "SUB", "SUBS", "MUL", "MULS", "DIV", "DIVS", "IDIV", "IDIVS",

    "LT", "LTS", "GT", "GTS", "EQ", "EQS",

    "AND", "ANDS", "OR", "ORS", "NOT", "NOTS",

    "INT2FLOAT", "INT2FLOATS", "FLOAT2CHAR", "FLOAT2INTS", "INT2CHAR",
    "INT2CHARS", "STRI2INT", "STRI2INTS",

    "READ", "WRITE",

    "CONCAT", "STRLEN", "GETCHAR", "SETCHAR",

    "TYPE",

    "LABEL", "JUMP", "JUMPIFEQ", "JUMPIFEQS", "JUMPIFNEQ", "JUMPIFNEQS", "EXIT",                       

    "BREAK", "DPRINT"
};

const char *TYPES[TYPES_COUNT] = {
    "float", "int", "string", "bool", "nil",    
    "GF", "LF", "TF",
};

void printCode(unsigned argc, enum InstructionSet instr, ...) {
    va_list args;
    va_start(args, instr);

    unsigned current_type;
    double current_value_float;
    uint64_t current_value_int;
    char *current_value_str;
    char *current_char;

    // Begin the instruction
    printf("%s ", INSTRUCTION_SET[instr]);

    switch (instr) {
        case INSTR_CALL:
        case INSTR_LABEL:
        case INSTR_JUMP:
        case INSTR_JUMPIFEQS:
        case INSTR_JUMPIFNEQS:
            // these instructions take label only which has no type
            current_value_str = va_arg(args, char *);
            printf(" %s", current_value_str);
            break;
        case INSTR_JUMPIFEQ:
        case INSTR_JUMPIFNEQ:
            // these instrucions take label but also need the rest of arguments processed
            current_value_str = va_arg(args, char *);
            printf(" %s", current_value_str);
            argc--;
            // missing break is inteded!
        default:
            // Add arguments to the instruction 
            while (argc > 0) {

                current_type = va_arg(args, unsigned); // Type of the argument
                
                // Get value of the argument process it according to its type and print
                switch (current_type) {
                    case TYPE_FLOAT:
                        current_value_float = va_arg(args, double);
                        printf(" float@%a", current_value_float);
                        break;
                    case TYPE_INT:
                        current_value_int = va_arg(args, uint64_t);
                        printf(" int@%"PRId64, current_value_int);
                        break;
                    case TYPE_STR:
                        current_value_str = va_arg(args, char *);
                        current_char = current_value_str;
                        printf(" string@"); //Initiate the argument by printing its type
                        while (*current_char != '\0'){ //Reformat the string value according to assignment
                            if (*current_char == '\\' || *current_char == '#' || *current_char <= ' ') {
                                printf("\\%03d", *current_char);
                            } else {
                                printf("%c", *current_char);
                            }
                            current_char++;
                        }
                        break;
                    case TYPE_BOOL:
                        current_value_int = va_arg(args, int);
                        if(current_value_int != 0) {
                            printf(" %s@true", TYPES[current_type]);
                        } else {
                            printf(" %s@false", TYPES[current_type]);
                        }
                        break;
                    default: // The other types just need to be printed in correct format
                        current_value_str = va_arg(args, char *);
                        printf(" %s@%s", TYPES[current_type], current_value_str);
                        break;
                }

                // loaded 2 args that are combined to a single one (type and value in ifjCode)
                argc -= 1;
            }
    };
    printf("\n");

    // needs to be called at the end of the function 
    va_end(args);
}
