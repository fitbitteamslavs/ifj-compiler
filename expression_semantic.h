/* 
 * Subor expression_semantic.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 */

#ifndef IFJ_EXPRESSION_SEMANTIC
#define IFJ_EXPRESSION_SEMANTIC

#include <stdlib.h>

#include "semantic.h"
#include "expression.h"
#include "error_handling.h"
#include "symbol.h"
#include "symtable.h"
#include "buildins.h"
#include "stdint.h"

void setSymtable(Symtable *st);
/**
 * Does semantic analysis and generates code for computing expressions.
 * If it finds any function that returns more than one value throws semantic error.
 * The computed value is saved on stack in DC (Destination code).
 * @return type of final result
 */
enum ValueType processExpression(Expr *expr);
/**
 * Should be called when expecting multiple or none return values.
 * @warning You have to check yourself if return values have types as expected in symtable
 * @warning Do not call with Expression other than EXPR_FUNCALL
 * @return number of return values or -1 when the passed Expression is not funcCall
 */
int processFunctionCall(Expr *expr);

struct ExprResult {
    enum ValueType type;
    bool isPropagated;
    union {
        int64_t intVal;
        double flVal;
    };
};

/* Private functions */
struct ExprResult recursiveDescent(Expr *expr);
Symbol *getIdentifierSymbol(SymbolName name);
Symbol *getFunctionSymbol(SymbolName name);
struct ExprResult processBinaryExpression(Expr *expr);


#endif
