/* 
 * Subor syntax.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_SYNTAX_H
#define IFJ20_SYNTAX_H
#include "scanner.h"
#include "buildins.h"
#include "expression.h"
#include "semantic.h"
#include "scanner_errinfo.h"
#include "error_handling.h"
#include "expression_semantic.h"
#include "symtable.h"
#include "symbol.h"
#include "datastructures.h"

#define TokenT2SymbolT(type) type - KEYWORD_FLOAT64

extern const char*const VALUE_TYPES_NAMES[];

 /* Public interface */
/**
 * Starting token of syntax analysis
 */
void program();


 /* Private functions */
unsigned numOfDigits(unsigned integer); 

/** @return true if current token is given type */
int compare(enum TokenType type);
/** @return true if token is keyword of given type */
int compareKW(enum KeywordType keyword);

/** @return true if token of given type was consumed */
int accept(enum TokenType type);
/** @return true if given keyword was consumed */
int acceptKW(enum KeywordType keyword);

/** Like accept(), but if it is not equal calls syntaxError().
 * @return true if current token is of given type.
 */
int expect(enum TokenType type);
/** Like acceptKW(), but with behaviour of expect().
 * @return true if current token is of given type.
 */
int expectKW(enum KeywordType keyword);

/**
 * Calls expression parse and frees the returned tree.
 */
enum ValueType expression();

/** Expects boolExpression, if not given calls syntaxError()
 */
void boolExpression();

Symbol* insertLastSeenIdentifierToSymtable(enum ValueType type, bool keepName);

struct TypeMissmatchInfo {
    char *identifierName;
    Symbol *identifierSymbol;
    enum ValueType actualType;
};


/**
 * Verfies, that all expression types match the type of their corresponding identifier.
 */
bool verifyExprTypesMatchExpected(
        enum ValueType *types_actual_list,
        struct IdentifierStack *identifiers,
        struct TypeMissmatchInfo *missmatch_info);

void printAssignmentTypeMissmatchError(
        struct ErrorInfo *err_info, 
        struct TypeMissmatchInfo *missmatch_info
        );

void replaceDuplicitIdentifiersWithUnderscore(struct IdentifierStack *identifier_stack);

char* mangleVariableName(
        char *current_fun_name,
        unsigned current_ctx_id,
        char *var_name
        );

void emitInstructionsForMultiAssignment(
        struct IdentifierStack *identifier_stack
        );

void emitInstrucionForExprAssignent(Symbol *symbol);
void emitFunctionVariableDeclarations(Symbol *func_symbol);

/* Checks */
bool isFunctionMissingReturnStatement(Symbol *func_symbol);

/* Non-terminal functions for recursive descend */
void functionList();
void function();
void type();
void identifN();
void defAssignStatement();
void defAssignRest();
void statement();
void block();
void blockRestN();
void paramDeclFirst();
void paramDeclSecond();
void paramDeclN();
void paramDeclList();
void packageDecl();
void typeN();
void typeList();
void function();

/**
 * @param expected_type_buffer_size the amount of expected experssions and therefore the types (of these expressions) buffer size 
 * @returns NULL if there was no expression processed or an array of value types of processed expressions
 */
enum ValueType  *expressionList(unsigned expected_count_of_types, int exit_val);

/**
 * @param type_buffer the buffer of expression types this function fills with pyes of processed expressions
 * @param expected_type_buffer_size caps the buffer size and number of expected types so we dont access unallocated memory and throw an error if there is more expressions than expected
 */
void expressionN(enum ValueType *type_buffer, unsigned expected_type_buffer_size, int exit_val);
void returnStat();
void ifStatement();
void maybeAsignDef();
void forLoop();
void program();
void functionList();
void maybeEOL();

#endif
