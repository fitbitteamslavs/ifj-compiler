/* 
 * Subor symbol.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 */

#include "symbol.h"

const char *const VALUE_TYPES_NAMES[] = {
    "float64",
    "int",
    "str",
    "bool",
    "null"
};

Symbol *createSymbol() {
    Symbol *sm = malloc(sizeof(Symbol));
    if(sm == NULL) {
        fprintf(stderr, "Error allocating symbol for symtable.\n");
        exit(99);
    }
    return sm;
}

void freeSymbol(Symbol *sm) {
    if(sm != NULL) {
        if(sm->nameInCode != NULL) {
            free(sm->nameInCode);
        }
    }
    free(sm);
}

Symbol *createFunctionSymbol() {
    Symbol *sm = createSymbol();
    sm->type = SYMBOL_TYPE_FUNCTION;
    sm->nameInCode = NULL;
    sm->functionAttrs.returnTypes = NULL;
    sm->functionAttrs.returnedValuesCount = 0;
    sm->functionAttrs.argumentTypes = NULL;
    sm->functionAttrs.argumentCount = 0;
    sm->functionAttrs.argumentNames = NULL;
    int status = stringVectorInit(&sm->functionAttrs.functionDeclarations);
    if (status == STRING_VECTOR_MALLOC_FAIL){
        fprintf(stderr, "Error allocating string vector for function symbol.\n");
        exit(99);
    } 
    return sm;
}

void addArgumentToFunction(Symbol *fn, SymbolName argName, enum ValueType type) {
    int count = fn->functionAttrs.argumentCount;
    enum ValueType *ptr = realloc(fn->functionAttrs.argumentTypes, sizeof(enum ValueType)*(count+1));
    SymbolName *newName = realloc(fn->functionAttrs.argumentNames, sizeof(SymbolName*)*(count+1));
    if(newName == NULL || ptr == NULL) {
        fprintf(stderr, "Realloc problem.\n");
        exit(99);
    }
    fn->functionAttrs.argumentTypes = ptr;
    fn->functionAttrs.argumentNames = newName;

    fn->functionAttrs.argumentTypes[count] = type;
    fn->functionAttrs.argumentNames[count] = argName;
    fn->functionAttrs.argumentCount++;
}

void addReturnTypeToFunction(Symbol *fn, enum ValueType type) {
    int count = fn->functionAttrs.returnedValuesCount;
    enum ValueType *ptr = realloc(fn->functionAttrs.returnTypes, sizeof(enum ValueType)*(count+1));
    if(ptr == NULL) {
        fprintf(stderr, "Realloc problem.\n");
        exit(99);
    }
    fn->functionAttrs.returnTypes = ptr;

    fn->functionAttrs.returnTypes[count] = type;
    fn->functionAttrs.returnedValuesCount++;
}

void setFunctionLabel(Symbol *fn, char *label) {
    fn->nameInCode = label;
}

Symbol *createVariable(enum ValueType type) {
    Symbol *sm = createSymbol();
    sm->type = SYMBOL_TYPE_VARIABLE;
    sm->nameInCode = NULL;
    sm->variableAttrs.type = type;

    return sm;
}
