/* last_identifier is already stored in symtable
 * Subor syntax.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#include "syntax.h"
#include "datastructures.h"

/// This variable is used as global object
Scanner scn;

struct ExprErrorDetails exprError;
Symtable *st;
Symbol *current_symbol;
enum ValueType last_type;
Token last_identifier;
bool expectDefinition = false;
bool expectAssignment = false;
void (*paramDecl)(void) = paramDeclFirst;
struct ContextStack context_stack;
char* current_function_name = NULL;
unsigned context_id = 0;

unsigned numOfDigits(unsigned integer) {
    unsigned digits_counter = 0;
    do {
        integer /= 10;
        digits_counter++;
    } while (integer > 0);
    return digits_counter;
}

int compare(enum TokenType type) {
    if(type == scn.current.type){
        return true;
    }
    return false;
}

int compareKW(enum KeywordType keyword) {
    if(compare(TOKEN_KEYWORD) && scn.current.data.keyword == keyword) {
        return true;
    }
    return false;
}

int accept(enum TokenType type){
    if(type == scn.current.type) {
        if(scn.current.type == TOKEN_IDENTIF) {
            last_identifier = scn.current;
        }
        getToken(&scn);
        return true;
    }
    return false;
}

int expect(enum TokenType type) {
    if(accept(type)) {
        return true;
    }
    syntaxError(generateUnexpectedTokenString(type, scn.current.type));
    return 0;
}

int acceptKW(enum KeywordType keyword) {
    if(compare(TOKEN_KEYWORD) && scn.current.data.keyword == keyword) {
        getToken(&scn);
        return true;
    }
    return false;
}

int expectKW(enum KeywordType keyword) {
    if(acceptKW(keyword))
        return true;
    syntaxError(generateUnexpectedKWString(keyword, scn.current.type, scn.current.data.keyword));
    return false;
}

 /* Non-terminal functions */
Expr *parseExpression() {
    Expr *expr = parseExpr(&scn, &exprError);
    if(expr == NULL) {
        syntaxError(exprError.errorDetails);
    }
    return expr;
}

enum ValueType expression() {
    Expr *expr = parseExpression();
    enum ValueType expr_value_type = processExpression(expr);

    exprFree(expr);
    return expr_value_type;
}

void boolExpression() {
    Expr *expr = parseExpression();
    if (processExpression(expr) != VAL_TYPE_BOOL) {
        semanticError(5, "Expected bool expression.");
    }
    exprFree(expr);
}

Symbol* insertLastSeenIdentifierToSymtable(enum ValueType type, bool keepName) {
    Symbol *sm = createVariable(type);
    char *identif_name = last_identifier.data.identifierName;

    const unsigned current_ctx_id = contextStackPeekTop(&context_stack);
    char* function_name = current_function_name;

    char *mangled_name = mangleVariableName(function_name, current_ctx_id, last_identifier.data.identifierName);
    if (mangled_name == NULL) {
        fprintf
            (stderr, 
             "Could not create code (mangled) variable name for identifier %s (insertLastSeenIdentifierToSymtable)\n",
             identif_name
            );
        internalError("");
    }

    if (keepName) {
        sm->nameInCode = last_identifier.data.identifierName;
    } else {
        sm->nameInCode = mangled_name;
    }

    int return_status = symtableInsert(st, last_identifier.data.identifierName, sm);
    if (return_status == SYMTABLE_ALREADY_DEFINED) {
        semanticError(3,"Symbol is already define.");
    } else if(return_status == SYMTABLE_MALLOC_FAILED) {
        internalError("Symtable malloc failed");
    }

    return sm;
}

void crashIfLastIdentifierNotDefined() {
    if (strcmp("_", last_identifier.data.identifierName) == 0) {}
    else if (symtableLookup(st, last_identifier.data.identifierName) == NULL) {
        fprintf(stderr, "Identifier %s not defined.\n", last_identifier.data.identifierName);
        exit(3);
    }
}


void typeNFirst() {
    do {
        if (accept(TOKEN_SEP_COMMA)) {
            maybeEOL();
            type();
            addReturnTypeToFunction(current_symbol, last_type);
        } else if (compare(TOKEN_SEP_R_BRACKET)) {
            break;
        } else {
            syntaxError("Bad return type list.\n");
        }
    } while(true);
}

void typeListFirst() {
    if (compare(TOKEN_SEP_R_BRACKET)) {
    } else {
        maybeEOL();
        type();
        addReturnTypeToFunction(current_symbol, last_type);
        typeNFirst();
    }
}


 /* Syntax analysis functions */
void program() {
    scannerInit(&scn);
    st = symtableNew();
    setSymtable(st);
    
    skipAllTokensWithType(&scn,TOKEN_EOL);
    packageDecl();
    symtableInsertBuildIns(st);
    contextStackInit(&context_stack);
    while(scn.current.type != TOKEN_EOF) {
        if (compareKW(KEYWORD_FUNC)) {
            expectKW(KEYWORD_FUNC);
            expect(TOKEN_IDENTIF);

            // Initiate the context counter along with its stack
            context_id = 0;
            contextStackClearContents(&context_stack);
            contextStackPush(&context_stack, context_id, CONTEXT_TYPE_FUNC);

            // Semantic action for pushing to symtable
            Symbol *sm = createFunctionSymbol();
            current_symbol = sm;
            current_function_name = scn.current.data.identifierName;
            setFunctionLabel(sm, current_function_name);
            int status = symtableInsert(st, current_function_name, sm);

            if(status != SYMTABLE_OK) {
                semanticError(3,"Given identifier already defined.\n");
            }

            expect(TOKEN_SEP_L_BRACKET);
            paramDeclList();
            expect(TOKEN_SEP_R_BRACKET);
            if(accept(TOKEN_SEP_L_BRACKET)) {
                typeListFirst();
                expect(TOKEN_SEP_R_BRACKET);
            }
            current_symbol = NULL;
            expect(TOKEN_SEP_L_CURLY);
        } else if (compare(TOKEN_IDENTIF) && contextStackPeekTop(&context_stack) == CONTEXT_STACK_EMPTY){
            syntaxError("Global variables are not premitted.");
        } else if (compareKW(KEYWORD_FOR)) {
            context_id++;
            contextStackPush(&context_stack, context_id, CONTEXT_TYPE_FOR_HEADER);
            getToken(&scn);
        } else if(compare(TOKEN_SEP_L_CURLY)) {
            context_id++;
            contextStackPush(&context_stack, context_id, CONTEXT_TYPE_OTHER);
            getToken(&scn);
        } else if (accept(TOKEN_OP_DECLARE)) {
            Symbol *current_function_symbol = symtableLookup(st,current_function_name);
            struct StringVector *func_decl = &current_function_symbol->functionAttrs.functionDeclarations;
            
            //Create and append new variable name
            Token decl_identifier_tk = scn.hist.tokens[scn.hist.count-3].tk;
            char *original_variable_name = decl_identifier_tk.data.identifierName;
            char *new_variable = mangleVariableName(current_function_name, contextStackPeekTop(&context_stack), original_variable_name);
            stringVectorAddString(func_decl, new_variable);
            getToken(&scn);
        } else if (accept(TOKEN_SEP_R_CURLY)) {
            contextStackPop(&context_stack);
            if(contextStackGetTopType(&context_stack) == CONTEXT_TYPE_FOR_HEADER) {
                contextStackPop(&context_stack);
            }
        } else {
            getToken(&scn);
        }
    }

    scannerSeek(&scn, 0);
    paramDecl = paramDeclSecond;

    skipAllTokensWithType(&scn, TOKEN_EOL);
    packageDecl();
    fprintf(stdout, ".IFJcode20\n");
    printCode(1, INSTR_DEFVAR, TYPE_GF, "$trash");
    printCode(1, INSTR_DEFVAR, TYPE_GF, "$strbuild1");
    printCode(1, INSTR_DEFVAR, TYPE_GF, "$strbuild2");
    printCode(0, INSTR_CREATEFRAME);
    printCode(1, INSTR_CALL, "main");
    printCode(1, INSTR_EXIT, TYPE_INT,(uint64_t) 0);

    scannerNewStatement(&scn);
    functionList();
    Symbol *mainFound = symtableLookup(st, "main");
    if(mainFound == NULL) {
        semanticError(3,"Main function is not defined!\n");
    }
    if((mainFound->functionAttrs.argumentCount != 0) || (mainFound->functionAttrs.returnedValuesCount != 0)) {
        semanticError(6, "Main cannot have any arguments or return values!\n");
    }

    for(int i = 0;i < 10;i++)
        emitCodeForBuildinFunction(i);
}

void functionList() {
    do {
        if(compareKW(KEYWORD_FUNC)) {
            function();
        } else if(accept(TOKEN_EOL)) {
            continue;
        } else if(accept(TOKEN_EOF)) {
            break;
        } else {
            syntaxError("Invalid symbol in function declaration space.\n");
            return;
        }
    } while(true);
}

void function() {
    expectKW(KEYWORD_FUNC);
    expect(TOKEN_IDENTIF);

    current_symbol = symtableLookup(st, last_identifier.data.identifierName);
    printCode(1, INSTR_LABEL, last_identifier.data.identifierName);
    current_function_name = last_identifier.data.identifierName;

    // We are entering a new context
    context_id = 0;
    contextStackClearContents(&context_stack);
    contextStackPush(&context_stack, context_id, CONTEXT_TYPE_FUNC);
    symtablePushContext(st);

    expect(TOKEN_SEP_L_BRACKET);
    maybeEOL();
    paramDeclList();
    expect(TOKEN_SEP_R_BRACKET);
    if(accept(TOKEN_SEP_L_BRACKET)) {
        typeList();
        expect(TOKEN_SEP_R_BRACKET);
    }

    printCode(0, INSTR_PUSHFRAME);

    unsigned count = current_symbol->functionAttrs.functionDeclarations.stringsCount;
    for(unsigned i = 0; i < count;i++) {
        printCode(1, INSTR_DEFVAR, TYPE_LF, current_symbol->functionAttrs.functionDeclarations.strings[i]);
    }

    block();
    printCode(0, INSTR_POPFRAME);

    if (isFunctionMissingReturnStatement(current_symbol)) {
        semanticError(6, "Function must return a value!\n");
    }

    printCode(0, INSTR_RETURN);
    expect(TOKEN_EOL);
    current_symbol = NULL;
    
    symtablePopContext(st);
    // We don't have to clear contextStack since it is cleared on every entry
}

void returnStatement() {
    expectKW(KEYWORD_RETURN);
    enum ValueType *expList = NULL;

    expList = expressionList(current_symbol->functionAttrs.returnedValuesCount, 6);
    for(unsigned i = 0; i < current_symbol->functionAttrs.returnedValuesCount; i++) {
        if (expList[i] != current_symbol->functionAttrs.returnTypes[i]) {
            semanticError(6, "Invalid types in return statements\n");
        }
    }
    current_symbol->functionAttrs.returnExists = true;
    free(expList);
    printCode(0, INSTR_POPFRAME);
    printCode(0, INSTR_RETURN);
}

void ifStatement() {
    // If statements head
    expectKW(KEYWORD_IF);
    boolExpression();
    printCode(1, INSTR_PUSHS, TYPE_BOOL, false);
     
    // Entering new context
    context_id++;
    contextStackPush(&context_stack, context_id, CONTEXT_TYPE_OTHER);
    symtablePushContext(st);

    // Create label pointing to beginning of else
    char *else_label = mangleVariableName(current_function_name, contextStackPeekTop(&context_stack), "else$"); 
    printCode(1, INSTR_JUMPIFEQS, else_label);

    // ---- Entering if block ----
    block();

    // Create  new label pointing to end of ifelse
    char *endifelse_label = mangleVariableName(current_function_name, contextStackPeekTop(&context_stack), "endifelse$");

    printCode(1, INSTR_JUMP, endifelse_label); // the bool expression was true so end the ifelse
    printCode(1, INSTR_LABEL, else_label); // the bool expression was false so end we jumped to the else part
    free(else_label);

    // Leaving the context
    symtablePopContext(st); 
    contextStackPop(&context_stack);


    // ---- else block ----
    expectKW(KEYWORD_ELSE);
    // Entering new context
    context_id++;
    contextStackPush(&context_stack, context_id, CONTEXT_TYPE_OTHER);
    symtablePushContext(st);
    
    maybeEOL();
    // Entering else block
    block();
    
    // Leaving the context
    symtablePopContext(st);
    contextStackPop(&context_stack);

    // The bool expression in the if was true so after if block we jump here
    printCode(1, INSTR_LABEL, endifelse_label);
    free(endifelse_label);
    expect(TOKEN_EOL);
}

void maybeAsignDef() {
    if(compare(TOKEN_IDENTIF)) {
        defAssignStatement();
    } else if(compare(TOKEN_SEP_SEMICOLON) || compare(TOKEN_SEP_L_CURLY)) {
    } else {
        syntaxError("Expected assignment, definition or empty.\n");
    }
}

void forLoop() {
    // Forloop head
    expectKW(KEYWORD_FOR);

    // Entering forloop head context
    context_id++;
    contextStackPush(&context_stack, context_id, CONTEXT_TYPE_FOR_HEADER);
    symtablePushContext(st);

    expectDefinition = true;
    maybeAsignDef();
    expectDefinition = false;
    expect(TOKEN_SEP_SEMICOLON);

    // Create forloop label
    char * forloop_label = mangleVariableName(current_function_name, contextStackPeekTop(&context_stack), "forloop$");
    
    printCode(1, INSTR_LABEL, forloop_label );
    boolExpression();
    printCode(1, INSTR_PUSHS, TYPE_BOOL, false);
    expect(TOKEN_SEP_SEMICOLON);
    unsigned long long pos = scn.hist.currentPosition;
    while(scn.current.type != TOKEN_SEP_L_CURLY) {
        getToken(&scn);
    }

    // Create endfor label
    char *endfor_label = mangleVariableName(current_function_name, contextStackPeekTop(&context_stack), "endfor$");
    printCode(1, INSTR_JUMPIFEQS, endfor_label); 

    // Entering forloop body context
    context_id++;
    contextStackPush(&context_stack, context_id, CONTEXT_TYPE_OTHER);
    symtablePushContext(st); 

    // Entering forloop block
    block();
    expect(TOKEN_EOL);

    // Leaving the context
    symtablePopContext(st);
    contextStackPop(&context_stack);

    unsigned long long endfor_pos = scn.hist.currentPosition;
    scannerSeek(&scn, pos);
    expectAssignment = true;
    maybeAsignDef();
    expectAssignment = false;
    scannerSeek(&scn, endfor_pos);

    // Leaving forloop head context 
    symtablePopContext(st);
    if(contextStackGetTopType(&context_stack) != CONTEXT_TYPE_FOR_HEADER) {
        fprintf(stderr, "Wrong type on stack: %d %d\n", context_stack.top, contextStackGetTopType(&context_stack));
    }
    contextStackPop(&context_stack);


    printCode(1, INSTR_JUMP, forloop_label);
    printCode(1, INSTR_LABEL, endfor_label);
    
    free(forloop_label);
    free(endfor_label);
}

void type() {
    last_type = scn.current.data.keyword - KEYWORD_FLOAT64;
    if(acceptKW(KEYWORD_INT)) {
    } else if (acceptKW(KEYWORD_STRING)) {
    } else if (acceptKW(KEYWORD_FLOAT64)) {
    } else {
        syntaxError("Wrong data type of variable.\n");
    }
}

void identifN(struct IdentifierStack *identifier_stack) {
    enum IdentifierStackStatus push_status;
    do {
        if(accept(TOKEN_SEP_COMMA)) {
            expect(TOKEN_IDENTIF);
            push_status = identifierStackPush(identifier_stack, last_identifier.data.identifierName);
            if (push_status != IDENTIF_STACK_OK) {
                internalError("MallocFail: Failed to push next identifier onto stack (in indentifN)");
            }

            // We know that only assignents can contain identifier vectors - hence they must be defined 
            crashIfLastIdentifierNotDefined(); 
            continue;
        } else if(compare(TOKEN_OP_ASSIGN)) {
            break;
        } else {
            syntaxError("Expected ',' or '='.\n");
            return;
        }
    } while(true);

}

void defAssignStatement() {

    // When assigning lhs must be IDENTIF
    if (expect(TOKEN_IDENTIF)) {
        struct IdentifierStack stack = {0};
        enum IdentifierStackStatus status = identifierStackInit(&stack);

        if (status != IDENTIF_STACK_OK) {
            internalError("Error: Could not malloc memory for identifier stack (in: defAssignStatement).");
        }

        // This will not fail, since the initial cappacity is more than 0
        identifierStackPush(&stack, last_identifier.data.identifierName);

        defAssignRest(&stack);
        identifierStackDestroy(&stack);
    }
    // If there was a different token - program crashed already
}

void defAssignRest(struct IdentifierStack *identifier_stack) {
    // Try accepting OP_DECLARE, since declaration might contain only one new identifier eg: a := ...
    if (accept(TOKEN_OP_DECLARE)) {

        if (expectAssignment) {  // <-- This is used to when requiring assignments in forloops
            syntaxError("Expected assignment.\n");
        }

        enum ValueType expr_type = expression();
        Symbol *variable_symbol = insertLastSeenIdentifierToSymtable(expr_type, false);

        emitInstrucionForExprAssignent(variable_symbol);

    } else if (compare(TOKEN_SEP_COMMA) || compare(TOKEN_OP_ASSIGN)) {

        crashIfLastIdentifierNotDefined();    

        if (compare(TOKEN_OP_ASSIGN) && expectDefinition) {
            syntaxError("Expected definition.\n");
        }

        identifN(identifier_stack); // Collects all encountered identifiers / stack_top=right_most
        expect(TOKEN_OP_ASSIGN);

        unsigned identifier_count = identifierStackGetSize(identifier_stack);

        // After this call, the values will be stored on stack from right to left
        enum ValueType *const expression_types = expressionList(identifier_count, 7);  

        struct TypeMissmatchInfo missmatch_info = {0};
        bool do_types_match = verifyExprTypesMatchExpected(expression_types, identifier_stack, &missmatch_info); 
        if (!do_types_match) {
            printAssignmentTypeMissmatchError(&scn.stateMachine.errorInfo, &missmatch_info);
            exit(7);
        }

        replaceDuplicitIdentifiersWithUnderscore(identifier_stack);

        emitInstructionsForMultiAssignment(identifier_stack);
        free(expression_types);
    } else {
        syntaxError("Expected assign, definition or function call.");
    }
}

void statement() {
    if(compare(TOKEN_IDENTIF)) {
        peekToken(&scn);
        if(scn.next.type == TOKEN_SEP_L_BRACKET){
            Expr *expr = parseExpression();
            int ret_num = processFunctionCall(expr);
            if(ret_num != 0) {
                semanticError(6, "You cannot call function with return values without assigning them.");
            }
            exprFree(expr);
            expect(TOKEN_EOL);
        } else {
            defAssignStatement();
            expect(TOKEN_EOL);
        }
    } else if (compareKW(KEYWORD_IF)) {
        ifStatement();
    } else if (compareKW(KEYWORD_FOR)) {
        forLoop();
    } else if (compareKW(KEYWORD_RETURN)) {
        returnStatement();
        expect(TOKEN_EOL);
    } else if (accept(TOKEN_EOL)){
    } else {
        syntaxError("Statement or end of block expected.\n");
    }
}

void block() {
    expect(TOKEN_SEP_L_CURLY);
    expect(TOKEN_EOL);
    scannerNewStatement(&scn);
    blockRestN();
}

void blockRestN() {
    while(!accept(TOKEN_SEP_R_CURLY)) {
        maybeEOL();
        statement();
        scannerNewStatement(&scn);
    }
}

void packageDecl() {
    expectKW(KEYWORD_PACKAGE);
    expect(TOKEN_IDENTIF);
    if((strcmp(last_identifier.data.identifierName, "main")) != 0) {
        syntaxError("Expected package main\n");
    }
    expect(TOKEN_EOL);
}

void paramDeclFirst() {
    expect(TOKEN_IDENTIF);
    SymbolName id = last_identifier.data.identifierName;
    type();
    addArgumentToFunction(current_symbol, id, last_type);
}

void paramDeclSecond() {
    expect(TOKEN_IDENTIF);
    type();
    insertLastSeenIdentifierToSymtable(last_type, true);
}

void paramDeclN() {
    do {
        if (accept(TOKEN_SEP_COMMA)) {
            maybeEOL();
            paramDecl();
        } else if (compare(TOKEN_SEP_R_BRACKET)){
            break;
        } else {
            syntaxError("Expected end of argument list or another argument separated by ','.\n");
        }
    } while(true);
}

void paramDeclList() {
    if(compare(TOKEN_IDENTIF)) {
        paramDecl();
        paramDeclN();
    } else if (compare(TOKEN_SEP_R_BRACKET)) {
    } else {
        syntaxError("Invalid parameter declaration.\n");
    }
}

void typeN() {
    do {
        if (accept(TOKEN_SEP_COMMA)) {
            maybeEOL();
            type();
        } else if (compare(TOKEN_SEP_R_BRACKET)) {
            break;
        } else {
            syntaxError("Bad return type list.\n");
        }
    } while(true);
}

void typeList() {
    if (compare(TOKEN_SEP_R_BRACKET)) {
    } else {
        type();
        typeN();
    }
}

enum ValueType *expressionList(unsigned expected_type_buffer_size, int exit_val) {
    enum ValueType *type_buffer = NULL;
    if(compare(TOKEN_EOL)) {
        if (expected_type_buffer_size != 0) {
            if (exit_val == 6) { 
                // Empty return is valid syntax but in this case a semantic error
                semanticError(exit_val, "Expected one or more return values.\n");
            }
            syntaxError("Expected one or more expressions.\n");
        }
    } else {

        type_buffer = malloc(sizeof(int)*expected_type_buffer_size);
        if (type_buffer == NULL) {
            internalError("Cannot allocate buffer for expressionList");
        }

        Expr *expr = parseExpression();
        if(expr->type == EXPR_FUNCALL) {
            Symbol *sm = getFunctionSymbol(expr->funcCall.funcName);
            if(sm->functionAttrs.returnedValuesCount == expected_type_buffer_size) {
                processFunctionCall(expr);
                for(unsigned i = 0; i < expected_type_buffer_size; i++) {
                    type_buffer[i] = sm->functionAttrs.returnTypes[i];
                }
                if (scn.current.type != TOKEN_EOL){
                    semanticError(6,"Too many expressions found.\n");
                }
                return type_buffer;
            } else if(sm->functionAttrs.returnedValuesCount != 1) {
                semanticError(6, "Function with more than one return value should be in separate assignment.\n");
            }
        }

        type_buffer[0] = processExpression(expr);
        exprFree(expr);
        expressionN(type_buffer, expected_type_buffer_size, exit_val);

    }
    return type_buffer;
}

void expressionN(enum ValueType *type_buffer, unsigned expected_type_buffer_size, int exit_val) {
    unsigned type_buffer_write_index = 1;
    do {
        if (compare(TOKEN_EOL) || compare(TOKEN_SEP_L_CURLY)) {
            if (expected_type_buffer_size != type_buffer_write_index) {
                free(type_buffer);
                semanticError(exit_val, "The number of expressions does not match expected count.\n");
            }
            break;
        } else if(expect(TOKEN_SEP_COMMA)) {
            if (type_buffer_write_index >= expected_type_buffer_size){
                free(type_buffer);
                semanticError(exit_val, "The number of expressions does not match expected count.\n");
                break;
            }
            type_buffer[type_buffer_write_index] = expression();
            type_buffer_write_index++;
        }
    } while(true);
}

void maybeEOL() {
    while(accept(TOKEN_EOL));
}


bool verifyExprTypesMatchExpected(
        enum ValueType *types_actual_list,
        struct IdentifierStack *identifiers,
        struct TypeMissmatchInfo *missmatch_info
        )
{
    const unsigned identifier_count = identifierStackGetSize(identifiers);
    enum ValueType current_expr_type;
    char *identifier_name;
    Symbol *current_symbol;

    for (unsigned i = 0; i < identifier_count; i++) {
        current_expr_type = types_actual_list[i];
        identifier_name = identifiers->identifierStack[i];
        
        if(strcmp(identifier_name, "_") != 0) {
            current_symbol = symtableLookup(st, identifier_name);
            switch (current_symbol->type) {
                case SYMBOL_TYPE_VARIABLE:
                    if (current_symbol->variableAttrs.type != current_expr_type) {
                        missmatch_info->actualType = current_expr_type;
                        missmatch_info->identifierName = identifier_name;
                        missmatch_info->identifierSymbol = current_symbol;
                        return false;
                    }
                    break;
                case SYMBOL_TYPE_FUNCTION:
                    missmatch_info->actualType = current_expr_type;
                    missmatch_info->identifierName = identifier_name;
                    missmatch_info->identifierSymbol = current_symbol;
                    return false;
                    break;
            }
        }
    }

    return true;
}

void printAssignmentTypeMissmatchError(
        struct ErrorInfo *err_info, 
        struct TypeMissmatchInfo *missmatch_info
        )
{
    if (missmatch_info->identifierSymbol->type == SYMBOL_TYPE_FUNCTION) {
        fprintf(
                stderr,
                "Error on line %u, column %u: Trying to assign a value of type `%s` to a function %s.",
                err_info->srcPositionLine,
                err_info->srcPositionColumn,
                VALUE_TYPES_NAMES[missmatch_info->actualType],
                missmatch_info->identifierName
                );
    } else {
        const enum ValueType expected_type = missmatch_info->identifierSymbol->variableAttrs.type;
        fprintf(
                stderr,
                "Error on line %u, column %u: Trying to assign value of type `%s` to identifier %s, which was declared as `%s`",
                err_info->srcPositionLine,
                err_info->srcPositionColumn,
                VALUE_TYPES_NAMES[expected_type],
                missmatch_info->identifierName,
                VALUE_TYPES_NAMES[missmatch_info->actualType]
                );
    }

}

char* mangleVariableName(
        char *current_fun_name,
        unsigned current_ctx_id,
        char *var_name
        )
{
    int buffer_size = snprintf(NULL, 0, "%s$%u$%s", current_fun_name, current_ctx_id, var_name) + 1;
    // + 1 because of the null termination

    char *buffer = (char *) malloc(sizeof(char)*buffer_size);
    if (buffer == NULL) {
        return NULL;
    }
    
    int bytes_written = snprintf(buffer, buffer_size, "%s$%u$%s", current_fun_name, current_ctx_id, var_name);

    if (bytes_written > 0 && bytes_written <= buffer_size) {
        return buffer;
    } else {
        fprintf(
                stderr,
                "Error while mangling variable name %s, func %s | allocated %d, would write %d\n", 
                var_name,
                current_fun_name,
                buffer_size,
                bytes_written
                );
        return NULL;
    }

    return 0;
}


void emitInstructionsForMultiAssignment(
        struct IdentifierStack *identifier_stack
        ) 
{
    char *identifier_name;
    char *mangled_name;
    Symbol *symbol;

    // LowPrioTODO(codeboy): pass directly symbol stack, instead of identifier stack
    while(!identifierStackIsEmpty(identifier_stack)) {
        identifier_name = identifierStackPop(identifier_stack);
        if(strcmp(identifier_name, "_") != 0) {
            symbol = symtableLookup(st, identifier_name);
            mangled_name = symbol->nameInCode;

            // Since the results of expressions are stored on data stack (top is rightmost)
            // Pops should do the trick
            printCode(1, INSTR_POPS, TYPE_LF, mangled_name);
        } else {
            printCode(1, INSTR_POPS, TYPE_GF, "$trash");
        }
    }
}

void emitInstrucionForExprAssignent(Symbol *variable_symbol) {
    char *mangled_name = variable_symbol->nameInCode; 
    printCode(1, INSTR_POPS, TYPE_LF, mangled_name);
}

void replaceDuplicitIdentifiersWithUnderscore(struct IdentifierStack *identifier_stack) {
    bool do_identifiers_match = false;
    const unsigned stack_size = identifier_stack->top;

    for (unsigned i = 0; i < stack_size; i++) {
        // _ is ignored
        if (strcmp(identifier_stack->identifierStack[i], "_") == 0) {
            continue;
        }

        for (unsigned j = i + 1; j < stack_size; j++) {
            do_identifiers_match = strcmp(
                    identifier_stack->identifierStack[i], 
                    identifier_stack->identifierStack[j]) == 0;

            if (do_identifiers_match) {
                // Replace the first one matching with `_`
                // Character manipulation should be allowed here since all identifiers has 
                // size of at least 2 (char + null byte), and everyone is iterating over string
                // with strlen, which checks for nullbyte
                identifier_stack->identifierStack[i][0] = '_';
                identifier_stack->identifierStack[i][1] = '\0';

                break; // We want to stop iteration here, size identifierStack[i] was replaced with `_`
            }
        }
    }
}

void emitFunctionVariableDeclarations(Symbol *func_symbol) {
    unsigned count = func_symbol->functionAttrs.functionDeclarations.stringsCount;
    for(unsigned i = 0; i < count;i++) {
        printCode(1, INSTR_DEFVAR, TYPE_LF, current_symbol->functionAttrs.functionDeclarations.strings[i]);
    }
}

bool isFunctionMissingReturnStatement(Symbol *func_symbol) {
    bool is_missing_return = false;
    if (func_symbol->functionAttrs.returnedValuesCount != 0) {
        if (func_symbol->functionAttrs.returnExists == false) {
            is_missing_return = true;
        }
    }

    return is_missing_return;
}
