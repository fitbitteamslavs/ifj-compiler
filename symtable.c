/* 
 * Subor symtable.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 */

#include "symtable.h"

SymtableBucket *symtableBucketNew(
		SymbolName name, 
		Symbol *symbol, 
		SymtableBucket *tail) {

	SymtableBucket *bucket = (SymtableBucket *) malloc(sizeof(SymtableBucket));

	if (bucket == NULL) {
		return NULL;
	}

    // #FIXME: Do not keep symbol name inside bucket
	bucket->symbolName = strdup(name);
	if (bucket->symbolName  == NULL) {
		free(bucket);
		return NULL;
	}

	bucket->next = tail;
	bucket->symbol = symbol;
	bucket->prevSymbolBucketId = -1;

	return bucket;
}

void symtableBucketFreeSingle(SymtableBucket *bucket) {
    freeSymbol(bucket->symbol);
	free(bucket->symbolName);
	free(bucket);
}

void symtableBucketFreeAll(SymtableBucket *head) {
	SymtableBucket *next;
	while (head != NULL) {
		next = head->next;	
		 symtableBucketFreeSingle(head);
		head = next;
	}
}

Symtable *symtableNew() {
	Symtable *symtable = (Symtable *) malloc(sizeof(Symtable));
	if (symtable == NULL) return NULL;
	
	// Initialize stack bucket
	symtable->symbolStackTopBucketId = -1;

	for (unsigned i = 0; i < SYMTABLE_SIZE; i++) {
		symtable->table[i] = NULL; // NULL initialize buckets
	}

    symtable->stackLevel = 0;

	return symtable;
}

void symtableFree(Symtable *symtable) {
	
	for (unsigned i = 0; i < SYMTABLE_SIZE; i++) {
		 symtableBucketFreeAll(symtable->table[i]);
	}

	free(symtable);
}

int symtableInsert(
		Symtable *symtable, 
		SymbolName symbol_name, 
		Symbol *symbol) {
	if(symtableIsInCurrentContext(symtable, symbol_name)) {
        return SYMTABLE_ALREADY_DEFINED;
    }

	unsigned bucket_id = symtableHash(symbol_name) % SYMTABLE_SIZE;
	SymtableBucket *bucket = symtable->table[bucket_id];	
	
	SymtableBucket *new_bucket_head = symtableBucketNew(symbol_name, symbol, bucket);
	if (new_bucket_head == NULL) {
		return SYMTABLE_MALLOC_FAILED;
	} else {
		symtable->table[bucket_id] = new_bucket_head;
		
		// Update auxiliary stack for performing Context push/pop
		new_bucket_head->prevSymbolBucketId = symtable->symbolStackTopBucketId;
        new_bucket_head->level = symtable->stackLevel;

		symtable->symbolStackTopBucketId = bucket_id;
	}

	return SYMTABLE_OK;
}

SymtableBucket *lookup(Symtable *symtable, SymbolName symbol_name) {
	unsigned bucket_id = symtableHash(symbol_name) % SYMTABLE_SIZE;		

	SymtableBucket *iter;
	for (iter = symtable->table[bucket_id]; iter != NULL; iter=iter->next) {
		if (strcmp(iter->symbolName, symbol_name) == 0) {
			return iter;
		}
	}

	return NULL;
}

bool symtableIsInCurrentContext(Symtable *symtable, SymbolName symbol_name){
    SymtableBucket *bucket = lookup(symtable, symbol_name);
    if(bucket != NULL && bucket->level == symtable->stackLevel) {
        return true;
    } else {
        return false;
    }
}

Symbol *symtableLookup(Symtable *symtable, SymbolName symbol_name) {
    SymtableBucket *bucket = lookup(symtable, symbol_name);
    if(bucket == NULL) {
        return NULL;
    } else {
        return bucket->symbol;
    }
}

int symtablePushContext(Symtable *symtable) {
	int insert_status = symtableInsert(symtable, SYMTABLE_STACK_SENTINEL_NAME, NULL);
	if (insert_status != SYMTABLE_OK) {
		return insert_status;
	}

    symtable->stackLevel++;

	return SYMTABLE_OK;
}

int symtablePopContext(Symtable *symtable) {
	int current_symbol_bucket_id;
	SymtableBucket *current_bucket;
	bool is_sentinel = false;
	do {
		current_symbol_bucket_id = symtable->symbolStackTopBucketId;
		if (current_symbol_bucket_id == -1) {
			// We have emptied symbol stack -- time to stop deleting context
			return 0;
		}
		current_bucket = symtable->table[current_symbol_bucket_id];
		assert(current_bucket); // #FIXME Remove this assert after it is know that current_symbol cannot be null

		// Update stack top
		symtable->symbolStackTopBucketId = current_bucket->prevSymbolBucketId;

		// Check whether we have found sentinel
		is_sentinel = strcmp(current_bucket->symbolName, SYMTABLE_STACK_SENTINEL_NAME) == 0;

		// Either way free the current element -- pop_context will allways destroy at least one elem
		symtable->table[current_symbol_bucket_id] = current_bucket->next; // Move +1 in linked list

		symtableBucketFreeSingle(current_bucket);
	} while (!is_sentinel);

    symtable->stackLevel--;
	
	return 0;
}
