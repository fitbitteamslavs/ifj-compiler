/* 
 * Subor symbol.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xhecko02 - Michal Hečko
 *  xlacko09 - Dávid Lacko
 */

#ifndef IFJ20_SYMBOL_H 
#define IFJ20_SYMBOL_H 
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "scanner.h"
#include "datastructures.h"

typedef char* SymbolName;

enum ValueType {
    VAL_TYPE_FLOAT,
    VAL_TYPE_INT,
    VAL_TYPE_STR,
    VAL_TYPE_BOOL,
    VAL_TYPE_NULL
};

enum SymbolType {
    SYMBOL_TYPE_VARIABLE,
    SYMBOL_TYPE_FUNCTION,
};

struct VariableAttributes {
    enum ValueType type;
};

struct FunctionAttributes {
    enum ValueType *returnTypes;        // An array containing returned values - all values are plain types (same as variable can have)
    unsigned returnedValuesCount;       // The size of the array of returned values

    struct StringVector functionDeclarations; // An array containing all declared variables in the function

    bool returnExists;

    unsigned argumentCount;                 
    enum ValueType *argumentTypes;      // We use VariableType here to dissallow the ussage of SYMBOL_TYPE_FUNCTION
    SymbolName *argumentNames;          // The Size is the same as argumentTypes = argumentCount
};

typedef struct Symbol { 
    enum SymbolType type;
    char *nameInCode;
    union {
        struct VariableAttributes variableAttrs;
        struct FunctionAttributes functionAttrs;
    };
} Symbol;

Symbol *createFunctionSymbol();
void freeSymbol(Symbol *sm);
void addArgumentToFunction(Symbol *fn, SymbolName argName, enum ValueType type);
void addReturnTypeToFunction(Symbol *fn, enum ValueType type);
void setFunctionLabel(Symbol *fn, char *label);

Symbol *createVariable(enum ValueType type);
#endif
