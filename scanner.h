/* 
 * Subor scanner.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_SCANNER_H 
#define IFJ20_SCANNER_H 
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

#include "datastructures.h"
#include "scanner_errinfo.h"

// Should be power of 2
#define MAX_REALLOC_INCREMENT 16

//These verify macros are used to check string (data structure) allocation an reallocation after callin strInit and strAddChar
#define verifyMalloc() \
    if (sm->errorInfo.errorStatus == SCANNER_MALLOC_FAILED ) {\
        sm->errorInfo.errorDetails = "Couldn't allocate enough space.\n";\
        sm->isTokenValid = false;\
        sm->hasFinishedReading = true;\
        return;\
    }\

#define verifyRealloc() \
    if (sm->errorInfo.errorStatus == SCANNER_REALLOC_FAILED) {\
        strFree(&sm->symbolAccumulator);\
        sm->errorInfo.errorDetails = "Couldn't reallocate enough space.\n";\
        sm->isTokenValid = false;\
        sm->hasFinishedReading = true;\
        return;\
    }\

#define MAX_TOKEN_NAME_LEN 14
enum TokenType {
    TOKEN_IDENTIF,
    TOKEN_EOL, // We need EOL since the gramar is driven by newlines

    TOKEN_OP_DECLARE, // :=
    TOKEN_OP_ASSIGN,  // =

    // Arithmetic operators
    TOKEN_OP_ARIT_PLUS, // 4
    TOKEN_OP_ARIT_MINUS,
    TOKEN_OP_ARIT_TIMES,
    TOKEN_OP_ARIT_DIV,

    // Relational operators
    TOKEN_OP_REL_EQ, // 8       ==
    TOKEN_OP_REL_NOTEQ,     //  !=
    TOKEN_OP_REL_LESS,      //  <
    TOKEN_OP_REL_LESSEQ,    //  <=
    TOKEN_OP_REL_GREATER,   //  >
    TOKEN_OP_REL_GREATEREQ, //  >=

    // LITERALS
    TOKEN_LIT_STR, // 14
    TOKEN_LIT_INT,
    TOKEN_LIT_FLOAT,

    // SEPARATORS
    TOKEN_SEP_L_BRACKET, // 17
    TOKEN_SEP_R_BRACKET,
    TOKEN_SEP_L_CURLY,
    TOKEN_SEP_R_CURLY,
    TOKEN_SEP_COMMA,
    TOKEN_SEP_SEMICOLON,
    
    TOKEN_KEYWORD, // 23
    TOKEN_EOF
};

#define KEYWORD_COUNT 9
#define MAX_KW_NAME_LEN 7
enum KeywordType {
    // Control flow
    KEYWORD_IF,
    KEYWORD_ELSE,
    KEYWORD_FOR,
    KEYWORD_RETURN,

    // Type declarations
    KEYWORD_FLOAT64,
    KEYWORD_INT,
    KEYWORD_STRING,
    KEYWORD_FUNC,

    KEYWORD_PACKAGE,
};

typedef struct token_str {
    enum TokenType type;
    union TokenData {
        enum KeywordType keyword;       /// Value for TOKEN_KEYWORD
        char *identifierName;           /// Value for TOKEN_IDENTIF
        char *strValue;                 /// Value for TOKEN_LIT_STR
        uint64_t intValue;              /// Value for TOKEN_LIT_INT
        double floatValue;              /// Value for TOKEN_LIT_FLOAT
    } data;
} Token;

/**
 * State machine is defined by the function that should be called
 * on next symbol.
 */
typedef struct StateMachine {
    /// Each state will set the next state here
    void (*handle)(struct StateMachine *sm, char chr);

    /// True if the machine is in final state and cannot read another symbol
    bool hasFinishedReading;

    /// If state loads an extra character, that should be processed by next 
    /// state, this character is stored in extraChar and hasExtraChar is true
    char extraChar; 
    bool hasExtraChar; 

    /// Some states work together to load a token, so we need to store symbols 
    /// these states load in one place to allow them to work together
    string symbolAccumulator;
    
    bool isTokenValid;
    Token outputToken;
    
    struct ErrorInfo errorInfo;
} StateMachine;

struct TokenRecord {
    Token tk;
    int srcLine;
    int srcColumn;
};

struct History {
    long long currentPosition;
    long long beginOfCurrentStatement;
    struct TokenRecord *tokens;
    int size;
    int count;
    int currentRow;
};

/**
 * Representation of the scanner. 
 * Contains history of tokens along with information needed for movement in the 
 * history. This is required, because we want to pass the source code twice, but
 * call the scanner only once. So in the first pass scanner creates the history 
 * of tokens for us and therefore we can pass the source code second time.
 */
typedef struct Scanner {
    struct StateMachine stateMachine;

    struct History hist;
    Token current;
    Token next;

    bool isNextValid; /// True if the next token is already loaded

    int newCol;       /// Column of the next toke
    int newLine;      /// Line of the next token

    /// Scanner reached EOF (from this point on the history is used)
    bool fileFinished;  
} Scanner;

 /* Public interface */
/** 
 * Initializes Scanner struct, which should never be used without
 * inicialization.
 */
void scannerInit(Scanner *scn);
void scannerDeinit(Scanner *scn);
void scannerSeek(Scanner *scn, long long index);
long long scannerGetPosition(Scanner *scn);
void scannerNewStatement(Scanner *scn);

/**
 * Sets the current token in the scanner
 */
void getToken(Scanner *scn);
/**
 * Sets the next token in the scanner, which is used as a information whats:
 * after current token. This token is then set to current when getToken is 
 * called.
 */
void peekToken(Scanner *scn);

 /* Private local functions */
/**
 * @return String containing the name of the token.
 */
const char *getTokenName(enum TokenType tk_type);

/**
 * @return Name of the keyword as string.
 */
const char *getKWName(enum KeywordType keyword);

/**
 * Checks if there were any errors in FSM run.
 * Should be called after stateMachineDriver.
 */
void checkError(Scanner *scn);

void stateMachineInitializeBeforeRun(StateMachine *sm);

/**
 * The main loop of the FSM.
 */
void stateMachineDriver(StateMachine *sm, Token *tk);

/**
 * If a state to read an extra symbol to identify current token, we need to
 * store this extra symbol to process it later in the next run of the FSM.
 */
void stateMachineStoreExtraSymbol(StateMachine *sm, char sym);

/**
 * Use this function to read a character from the source code. 
 * It allows for better error messages.
 * @return read character or EOF
 */
int stateMachineReadStdinChar(StateMachine *sm);

/**
 * Reads from stdin until given character is read
 * @return true if found character s else false (when EOF reached)
 * @param s character we want to fast forward to
 */
bool readUntil(StateMachine *sm, char s);

/**
 * @return true if given identifier is keyword, false otherwise.
 */
bool isKeyword(char *identifier, enum KeywordType *keyword);

/// State machine states - functions, that implement state machine <- link to diagram?
void SMInitialState(StateMachine *sm, char sym);
void SMReadOpDiv(StateMachine *sm, char sym);
void SMMultiComment(StateMachine *sm, char sym);
void SMMultiCommentEnd(StateMachine *sm, char sym);
void SMIdentifier(StateMachine *sm, char sym);
void SMReadOpRelGreater(StateMachine *sm, char sym);
void SMReadOpRelLess(StateMachine *sm, char sym);
void SMReadOpAssignOrEq(StateMachine *sm, char sym);
void SMReadNotEq(StateMachine *sm, char sym);
void SMReadOpDefine(StateMachine *sm, char sym);
void SMZero(StateMachine *sm, char sym);
void SMInteger(StateMachine *sm, char sym);
void SMExpectExponent(StateMachine *sm, char sym);
void SMExpectDecimalPart(StateMachine *sm, char sym);
void SMReal(StateMachine *sm, char sym);
void SMRealExponent(StateMachine *sm, char sym);
void SMString(StateMachine *sm, char sym);
void SMEscape(StateMachine *sm, char sym);
void SMExpectHexFirst(StateMachine *sm, char sym);
void SMExpectHexSecond(StateMachine *sm, char sym);
void SMStringEnd(StateMachine *sm, char sym);

#endif
