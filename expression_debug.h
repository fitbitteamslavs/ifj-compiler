/* 
 * Subor expression_debug.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xhecko02 - Michal Hečko
 */

#ifndef IFJ20_EXPRESSION_DEBUG_H 
#define IFJ20_EXPRESSION_DEBUG_H  

#include "expression.h"
#include "stdio.h"
#include "stdlib.h"


void prettyPrintExprTree(const Expr *expression, int depth);
void printCharNTimes(const char c, const unsigned times);
const char* getBinaryExprTypeName(enum BinaryExprType type);
void printLiteral(const Expr *e);
void printFuncCall(const Expr *e, const int depth);
#endif
