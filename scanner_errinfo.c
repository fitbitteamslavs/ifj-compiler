/* 
 * Subor scanner_errinfo.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 */

#include "scanner_errinfo.h"
#include <stdio.h>

int errorInfoInit(struct ErrorInfo *err_info) {
    err_info->currentLineContents = (char *) malloc(ERR_INFO_INITIAL_CAPACITY * sizeof(char));
    if (err_info->currentLineContents == NULL) {
        return ERR_INFO_MALLOC_FAILED;
    }

    err_info->lineContentsCapacity = ERR_INFO_INITIAL_CAPACITY;
    err_info->srcPositionColumn = 0;
    err_info->srcPositionLine = 1;
    err_info->currentLineLength = 0;

    err_info->errorDetails = NULL;
    err_info->errorStatus = SCANNER_OK;

    return ERR_INFO_OK;
}

int errorInfoStoreLineChar(struct ErrorInfo *err_info, char c) {
    if (err_info->currentLineLength == err_info->lineContentsCapacity) {
        // Grow the internal buffer
        char *new_buff = realloc(err_info->currentLineContents, (err_info->lineContentsCapacity)*2);
        if (new_buff != NULL) {
            err_info->currentLineContents = new_buff;
            err_info->lineContentsCapacity *= 2; 
        } else {
            return ERR_INFO_BUFFER_RESIZE_FAILED;
        }
    }
    
    err_info->currentLineContents[err_info->currentLineLength++] = c;
    err_info->srcPositionColumn++;

    return ERR_INFO_OK;
}

void errorInfoUpdateLineAdvanced(struct ErrorInfo *err_info) {
    // We can rewrite the current line buffer
    err_info->currentLineLength = 0;
    err_info->srcPositionColumn = 0;
    err_info->srcPositionLine++;
}

void errorInfoPrintErrorMessage(struct ErrorInfo *err_info) {
    // We will fill the destination buffer, with the current line and additional information
    // ```
    // We want our err message to look like this:
    //
    // var a = func(a, 34ea5);
    //                    ^
    // Error on line 3, column 29: Could not parse float64
    // ```
    // Provided message is: Could not parse float64

    // Prints: var a = func(a, 34e5);
    for (unsigned i = 0; i < err_info->currentLineLength; i++) {
        // We must print like this, since we have no idea, whether the current line is null terminated.
        putc(err_info->currentLineContents[i], stderr); 
    }
    if(err_info->currentLineContents[err_info->currentLineLength-1] != '\n') {
        int c = getc(stdin);
        while(c != '\n' && c != EOF){
            putc(c, stderr);
            c = getc(stdin);
        }
        putc('\n', stderr);
    }

    if(err_info->srcPositionColumn == 0) {
        putc('|', stderr);
        putc('<', stderr);
    } else {
        // Prints:                 ^
        for (unsigned i = 0; i < err_info->srcPositionColumn-1; i++) {
            putc(' ', stderr);
        }
        putc('^', stderr);
    }
    putc('\n', stderr);

    // Prints: Error on line 3, column 29: Could not parse float64 -- aka error details
    fprintf(stderr, "Error on line %u, column %u\n", err_info->srcPositionLine, err_info->srcPositionColumn);
    if (err_info->errorDetails != NULL) {
        fputs(err_info->errorDetails, stderr);
    }
}

void errorInfoDestroy(struct ErrorInfo *err_info) {
    free(err_info->currentLineContents);

    if (err_info->errorDetails != NULL) {
        free(err_info->errorDetails);
        err_info->errorDetails = NULL;
    }
}

int errorInfoUpdateInputAdvanced(struct ErrorInfo *err_info, char c) {
    int status = ERR_INFO_OK;
    if (err_info->currentLineLength > 0 && err_info->currentLineContents[err_info->currentLineLength-1] == '\n') {
        errorInfoUpdateLineAdvanced(err_info);
    }
    status = errorInfoStoreLineChar(err_info, c);

    return status;
}

int errorInfoStoreErrorDetails(struct ErrorInfo *err_info, char *details) {
    if (err_info->errorDetails != NULL) {
        // There should not be stored any details - they get deleted when reporting an error
        assert(err_info->errorDetails); // Crash!
    } 

    err_info->errorDetails = strdup(details);
    if (err_info->errorDetails == NULL) {
        return ERR_INFO_STORE_DETAILS_FAILED;
    }

    return ERR_INFO_OK;
}

void errorInfoClearErrorDetails(struct ErrorInfo *err_info) {
    free(err_info->errorDetails);
    err_info->errorDetails = NULL;
}

