/* 
 * Subor expression_semantic.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 */

#include "expression_semantic.h"
#include "buildins.h"
#include "expression.h"
#include "symbol.h"

Symtable *_st;

void setSymtable(Symtable *st) {
    // Copy symtable locally to avoid passing it to other functions
    _st = st;
}

enum ValueType processExpression(Expr *expr) {
    struct ExprResult res = recursiveDescent(expr);

    if(res.isPropagated) {
        switch(res.type) {
        case VAL_TYPE_INT:
            printCode(1, INSTR_PUSHS, TYPE_INT, res.intVal);
            break;
        case VAL_TYPE_FLOAT:
            printCode(1, INSTR_PUSHS, TYPE_FLOAT, res.flVal);
            break;
        default:
            break;
        }
    }
    return res.type;
}

Symbol *getIdentifierSymbol(SymbolName name) {
    Symbol *sm = symtableLookup(_st, name);
    if(sm == NULL) {
        semanticError(3, "Undefined variable.");
    }
    if(sm->type != SYMBOL_TYPE_VARIABLE) {
        semanticError(6, "Given identifier is not a variable.");
    }
    return sm;
}

Symbol *getFunctionSymbol(SymbolName name) {   
    Symbol *sm = symtableLookup(_st, name);
    if(sm == NULL) {
        semanticError(3, "Undefined function.");
    }
    if(sm->type != SYMBOL_TYPE_FUNCTION) {
        semanticError(3, "Given identifier is not a function.");
    }

    return sm;
}


struct ExprResult recursiveDescent(Expr *expr) {
    struct ExprResult res = {.isPropagated=false};
    switch(expr->type) {
        case EXPR_LITERAL:
        {
            res.type = (enum ValueType) expr->literal.type;
            switch(expr->literal.type) {
                case LIT_EXPR_INT:
                    res.intVal = expr->literal.intValue;
                    res.isPropagated = true;
                    break;
                case LIT_EXPR_FLOAT:
                    res.flVal = expr->literal.floatValue;
                    res.isPropagated = true;
                    break;
                case LIT_EXPR_STR:
                    printCode(1, INSTR_PUSHS, TYPE_STR, expr->literal.strValue);
            }
            break;
        }
        case EXPR_FUNCALL:
        {
            Symbol *sm = getFunctionSymbol(expr->funcCall.funcName);
            if(sm->functionAttrs.returnedValuesCount != 1) {
                semanticError(5, "Function used inside expression must return exactly one value.\n");
            }
            processFunctionCall(expr);
            res.type = sm->functionAttrs.returnTypes[0];
            break;
        }
        case EXPR_IDENTIFIER:
        {
            Symbol *sm = getIdentifierSymbol(expr->identifier.name);
            printCode(1, INSTR_PUSHS, TYPE_LF, sm->nameInCode);
            res.type = sm->variableAttrs.type;
            break;
        }
        case EXPR_BINARY:
            return processBinaryExpression(expr);
    }
    return res;
}

int processFunctionCall(Expr *expr) {
    if(expr->type != EXPR_FUNCALL) {
        return -1;
    }

    // If the function is print, handle specially for varible arguments
    if(strcmp(expr->funcCall.funcName, BUILDIN_PRINT_MAIN_LABEL) == 0) {
        for(int i = expr->funcCall.argCount-1; i >= 0; i--) {
            processExpression(expr->funcCall.args[i]);
        }
        callBuildinPrint(expr->funcCall.argCount);
        return 0;
    }


    Symbol *sm = getFunctionSymbol(expr->funcCall.funcName);
    struct FunctionAttributes attrs = sm->functionAttrs;

    if(sm->functionAttrs.argumentCount != expr->funcCall.argCount) {
        semanticError(6, "Invalid number of arguments.");
    }

    enum ValueType type;
    for(unsigned int i = 0; i < attrs.argumentCount; i++) {
        type = processExpression(expr->funcCall.args[i]);
        if(type != attrs.argumentTypes[i]) {
            semanticError(6, "Argument with wrong type passed to function call.");
        }
    }

    printCode(0, INSTR_CREATEFRAME);
    for(int i = attrs.argumentCount-1; i >= 0 ; i--) {
        printCode(1, INSTR_DEFVAR, TYPE_TF, attrs.argumentNames[i]);
        printCode(1, INSTR_POPS, TYPE_TF, attrs.argumentNames[i]);
    }

    printCode(1, INSTR_CALL, sm->nameInCode);
    
    return attrs.returnedValuesCount;
}

struct ExprResult processBinaryExpression(Expr *expr) {
    struct ExprResult left_res = recursiveDescent(expr->binaryExpr.left);
    struct ExprResult right_res = recursiveDescent(expr->binaryExpr.right);

    enum ValueType left = left_res.type;
    enum ValueType right = right_res.type;
    if(left != right) {
        semanticError(5, "Different types in binary operation.");
    }

    struct ExprResult res = {.isPropagated=false};

    if(left == VAL_TYPE_STR && expr->binaryExpr.type == BIN_EXPR_ADD) {
        printCode(1, INSTR_POPS, TYPE_GF, "$strbuild2");
        printCode(1, INSTR_POPS, TYPE_GF, "$strbuild1");
        printCode(3, INSTR_CONCAT, TYPE_GF, "$strbuild1", TYPE_GF, "$strbuild1", TYPE_GF, "$strbuild2");
        printCode(1, INSTR_PUSHS, TYPE_GF, "$strbuild1");
        res.type = VAL_TYPE_STR;
        return res;
    }

    //if arithmetic operation
    if(expr->binaryExpr.type <= BIN_EXPR_DIV && left > VAL_TYPE_INT) {
        semanticError(5, "Using arithmetic operation for types other than int or float is not supported.");
    }

    if(left_res.isPropagated && right_res.isPropagated){
        int64_t resInt = 0;
        double resFl = 0.0;
        bool resBolInt = false;
        bool resBolFl = false;

        int64_t num1Int = left_res.intVal;
        int64_t num2Int = right_res.intVal;

        double num1Fl = left_res.flVal;
        double num2Fl = right_res.flVal;

        switch(expr->binaryExpr.type) {
        case BIN_EXPR_ADD:
            resInt = num1Int + num2Int;
            resFl = num1Fl + num2Fl;
            break;
        case BIN_EXPR_SUB:
            resInt = num1Int - num2Int;
            resFl = num1Fl - num2Fl;
            break;
        case BIN_EXPR_MUL:
            resInt = num1Int * num2Int;
            resFl = num1Fl * num2Fl;
            break;
        case BIN_EXPR_DIV:
        {
            if(left_res.type == VAL_TYPE_FLOAT && num2Fl == 0.0){
                semanticError(9, "Division by zero float.\n");
            }
            if(left_res.type == VAL_TYPE_INT && num2Int == 0){
                semanticError(9, "Division by zero integer.\n");
            }
            resInt = num1Int / num2Int;
            resFl = num1Fl / num2Fl;
            break;
        }
        case BIN_EXPR_EQ:
            resBolInt = (num1Int == num2Int);
            resBolFl = (num1Fl == num2Fl);
            break;
        case BIN_EXPR_NE:
            resBolInt = (num1Int != num2Int);
            resBolFl = (num1Fl != num2Fl);
            break;
        case BIN_EXPR_LT:
            resBolInt = (num1Int < num2Int);
            resBolFl = (num1Fl < num2Fl);
            break;
        case BIN_EXPR_LE:
            resBolInt = (num1Int <= num2Int);
            resBolFl = (num1Fl <= num2Fl);
            break;
        case BIN_EXPR_GT:
            resBolInt = (num1Int > num2Int);
            resBolFl = (num1Fl > num2Fl);
            break;
        case BIN_EXPR_GE:
            resBolInt = (num1Int >= num2Int);
            resBolFl = (num1Fl >= num2Fl);
        }

        if(expr->binaryExpr.type >= BIN_EXPR_EQ) {
            if(left == VAL_TYPE_FLOAT) {
                printCode(1, INSTR_PUSHS, TYPE_BOOL, resBolFl);
            } else if(left == VAL_TYPE_INT) {
                printCode(1, INSTR_PUSHS, TYPE_BOOL, resBolInt);
            }
            res.type = VAL_TYPE_BOOL;
        } else {
            res.type = left;
            res.isPropagated = true;
        }

        if(left == VAL_TYPE_INT) {
            res.intVal = resInt;
        } else {
            res.flVal = resFl;
        }
        
        return res;
        
    } else if(left_res.isPropagated && !right_res.isPropagated) {
        printCode(1, INSTR_POPS, TYPE_GF, "$strbuild2");
        if(left_res.type == VAL_TYPE_INT) {
            printCode(1, INSTR_PUSHS, TYPE_INT, left_res.intVal);
        } else if(left_res.type == VAL_TYPE_FLOAT) {
            printCode(1, INSTR_PUSHS, TYPE_FLOAT, left_res.flVal);
        }
        printCode(1, INSTR_PUSHS, TYPE_GF, "$strbuild2");
    } else if(!left_res.isPropagated && right_res.isPropagated) {
        if(right_res.type == VAL_TYPE_INT) {
            printCode(1, INSTR_PUSHS, TYPE_INT, right_res.intVal);
        } else if(right_res.type == VAL_TYPE_FLOAT) {
            printCode(1, INSTR_PUSHS, TYPE_FLOAT, right_res.flVal);
        }
    }

    enum InstructionSet ins;
    switch(expr->binaryExpr.type) {
    case BIN_EXPR_ADD:
        ins = INSTR_ADDS;
        break;
    case BIN_EXPR_SUB:
        ins = INSTR_SUBS;
        break;
    case BIN_EXPR_MUL:
        ins = INSTR_MULS;
        break;
    case BIN_EXPR_DIV:
    {
        if(right_res.isPropagated) {
            if(right_res.type == VAL_TYPE_FLOAT) {
                if(right_res.flVal == 0.0) {
                    semanticError(9, "Division by zero float.\n");
                }
            } else if(right_res.type == VAL_TYPE_INT) {
                if(right_res.intVal == 0) {
                    semanticError(9, "Division by zero integer.\n");
                }
            }
        }

        if(left == VAL_TYPE_INT) {
            ins = INSTR_IDIVS;
        } else if(left == VAL_TYPE_FLOAT) {
            ins = INSTR_DIVS;
        }
        break;
    }
    case BIN_EXPR_EQ:
        ins = INSTR_EQS;
        break;
    case BIN_EXPR_NE:
        printCode(0, INSTR_EQS);
        ins = INSTR_NOTS;
        break;
    case BIN_EXPR_LT:
        ins = INSTR_LTS;
        break;
    case BIN_EXPR_LE:
        //using fact that (<=) is the same as (not >) 
        printCode(0, INSTR_GTS);
        ins = INSTR_NOTS;
        break;
    case BIN_EXPR_GT:
        ins = INSTR_GTS;
        break;
    case BIN_EXPR_GE:
        //using fact that (>=) is the same as (not <) 
        printCode(0, INSTR_LTS);
        ins = INSTR_NOTS;
    }
    printCode(0, ins);

    if(expr->binaryExpr.type >= BIN_EXPR_EQ) {
        res.type = VAL_TYPE_BOOL;
    } else {
        res.type = left;
    }
    return res;
}

