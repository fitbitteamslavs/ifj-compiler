/* 
 * Subor datastructures.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xlacko09 - Dávid Lacko
 *  xhecko02 - Michal Hečko
 *  xmocar00 - Peter Močáry
 */
#include "datastructures.h"

/* --- Context Stack --- */
int contextStackInit(struct ContextStack *stack) {
    struct ContextInfo *contents = malloc(CONSTANT_ALLOC_INCREMENT * sizeof(struct ContextInfo));
    if (contents == NULL) {
        return CONTEXT_STACK_MALLOC_FAIL;
    }
    stack->contents = contents;

    stack->allocated_size = CONSTANT_ALLOC_INCREMENT;
    stack->top = CONTEXT_STACK_EMPTY;
    return CONTEXT_STACK_OK;
}

void contextStackDestroy(struct ContextStack *stack) {
    free(stack->contents);
    stack->allocated_size = 0;
    stack->top = CONTEXT_STACK_EMPTY;
}

int contextStackPush(struct ContextStack *stack, unsigned context_id, enum ContextType type) {
    if (stack->allocated_size <= (unsigned) (stack->top+1)) { // The typecast is safe - lowest value of top is -1 (-1 + 1 == 0) which is fine for unsigned
        unsigned new_allocated_size = stack->allocated_size + CONSTANT_ALLOC_INCREMENT;

        struct ContextInfo *new_contents = (struct ContextInfo *) realloc(
                stack->contents, 
                new_allocated_size * sizeof(struct ContextInfo)); 

        if (new_contents == NULL) {
            return CONTEXT_STACK_REALLOC_FAIL;
        }
        stack->contents = new_contents;

        stack->allocated_size = new_allocated_size;
    }
    
    stack->top++;
    stack->contents[stack->top].id = context_id;
    stack->contents[stack->top].type = type;
    return CONTEXT_STACK_OK;
}

void contextStackPop(struct ContextStack *stack) {
    // TODO(codeboy):   Integrate functionality into one function:
    //                   - contextStackNotifyContextLeft() ?? - if the top context type after pop is FORLOOP pop it too
    if (stack->top >= 0) {
        stack->top--;
    }
}

void contextStackClearContents(struct ContextStack *stack) {
    if (stack->top >= 0) {
        stack->top = CONTEXT_STACK_EMPTY;
    }
}

int contextStackPeekTop(struct ContextStack *stack) {
    if (stack->top >= 0){
        return stack->contents[stack->top].id; 
    }
    return CONTEXT_STACK_EMPTY;
}

enum ContextType contextStackGetTopType(struct ContextStack *stack) {
    if (stack->top >= 0){
        return stack->contents[stack->top].type; 
    }
    return CONTEXT_TYPE_NOT_SET;
}


/* --- String --- */
int strInit(string* s){
    s->str = malloc(sizeof(char)*CONSTANT_ALLOC_INCREMENT);    
    
    if (s->str == NULL) {
        return STR_MALLOC_FAIL;
    }

    for (int i = 0; i < CONSTANT_ALLOC_INCREMENT; i++) {
        s->str[i] = '\0';
    }

    s->length = 0;
    s->allocatedSize = CONSTANT_ALLOC_INCREMENT;

        return STR_OK;
}

void strFree(string* s){
    free(s->str);
    s->str = NULL;
    s->length = 0;
    s->allocatedSize = 0;
}

int strAddChar(string* s, char c){
    unsigned write_index = s->length;    

    if (write_index >= s->allocatedSize-1){
        s->allocatedSize += CONSTANT_ALLOC_INCREMENT;
        char *newPtr = realloc(s->str, sizeof(char)*s->allocatedSize);
        if (newPtr == NULL) {
            return STR_REALLOC_FAIL;
        }
        s->str = newPtr;
        for (unsigned i = write_index+1; i < s->allocatedSize; i++){
            s->str[i] = '\0';
        }
    }

    s->str[write_index] = c;
    s->length++;
    return STR_OK;
}

int strAppendString(string* s, char* appendedString){
     
    int new_str_alloc_len = (strlen(s->str)+strlen(appendedString)+1);

    char* new_str = malloc(sizeof(char)*new_str_alloc_len);
    if (new_str == NULL){
        return STR_MALLOC_FAIL;
    }

    strncat(new_str, s->str, s->length);
    strcat(new_str, appendedString);

    free(s->str);
    s->str = new_str;
    s->allocatedSize = new_str_alloc_len;
    s->length = strlen(s->str);
    
    return STR_OK;
}

/* --- String vector --- */
int stringVectorInit(struct StringVector *vect) {
    vect->strings = malloc(sizeof(char*)*CONSTANT_ALLOC_INCREMENT);
    if (vect->strings == NULL){
        return STRING_VECTOR_MALLOC_FAIL;
    }
    for (int i = 0; i < CONSTANT_ALLOC_INCREMENT; i++) {
        vect->strings[i] = NULL;
    }
    vect->allocatedSize = CONSTANT_ALLOC_INCREMENT;
    vect->stringsCount = 0;
    return STRING_VECTOR_OK;
}

void stringVectorDestroy(struct StringVector *vect) {
    for (unsigned i = 0; i < vect->stringsCount; i++){
        free(vect->strings[i]);
        vect->strings[i] = NULL;
    }
    free(vect->strings);
    vect->strings = NULL;
    vect->allocatedSize = 0;
    vect->stringsCount = 0;
}

int stringVectorAddString(struct StringVector *vect, char *string) {
    if (vect->allocatedSize >= vect->stringsCount){
        unsigned new_allocated_size = vect->allocatedSize+CONSTANT_ALLOC_INCREMENT;
        char** new_strings_array = realloc(vect->strings, sizeof(char*)*new_allocated_size);
        if (new_strings_array == NULL) {
            return STRING_VECTOR_REALLOC_FAIL;
        }
        vect->strings = new_strings_array;
        vect->allocatedSize = new_allocated_size;
    }
    vect->strings[vect->stringsCount++] = string;    
    return STRING_VECTOR_OK;
}


/* --- Identifier stack --- */
enum IdentifierStackStatus identifierStackInit(struct IdentifierStack *stack) {
    char **stack_contents = (char **) malloc(sizeof(char *) * IDENTIF_STACK_INITIAL_CAPACITY);
    if (stack_contents == NULL) {
        return IDENTIF_STACK_MALLOC_FAILED; 
    }

    stack->capacity = IDENTIF_STACK_INITIAL_CAPACITY;
    stack->top = 0;
    stack->identifierStack = stack_contents;

    return IDENTIF_STACK_OK;
}

void identifierStackDestroy(struct IdentifierStack *stack) {
    if (stack->identifierStack == NULL) {
        return;
    }    
    free(stack->identifierStack);
    stack->identifierStack = NULL;
}

enum IdentifierStackStatus identifierStackPush(struct IdentifierStack *stack, char *identifierName) {
    if (stack->top == stack->capacity) {
        char ** new_contents_buffer = realloc(stack->identifierStack, stack->capacity * 2);
        if (new_contents_buffer == NULL) {
            return IDENTIF_STACK_CANNOT_GROW;
        } else {
            stack->capacity *= 2;
            stack->identifierStack = new_contents_buffer;
        }
    }
    
    stack->identifierStack[stack->top] = identifierName;
    stack->top++;

    return IDENTIF_STACK_OK;
}

char *identifierStackPop(struct IdentifierStack *stack) {
    // Stack top points to first free cell, therefore by decreasing we get the current top value
    stack->top--;
    return stack->identifierStack[stack->top];
}

bool identifierStackIsEmpty(struct IdentifierStack *stack) {
    return stack->top == 0;
}

unsigned identifierStackGetSize(struct IdentifierStack *stack) {
    return stack->top;
}
