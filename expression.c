/* 
 * Subor expression.c
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xhecko02 - Michal Hečko
 *  xlacko09 - Dávid Lacko
 */

#include "expression.h"

// This is set up when parseExpr is called.
// We use this global symbol to avoid copying on stack the same ptr
struct ExprErrorDetails *_err_details;

const int OPERATOR_PRIORITIES[] = {
    10, // Add
    10, // Sub
    30, // Mul
    30, // Div
    5,  // Relational
};

Expr* makeStrLiteralExpr(char *value) {
    Expr* expr = (Expr *) malloc(sizeof(Expr));
    if (expr == NULL) {
        return NULL;
    }

    expr->type = EXPR_LITERAL;
    expr->literal.type = LIT_EXPR_STR;
    expr->literal.strValue = value;
    
    return expr;
}

Expr *makeFloatLiteralExpr(double value) {
    Expr *expr = (Expr *) malloc(sizeof(Expr));
    if (expr == NULL) {
        return NULL;
    }

    expr->type = EXPR_LITERAL;
    expr->literal.type = LIT_EXPR_FLOAT;
    expr->literal.floatValue = value;

    return expr;
}

Expr *makeIntLiteralExpr(unsigned long long value) {
    Expr *expr = (Expr *) malloc(sizeof(Expr));
    if (expr == NULL) {
        return NULL;
    }

    expr->type = EXPR_LITERAL;
    expr->literal.type = LIT_EXPR_INT;
    expr->literal.intValue = value;

    return expr;
}

enum FuncCallStatus funcCallExprInit(struct FuncCallExpr *func) {
    func->funcName = NULL; 
    func->args = (Expr **) malloc(sizeof(Expr *) * FUNCALL_ARGS_INITIAL_CAPACITY);
    if (func->args == NULL) {
        return FUNCALL_MALLOC_FAILED;
    }
    func->argCount = 0;
    func->argCapacity = FUNCALL_ARGS_INITIAL_CAPACITY;

    return FUNCALL_OK;
}
enum FuncCallStatus funcCallExprAddArgument(struct FuncCallExpr *func, Expr *arg) {
    if (func->argCount == func->argCapacity) {
        Expr** new_args = (Expr **) realloc(func->args, sizeof(Expr *) * func->argCapacity * 2);
        if (new_args == NULL) {
            return FUNCALL_MALLOC_FAILED;
        }
        func->args = new_args;
        func->argCapacity *= 2;
    } 
    
    func->args[func->argCount] = arg;
    func->argCount++;

    return FUNCALL_OK;
}

void funcCallExprSetFuncName(struct FuncCallExpr *func, char *name) {
    // Maybe consider strdup-ing the name (for whatever the reason
    func->funcName = name;
}

void funcCallExprFree(struct FuncCallExpr *func) {
    for (unsigned i = 0; i < func->argCount; i++) {
        exprFree(func->args[i]); // Release each of args
    }

    free(func->args); // Release the array containing pointers to arg
    free(func->funcName);
    func->args = NULL;
    
    // We have no idea where the *func was allocated, since API functions provide no way 
    // of malloc'in the whole struct -- leave free'in struct to caller
}

Expr *makeBinaryExpr(enum BinaryExprType type, Expr *left, Expr *right) {
    Expr *e = (Expr *) malloc(sizeof(Expr));
    if (e == NULL) {
        return NULL;
    } 
    
    e->type = EXPR_BINARY;
    e->binaryExpr.left = left;
    e->binaryExpr.right = right;
    e->binaryExpr.type = type;

    return e;
}

Expr *makeIdentifExpr(char *name) {
    Expr *e = (Expr *) malloc(sizeof(Expr));
    if (e == NULL) {
        return NULL;
    } 

    e->identifier.name = name;
    e->type = EXPR_IDENTIFIER;

    return e;
}


void binaryExprFree(Expr *bin_expr){
    exprFree(bin_expr->binaryExpr.left);
    exprFree(bin_expr->binaryExpr.right);
    free(bin_expr);
}

void literalExprFree(Expr *lit_expr) {
    if (lit_expr->literal.type == LIT_EXPR_STR) {
        //free(lit_expr->literal.strValue);
    }

    free(lit_expr);
} 

void identifierExprFree(Expr *identif_expr) {
    //free(identif_expr->identifier.name);
    free(identif_expr);
}

void exprFree(Expr *expr) {
    switch (expr->type) {
        case EXPR_BINARY:
            binaryExprFree(expr);
            break;
        case EXPR_LITERAL:
            literalExprFree(expr);
            break;
        case EXPR_FUNCALL:
            funcCallExprFree(&expr->funcCall);
            free(expr);
            break;
        case EXPR_IDENTIFIER:
            identifierExprFree(expr);
            break;
        default:
            fprintf(stderr, "Trying to free expression of unknown type: %u\n", expr->type);
            assert(0);
    }
}


Expr *parseAtom(Scanner *scanner){
    Token tk = scanner->current;

    Expr *expr = NULL;
    switch (tk.type) {
        case TOKEN_IDENTIF: // If we see an identifier, either we might be FunCall-ing
            peekToken(scanner);  // We need to look at the next token, because Identifier and FuncCall are ambiguous
            Token next_tk = scanner->next;
            if (next_tk.type == TOKEN_SEP_L_BRACKET) {
                // Now we know that we are dealing with funcall: <identif>(
                return parseFuncallIterative(scanner);  // If needed, propagate NULL, function will fill out err_details
            } else {
                getToken(scanner);
                Expr *e = makeIdentifExpr(tk.data.identifierName);  // TODO(err_check): Check for an error
                if (e == NULL) {
                    free(tk.data.identifierName);
                    setErrorMallocFailed(_err_details);
                } else {
                    return e;
                }
            }
            break;

        case TOKEN_LIT_INT:
            getToken(scanner);
            expr = makeIntLiteralExpr(tk.data.intValue);
            break;

        case TOKEN_LIT_STR:
            getToken(scanner);
            expr = makeStrLiteralExpr(tk.data.strValue);  // TODO(err_check):
            break;

        case TOKEN_LIT_FLOAT:
            getToken(scanner);
            expr = makeFloatLiteralExpr(tk.data.floatValue);  // TODO(err_check):
            break;

        default:
            // The caller expected an expression, but there is none that was matched -> err
            setErrorMessageForMissingExpr(_err_details);
            return NULL;
    }

    // If we get there, that means the expr type is Literal_str/int/float
    if (expr == NULL) { 
        setErrorMallocFailed(_err_details);
    } else {
        return expr;
    };
    return NULL;
}

void setErrorMessageForInvalidToken(
        struct ExprErrorDetails *expr_error, 
        enum TokenType expected, 
        enum TokenType actual) 
{
    const char *const exp_tk_name = getTokenName(expected);
    const char *const act_tk_name = getTokenName(actual);
    size_t buf_size = strlen(EXPR_ERRROR_INVALID_TOKEN_MSG_FORMAT) + strlen(act_tk_name) + strlen(exp_tk_name) + 1; // One extra for terminating '\0'

    char *buf = (char *) malloc(buf_size * sizeof(char));
    if (buf == NULL) {
        expr_error->errorType = EXPR_ERROR_SYNTAX; // Whatever, there is no mem
        expr_error->errorDetails = NULL;
        expr_error->ownsErrorMessage = false;
        return;    
    }

    int chars_remaining = snprintf(buf, buf_size, EXPR_ERRROR_INVALID_TOKEN_MSG_FORMAT, exp_tk_name, act_tk_name);
    
    if (chars_remaining > 0 && chars_remaining >= buf_size) {
        fprintf(stderr, "Error buffer didn't fit the whole error msg?!\n");
    }

    expr_error->errorType = EXPR_ERROR_SYNTAX;
    expr_error->errorDetails = buf;
    expr_error->ownsErrorMessage = true;
}

void setErrorForUnbalancedBrackets(
        struct ExprErrorDetails *details, 
        enum TokenType unbalaned_bracket_type)
{
    if (unbalaned_bracket_type == TOKEN_SEP_L_BRACKET) {
        details->errorDetails = "Unbalanced brackets in expression. (Missing `)`)";
    } else {
        details->errorDetails = "Unbalanced brackets in expression. (Missing `(`)";
    }
    details->ownsErrorMessage = false;
    details->errorType = EXPR_ERROR_SYNTAX;
}

void setErrorMessageForMissingOperator(struct ExprErrorDetails *details) {
    details->ownsErrorMessage = false;
    details->errorDetails = "Error while parsing expression: Missing a binary operator.";
    details->errorType = EXPR_ERROR_SYNTAX;
}

void setErrorForUnexpectedToken(
        struct ExprErrorDetails *details, 
        enum TokenType token)
{
    const char *const token_name = getTokenName(token);
    size_t buf_size = strlen(EXPR_ERROR_UNEXPECTED_TOKEN) + strlen(token_name) + 1; // One extra for terminating '\0'

    char *buf = (char *) malloc(buf_size * sizeof(char));
    if (buf == NULL) {
        details->errorType = EXPR_ERROR_SYNTAX; // Whatever, there is no mem
        details->errorDetails = NULL;
        details->ownsErrorMessage = false;
        return;    
    }

    int chars_remaining = snprintf(buf, buf_size, EXPR_ERROR_UNEXPECTED_TOKEN, token_name);
    
    if (chars_remaining > 0 && chars_remaining >= buf_size) {
        fprintf(stderr, "Error buffer didn't fit the whole error msg?!\n");
    }

    details->errorType = EXPR_ERROR_SYNTAX;
    details->errorDetails = buf;
    details->ownsErrorMessage = true;
}

void setErrorMessageForMissingExpr(struct ExprErrorDetails *err_details){
    err_details->errorType = EXPR_ERROR_SYNTAX;
    err_details->errorDetails = "Expected an expression\n";
    err_details->ownsErrorMessage = false;
}

void setErrorMallocFailed(struct ExprErrorDetails *err_details) {
    err_details->errorType = EXPR_ERROR_MALLOC;
    err_details->errorDetails = NULL;
    err_details->errorDetails = false;
}

void skipAllTokensWithType(Scanner *s, enum TokenType tk_type) {
    while (s->current.type == tk_type) {
        getToken(s);
    } 
}


/*
 * ------ EXPRESSION STACK ------ 
 */

enum ExpressionStackStatus expressionStackInit(struct ExpressionStack *stack) {
    ExpressionStackItem *contents = (ExpressionStackItem *) malloc(
            sizeof(ExpressionStackItem) * EXPRESSION_STACK_INITIAL_CAPACITY); 
    if (contents == NULL) {
        return EXPR_STACK_STATUS_MALLOC_FAILED;
    }

    stack->capacity = EXPRESSION_STACK_INITIAL_CAPACITY;
    stack->contents = contents;
    stack->firstFreeSlot = 0;

    return EXPR_STACK_STATUS_OK;
}

void expressionStackDestroy(struct ExpressionStack *stack) {
    if (stack->contents != NULL) {
        free(stack->contents);
    }
}

enum ExpressionStackStatus expressionStackGrow(ExpressionStack *stack) {
    ExpressionStackItem *new_contents = (ExpressionStackItem *) realloc(
            stack->contents, 
            sizeof(ExpressionStackItem) * stack->capacity*2);
    if (new_contents == NULL) {
        return EXPR_STACK_STATUS_REALLOC_FAILED;
    }

    stack->contents = new_contents;
    stack->capacity *= 2;

    return EXPR_STACK_STATUS_OK;
}

enum ExpressionStackStatus expressionStackPushExpression(struct ExpressionStack *stack, Expr *expr) {
    if (stack->capacity <= stack->firstFreeSlot) {
        enum ExpressionStackStatus status = expressionStackGrow(stack);
        if (status != EXPR_STACK_STATUS_OK) {
            return status;
        }
    } 

    stack->contents[stack->firstFreeSlot].type = EXPR_STACK_ITEM_TYPE_EXPRESSION;
    stack->contents[stack->firstFreeSlot].expr = expr;

    stack->firstFreeSlot++; 
    return EXPR_STACK_STATUS_OK;
}

enum ExpressionStackStatus expressionStackPushTerminal(struct ExpressionStack *stack, enum TokenType terminal_type) {
    if (stack->capacity <= stack->firstFreeSlot) {
        enum ExpressionStackStatus status = expressionStackGrow(stack);
        if (status != EXPR_STACK_STATUS_OK) {
            return status;
        }
    } 

    stack->contents[stack->firstFreeSlot].type = EXPR_STACK_ITEM_TYPE_TERMINAL;
    stack->contents[stack->firstFreeSlot].terminal = terminal_type;

    stack->firstFreeSlot++; 
    return EXPR_STACK_STATUS_OK;
}


void expressionStackPop(struct ExpressionStack *stack, ExpressionStackItem *dest) {
    const unsigned stack_top = stack->firstFreeSlot - 1;
    *dest = stack->contents[stack_top]; 
    stack->firstFreeSlot--;
}

bool expressionStackEmpty(struct ExpressionStack *stack) {
    return (stack->firstFreeSlot == 0);
}

enum TokenType expressionStackGetTopTerminal(ExpressionStack *stack) {
    ExpressionStackItem *current_item;
    for (int i = stack->firstFreeSlot - 1; i >= 0; i--) {
        current_item = &stack->contents[i];

        if (current_item->type == EXPR_STACK_ITEM_TYPE_TERMINAL) {
            return current_item->terminal;
        }
    }

    return -1; // Enums are numbered from 0
}

unsigned expressionStackSize(ExpressionStack *stack) {
    return stack->firstFreeSlot;
}

void expressionStackPeekTop(ExpressionStack *stack, ExpressionStackItem *dest) {
    *dest = stack->contents[stack->firstFreeSlot - 1];
}

bool expressionStackIsTopBinaryOperator(ExpressionStack *stack) {
    if (stack->firstFreeSlot == 0) {
        return false; // Empty stack
    }

    const unsigned stack_top = stack->firstFreeSlot - 1;
    
    ExpressionStackItem *top = &stack->contents[stack_top];
    if (top->type == EXPR_STACK_ITEM_TYPE_TERMINAL) {
        return isTokenBinaryOperator(top->terminal);        
    }
    return false;
}

bool expressionStackIsTopExpression(ExpressionStack *stack) {
    if (stack->firstFreeSlot == 0) {
        return false;
    }

    const unsigned stack_top = stack->firstFreeSlot - 1;
    
    ExpressionStackItem *top = &stack->contents[stack_top];
    if (top->type == EXPR_STACK_ITEM_TYPE_EXPRESSION) {
        return true;
    }

    return false;
}


int getTerminalPriority(enum TokenType type) {
    enum BinaryExprType bin_expr_type = type - TOKEN_OP_ARIT_PLUS;
    int op_priority;
    if (bin_expr_type >= BIN_EXPR_EQ) {
        // All relational operators have the same priority
        op_priority = OPERATOR_PRIORITIES[BIN_EXPR_EQ];
    } else {
        const int prio_index = bin_expr_type  - BIN_EXPR_ADD; // Calculate offset to the priority table
        op_priority = OPERATOR_PRIORITIES[prio_index];
    }

    return op_priority;
}

enum BinaryExprType getBinaryExprTypeFromTokenType(enum TokenType type) {
    enum BinaryExprType bin_expr_type = type - TOKEN_OP_ARIT_PLUS;
    return bin_expr_type;
}

bool isAtom(Scanner *scn) {
    switch (scn->current.type) {
        case TOKEN_LIT_INT:
        case TOKEN_LIT_STR:
        case TOKEN_LIT_FLOAT:
        case TOKEN_IDENTIF:
            return true;
        default:
            return false;
    }
}

void reduceTopmostExpressionOnStack(ExpressionStack *stack) {
    ExpressionStackItem item;
    expressionStackPop(stack, &item);

    Expr *right_operand = item.expr;

    expressionStackPop(stack, &item);
    enum TokenType operator_token = item.terminal;
    enum BinaryExprType binary_expr_type = getBinaryExprTypeFromTokenType(operator_token);

    expressionStackPop(stack, &item);
    Expr *left_operand = item.expr;

    Expr *reduced_expression = makeBinaryExpr(binary_expr_type, left_operand, right_operand);
    expressionStackPushExpression(stack, reduced_expression);
}

int getPriorityForTerminal(enum TokenType terminal_type) {
    if ((int) terminal_type == -1) {
        return -1; // No terminal on stack
    }

    // We know that terminal is a binary operator or a `(`
    if (terminal_type == TOKEN_SEP_L_BRACKET) {
        return MIN_OPERATOR_PRIORITY; 
    }

    // So the terminal must be some kind of binary operator
    enum BinaryExprType bin_type = terminal_type - TOKEN_OP_ARIT_PLUS; // We don't have to check bounds, since we know what is on stack
    int op_priority;
     
    if (bin_type >= BIN_EXPR_EQ) {
        // All relational operators have the same priority
        op_priority = OPERATOR_PRIORITIES[BIN_EXPR_EQ];
    } else {
        const int prio_index = bin_type - BIN_EXPR_ADD; // Calculate offset to the priority table
        op_priority = OPERATOR_PRIORITIES[prio_index];
    }
    return op_priority;
}

bool isTokenBinaryOperator(enum TokenType type) {
    // This is much less error prone then enum arithmetics
    switch (type) {
        case TOKEN_OP_ARIT_PLUS:
        case TOKEN_OP_ARIT_MINUS:
        case TOKEN_OP_ARIT_DIV:
        case TOKEN_OP_ARIT_TIMES:
        case TOKEN_OP_REL_EQ:
        case TOKEN_OP_REL_NOTEQ:
        case TOKEN_OP_REL_LESS:
        case TOKEN_OP_REL_LESSEQ:
        case TOKEN_OP_REL_GREATER:
        case TOKEN_OP_REL_GREATEREQ:
            return true;
        default:
            return false;
    }
}

void reduceExpressionUntilLeftBracketFound(ExpressionStack *stack) {
    // Reduce all binary expressions until there is none left
    while (expressionStackGetTopTerminal(stack) != TOKEN_SEP_L_BRACKET) {
        reduceTopmostExpressionOnStack(stack);
    }
}

Expr *parseFuncallIterative(Scanner *scn) {
    // We know that currently we have <identif>(
    
    Expr *e = (Expr *) malloc(sizeof(Expr));
    if (e == NULL) {
        _err_details->errorType = EXPR_ERROR_MALLOC;
        _err_details->errorDetails = NULL;
        return NULL; // This will propagate NULL all the way up.
    }

    e->type = EXPR_FUNCALL;
    funcCallExprInit(&e->funcCall);
    funcCallExprSetFuncName(&e->funcCall, scn->current.data.identifierName);

    // Now we can consume <identif>(
    getToken(scn);
    getToken(scn);

    bool found_call_end = (scn->current.type == TOKEN_SEP_R_BRACKET); // In case we had <identif>() we end parsing
    bool hit_error = false;
    
    // After initial call L-bracket EOLs are accepted - skip them
    skipAllTokensWithType(scn, TOKEN_EOL);

    while ((!found_call_end) && (!hit_error)) {
        // We know that call is not empty - current tk should be expression 
        Expr *arg = parseExpressionIterative(scn, true); 
        
        if (arg == NULL) {
            funcCallExprFree(&e->funcCall); // First release info about function
            free(e); // Release expr itself
            return NULL;// err_details was already filled
        }

        enum FuncCallStatus status = funcCallExprAddArgument(&e->funcCall, arg); 
        if (status == FUNCALL_MALLOC_FAILED) {
            funcCallExprFree(&e->funcCall); // Release args and funcname
            free(e); // Release expr itself
            setErrorMallocFailed(_err_details);
        }

        // parseExpr consumes everything it parses, therefore current token is ','
        found_call_end = (scn->current.type == TOKEN_SEP_R_BRACKET);
        if (!found_call_end) {
            // If we have not found the end, therefore next token must be ',' 
            if (scn->current.type != TOKEN_SEP_COMMA) {
                // If it isn't comma - then there might be <EOL>s and then bracket )
                if (scn->current.type == TOKEN_EOL) {
                    skipAllTokensWithType(scn, TOKEN_EOL); 
                    // Since there was no comma, an ')' must close up function call
                    if (scn->current.type == TOKEN_SEP_R_BRACKET) {
                        found_call_end = true; // Funcall done
                    } else {
                        hit_error = true; // The funcall was invalid

                        // TODO: The error was most likely caused by forgotten comma, when writing a funcall like this:
                        //      funcall(
                        //          a   <-- missing comma
                        //          b
                        //      )
                        //      However this would report missing ')'.
                        //      Since we know that the funcall is invalid either way, we can peek at next token, ignoring EOLS
                        //      and when we find the rest of funcall (which would be tedious) report a nicer message - missing comma 
                        /*setErrorMessageForInvalidToken(_err_details, TOKEN_SEP_R_BRACKET, sc->current.type);                     */
                    }
                } else {
                    // If there is no EOL we can be sure that there is an invalid token
                    hit_error = true;
                    /*setErrorMessageForInvalidToken(_err_details, TOKEN_SEP_COMMA, sc->current.type);*/
                }
            } else {
                getToken(scn); // Consume the comma, and continue loop - after comma there should be expr

                // Comma can be followed by EOLs
                skipAllTokensWithType(scn, TOKEN_EOL);
            }
        }
    }
    
    // If we didn't hit an error, we want to consume the ')' aswell 
    if (!hit_error) {
        getToken(scn);
    } else {
        funcCallExprFree(&e->funcCall); // First release info about function
        free(e); // Release expr itself
        return NULL;
    }

    return e;
}


bool doesExpressionStackContainTerminalWithType(ExpressionStack *stack, enum TokenType type) {
    for (unsigned int i = 0; i < stack->firstFreeSlot; i++) {
        if (stack->contents[i].type == EXPR_STACK_ITEM_TYPE_TERMINAL) {
            if (stack->contents[i].terminal == type) {
                return true;
            }
        }
    }
    return false;
}


Expr *parseExpressionIterative(Scanner *scn, bool inside_funcall) { 
    struct ExpressionStack stack;
    enum ExpressionStackStatus status = expressionStackInit(&stack);
    if (status == EXPR_STACK_STATUS_MALLOC_FAILED) {
        setErrorMallocFailed(_err_details); 
        return NULL;
    }
    bool found_expression_end = false; 
    bool hit_error = false;

    while ((!found_expression_end) && (!hit_error)) {
        // a * b + c
        if (isAtom(scn)) {
            Expr *e = parseAtom(scn);
            if (e == NULL) {
                // ErrorDetails have been filled out by parseAtom
                expressionStackDestroy(&stack);
                return NULL;
            }
            // Check whether we are not pushing 2 expressions on top each other <E> <E>
            if (expressionStackIsTopExpression(&stack)) {
                setErrorMessageForMissingOperator(_err_details);
                hit_error = true;
                continue;
            }

            status = expressionStackPushExpression(&stack, e);
            if (status == EXPR_STACK_STATUS_REALLOC_FAILED) {
                setErrorMallocFailed(_err_details); 
            }

        } else if (isTokenBinaryOperator(scn->current.type)){
            if (!expressionStackIsTopExpression(&stack)) {
                setErrorMessageForMissingExpr(_err_details); 
                hit_error = true;
                continue;
            }

            int current_operator_priority = getPriorityForTerminal(scn->current.type);
            int previos_terminal_priority = getPriorityForTerminal(expressionStackGetTopTerminal(&stack));
           
            if (current_operator_priority > previos_terminal_priority) {
                expressionStackPushTerminal(&stack, scn->current.type);
                getToken(scn);
            } else {
                // Apply reduction E(a) * E(b)    seeing: +
                reduceTopmostExpressionOnStack(&stack);
            }
        } else if (scn->current.type == TOKEN_SEP_L_BRACKET) {
            if (expressionStackIsTopExpression(&stack)) {
                setErrorMessageForMissingOperator(_err_details);
                hit_error = true;
                continue;
            }
            expressionStackPushTerminal(&stack, scn->current.type);
            getToken(scn);
        } else if (scn->current.type == TOKEN_SEP_R_BRACKET) {
            if (!expressionStackIsTopExpression(&stack)) {
                setErrorMessageForMissingExpr(_err_details);
                hit_error = true;
                continue;
            }

            if (doesExpressionStackContainTerminalWithType(&stack, TOKEN_SEP_L_BRACKET)) {
                if (expressionStackIsTopBinaryOperator(&stack)) {
                    setErrorMessageForMissingExpr(_err_details);
                    hit_error = true;
                    continue;
                }

                reduceExpressionUntilLeftBracketFound(&stack); 

                // We need to remove the bracket before the reduced expression
                ExpressionStackItem top_item;
                expressionStackPop(&stack, &top_item);

                if (top_item.type == EXPR_STACK_ITEM_TYPE_TERMINAL) {
                    // We either have an empty grouping ()
                    // Or we might have an incomplete expression (<expression + )
                    setErrorMessageForMissingExpr(_err_details);
                    hit_error = true; // Exit gracefully
                    continue;
                }
                
                Expr *grouping_expr = top_item.expr;
                expressionStackPop(&stack, &top_item);                  // Read the ( off
                expressionStackPushExpression(&stack, grouping_expr);   // Push the reduced expression back in
                getToken(scn); // Consume )
            } else {
                if (inside_funcall) {
                    // This is the end of funcall, everything ok
                    found_expression_end = true;
                } else {
                    // Fill out the error message that there was extra `)`
                    setErrorForUnbalancedBrackets(_err_details, TOKEN_SEP_R_BRACKET);
                    hit_error = true;
                }
            }
        } else {
            // We might see EOL
            if (expressionStackIsTopBinaryOperator(&stack)) {
                // Binary operator might be followed by EOLs, so consume them
                skipAllTokensWithType(scn, TOKEN_EOL);
            } else {
                found_expression_end = true;
            }
        }
    }

    if (hit_error) {
        expressionStackDestroy(&stack);
        return NULL;
    }

    if (expressionStackSize(&stack) == 0) {
        setErrorMessageForMissingExpr(_err_details);
        expressionStackDestroy(&stack);
        return NULL;
    }

    while (expressionStackSize(&stack) > 1) {
        // We need to check, whether there is no `(` somewhere on the stack
        if (doesExpressionStackContainTerminalWithType(&stack, TOKEN_SEP_L_BRACKET)) {
            setErrorForUnbalancedBrackets(_err_details, TOKEN_SEP_L_BRACKET);
            expressionStackDestroy(&stack);
            return NULL; 
        }
        reduceTopmostExpressionOnStack(&stack);
    } 
    
    Expr *resulting_expr = stack.contents[0].expr;
    expressionStackDestroy(&stack);
    return resulting_expr;
}

Expr *parseExpr(Scanner *scn, struct ExprErrorDetails *error_details) {
    _err_details = error_details;
    return parseExpressionIterative(scn, false);
}
