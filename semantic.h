/* 
 * Subor semantic.h
 * Nazov projektu: IFJ 2020 Projekt
 * Projekt vypracovali:
 *  xbalaz12 - Peter Balážec
 *  xlacko09 - Dávid Lacko
 *  xmocar00 - Peter Močáry
 *  xhecko02 - Michal Hečko
 * Team leader:
 *  xlacko09 - Dávid Lacko
 *
 * Autori tohto súboru:
 *  xmocar00 - Peter Močáry
 */
#ifndef IFJ20_SEMANTIC_H
#define IFJ20_SEMANTIC_H

#include  <stdarg.h>
#include  <stdio.h>
#include  <inttypes.h>

#include "datastructures.h"

#define INST_COUNT 56
#define TYPES_COUNT 8
#define CONST_REALLOC 16
    
enum InstructionSet {
    // Frame and function instructions
    INSTR_MOVE,                                     // 0
    INSTR_CREATEFRAME,
    INSTR_PUSHFRAME,
    INSTR_POPFRAME,
    INSTR_DEFVAR,
    INSTR_CALL,
    INSTR_RETURN,
    
    // Data stack instructions
    INSTR_PUSHS,                                    // 7
    INSTR_POPS,
    INSTR_CLEARS,

    // Aritmethical instructions
    INSTR_ADD,  INSTR_ADDS,                         // 10, 11
    INSTR_SUB,  INSTR_SUBS,
    INSTR_MUL,  INSTR_MULS,
    INSTR_DIV,  INSTR_DIVS,
    INSTR_IDIV, INSTR_IDIVS,
    
    // relational instructions
    INSTR_LT, INSTR_LTS,                            // 20, 21
    INSTR_GT, INSTR_GTS,
    INSTR_EQ, INSTR_EQS,
    
    // boolean instructions
    INSTR_AND, INSTR_ANDS,                          // 26, 27
    INSTR_OR,  INSTR_ORS,
    INSTR_NOT, INSTR_NOTS,
    
    // convertion instructions
    INSTR_INT2FLOAT,  INSTR_INT2FLOATS,             // 32, 33
    INSTR_FLOAT2CHAR, INSTR_FLOAT2INTS,
    INSTR_INT2CHAR,   INSTR_INT2CHARS,
    INSTR_STRI2INT,   INSTR_STRI2INTS,
    
    // I/O instructions
    INSTR_READ,                                     // 40
    INSTR_WRITE,
    
    // string instructions
    INSTR_CONCAT,                                   // 42
    INSTR_STRLEN,
    INSTR_GETCHAR,
    INSTR_SETCHAR,
    
    // type instruction
    INSTR_TYPE,                                     // 46
    
    // proglam flow control instructions 
    INSTR_LABEL,                                    // 47
    INSTR_JUMP,
    INSTR_JUMPIFEQ,  INSTR_JUMPIFEQS,
    INSTR_JUMPIFNEQ, INSTR_JUMPIFNEQS,
    INSTR_EXIT,                       
    
    // debugging instructions
    INSTR_BREAK,                                    // 54
    INSTR_DPRINT
};

enum Types {
    TYPE_FLOAT, TYPE_INT, TYPE_STR, TYPE_BOOL, TYPE_NIL,
    TYPE_GF, TYPE_LF, TYPE_TF
};

/**
 * Variadic function created to pring instruction with arguments.
 * For example: MOVE LF@var_x int@42
 * Call example: printCode(2, INSTR_MOVE, TYPE_LF, "var_x", TYPE_INT, "42");
 *  
 * The additional arguments are expected to be grupped in pairs of argument type
 * and argument value. Where argument type shoud be value from Types enum and
 * argument value should be a double/long long int/string.
 *
 * Mind that passing wrong arg / wrong number of additional arguments / wrong 
 * types of additional arguments will cause undefined behavior.
 *
 * @param argc is the number of arguments of the instruction one wants to print
 * @param instr the instruction value from the InstructionSet enum
 * @param ... aditional argumets
 */
void printCode(unsigned argc, enum InstructionSet instr, ...);
#endif
