# Symtable
From the assignment: symtable must be implemented as a hash table.

## Hashtables primer
### Hash tables work in following session:
They keep internally array of "buckets" of some arbitrary size `HASH\_TABLE\_SIZE`.
The use something called the hashing function to turn a key (string) into an integer, which 
is then trimmed (using modulo operator) to fit the array boundaries. This way an index to the table
is produced. However since there are infinite number of possible keys and the array size is limited,
sometimes 2 keys will produce the same index - this situation is called _collision_. To handle collisions
buckets are in fact linked lists and the correct value assigned to key is then looked up in a linear fasion.

## Hashtable hashing function
For its simplicity the djb2 hashing algorithm was chosen. This algorithm is one from a class of hashing function called: _Linear Congruential Generator_, 
which take the form of: `hash = p*hash + c`. `p` is a prime, `c` is a arbitrary constant (ideally another prime). The original djb2 constans of djb2 were tweaked
a bit - `p=33` was left alone, `c` was chosen differently -- this way the imlpementation is unique and the mathematical properties are kept.

For more details see: http://www.cse.yorku.ca/~oz/hash.html.

## How is context managment implemented. 
To implement a context managment in an efficient fashion (aka do not create a new hash table for every new context), a stack is needed to keep track which symbols
were pushed at what time and to make note "when" the current context frame starts.

### Stack implementation
Every stored linked list inside the array of an symbol table has slightly modified node structure. Except the traditional componens such as `next` pointer
an integer is stored. This integer is a index to another (possibly the same) bucket, to which the previous symbol was pushed. This way we keep a history
of where was a new symbol added.

#### Context pushing/popping
When a context is pushed a special symbol is added to the table - SENTIENT symbol. It is a symbol with empty data and special name (one that cannot be contained
withing source text of a program (currently `"<!SENTIENT>"`)). Then we continue inserting symbols normally into the table. When the time to destroy the context comes,
we just walk the stack till we hit first occurence of a symbol with name _SENTIENT_. That way we know we have destroyed the whole context. (We can also find that we are at the bottom of the stack - stack is empty, but that also means we have destoyed the context).


### Variable shadowing
Since we are currently using only one hashtable to manage multiple contexts, a situation when hashtable contains multiple symbols with the same name might arise.
This is resolved that the more recent symbol is located sooner in corresponding bucket. When looking up a symbol value in the context we just locate the first
occurence in the corresponding bucket (linked list) - and the variable shadowing is implemented.

# Inspiration
Normally I would suggest keeping a hashtable for every context push/pop and then going down the stack of contexts to see whether there is symbol with a given name.

I've found a rough sketch of this approach in the book: Andrew W. Appel - Modern compiler implementation in C, 1998, ISBN: 0-521-60765-5.
